from __future__ import with_statement


import socket, threading, time


CACHE_TIME = 60*60


_lock = threading.Lock()
_cache = {}	# ip => (check-time, is-proxy?)


def proxy(board_db, ip):
	""" ip is an dotted ipv4 string. return if ip appears to be a proxy. """
	
	#check cache first
	with _lock:
		for e in _cache.keys():
			if (time.time() - _cache[e][0]) > CACHE_TIME:
				del _cache[e]
		if ip in _cache:
			return _cache[ip][1]
	
	r = '.'.join(reversed(ip.split('.')))

	proxy = False
	
	for bl in board_db.config.proxy_dnsbls.splitlines():
		try:
			if socket.gethostbyname('%s.%s' % (r, bl.strip())):
				proxy = True
				break
		except:
			pass

	with _lock:
		_cache[ip] = (time.time(), proxy)
	
	return proxy


#if __name__ == '__main__':
#	print proxy('67.159.50.93')#85.225.147.68')

