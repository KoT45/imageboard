"""
operation of captchas is complicated by caching, but this is how it works:
	a new captcha-code is generated every time a captcha-containing form is generated.
	a new captcha image is generated for every <captcha-code, session-id>.
	once someone has posted with a certain <captcha-code, session-id> the captcha-code is stored in their session and may not be used again.
"""


import subprocess, random


LENGTH = 4		# do not change without also changing make(...)
CODE_REGEX = r'[0-9a-fA-F]{16}'


def random_code():
	return ('%X' % random.getrandbits(64)).rjust(16, '0')


def letters(global_db, code, session_id):
	CHARS = 'ABCDEFGHJLMNPQRSTUVWYZ23456789'	# don't use any of '01IKOX' because they are easily confused
	r = random.Random(global_db.unique_string + code + session_id)
	return ''.join(r.choice(CHARS) for i in xrange(LENGTH))


def make(global_db, code, session_id):

	lts = letters(global_db, code, session_id)
	sw1 = random.choice([1, -1]) * random.randint(30, 50)
	sw2 = random.choice([1, -1]) * random.randint(30, 50)

	x = 'convert -size 80x20 xc:transparent -pointsize 24 -font img2/Tuffy_Bold.ttf -stroke white -strokewidth 1'
	for i, c in enumerate(lts):
		x += ' -annotate +%d+18 %s' % (4+i*18, c)
	x += ' -region 40x20+0+0 -swirl %d -region 40x20+40+0 -swirl %d png:-' % (sw1, sw2)

	p = subprocess.Popen(x.split(), stdout = subprocess.PIPE)
	data = p.communicate()[0]
	if p.returncode:
		raise ValueError

	return data, 'image/png'


def valid(global_db, code, session_id, solution):
	return letters(global_db, code, session_id) == solution.upper()
