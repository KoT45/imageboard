
"""
abstracts database access. sql goes in here.

There should be only one Global object created.

Any request that modifies the cached config information that Global and Board objects hold,
or deletes or renames a board, must use the EXCLUSIVE http request isolation level.
"""

from __future__ import with_statement

import os, sqlite3, math, time, cPickle as pickle, random, re, threading, logging, shutil

import common, uploaded_files, fields, parser, proxycheck, util.sessions, util.cache as cache, util.json as json, util.i18n as i18n

from util import ObjDict

from collections import defaultdict

import geoip2.database


class PostStatus:
	UNDELETED, USER_DELETED, MOD_DELETED = 0, 1, 2


NEW_POST = ObjDict(
	name = '',
	tripcode = '',
	noko = False,
	bump = True,
	locked = False,
	sticky = False,
	mod = False,
	comment = ''
)

NEW_THREAD = ObjDict(
	name = '',
	tripcode = '',
	noko = False,
	locked = False,
	sticky = False,
	bumpable = True,
	mod = False,
	subject = '',
	comment = ''
)


def _json_decode(x):
	return None if x is None else json.decode(x, ObjDict)


def valid_ban_ip(ip):
	return util.valid_ip(ip) or re.match(r'^\*?\.?([-a-z0-9]+\.)*([a-z]+)$', ip)


def num_preceding_deleted(thread, index):
	mc, pc = 0, 0	#mod/poster deletion count
	i = index
	while i >= 0 and thread[i].id > 0:
		if thread[i].status == PostStatus.MOD_DELETED:
			mc += 1
		elif thread[i].status == PostStatus.USER_DELETED:
			pc += 1
		else:
			break
		i -= 1
	return mc, pc


class NoThread(Exception):
	""" raised when a post is attemped to be added to a non-existant thread. """

class ThreadFull(Exception):
	""" raised when a post is attempted to be added to a thread that has reached the maximum number of posts. """

class DoublePost(Exception):
	""" raised when a duplicate post is attempted to be added to a thread. """

class DoubleThread(Exception):
	""" raised when a duplicate thread is made """

class DuplicateFile(Exception):
	""" raised when a duplicate file is attempted to be posted """

class BannedFile(Exception):
	""" raised when a banned file is attempted to be posted """

class Flooding(Exception):
	""" raised when an ip attempts to post too quickly """


class _SQLiteDB(util.SQLiteDB):
	
	def create_functions(self, gmt_offset):
		
		def udf(f):
			""" return a wrapped user defined function that logs any exception. """
			def g(*args, **kwargs):
				try:
					return f(*args, **kwargs)
				except:
					logging.exception('exception in user defined sqlite function')
					raise
			return g
		
		self.db.create_function('format_time', 0, udf(lambda: util.format_time(None, gmt_offset)))
		self.db.create_function('format_time', 1, udf(lambda t: util.format_time(t, gmt_offset)))
		self.db.create_function('inow', 0, udf(lambda: int(time.time())))
		self.db.create_function('unhex', 1, udf(lambda x: buffer(util.unhex(x))))
		self.db.create_function('fqdn', 1, udf(lambda x: unicode(util.reverse_dns(x)) if (x and util.valid_ip(x)) else None))

	def update_tables(self, script):
		"""
			take a script containing CREATE TABLE statements and ensures the described tables exist - adding new tables/columns as needed.
			required for upgrading from older versions of the database. do not use in new code.
		"""
		with self.write:
			for m in re.finditer(r'CREATE TABLE IF NOT EXISTS (.+?) \((.+?)\);', script, re.DOTALL):
				name = m.group(1)
				self.exe(m.group())					# creates table if it doesn't exist - but doesn't add any new columns if table already exists
				
				# add new columns
				self.all("SELECT * FROM %s LIMIT 0" % name)
				cur_cols = [col[0] for col in self.cur.description]
				for col in m.group(2).splitlines():
					col = col.strip()
					if col.endswith(','):
						col = col[:-1]
					if not col or 'PRIMARY KEY' in col or 'UNIQUE' in col:
						continue
					col_name = col.partition(' ')[0]
					if not col_name in cur_cols:
						self.exe("ALTER TABLE %s ADD COLUMN %s" % (name, col))


class Global(object):
	"""
	Attributes:
		unique_string
		board_config_defaults
		mods
		boards
	"""

	CURRENT_VERSION = 3

	CURRENT_TABLES = """
		CREATE TABLE IF NOT EXISTS global (
			version INT NOT NULL,
			unique_string TEXT NOT NULL
		);
		
		CREATE TABLE IF NOT EXISTS board_config_defaults (
			name TEXT PRIMARY KEY NOT NULL,
			value NOT NULL
		);
		
		CREATE TABLE IF NOT EXISTS mods (
			name TEXT PRIMARY KEY NOT NULL,
			password_hash BLOB NOT NULL,
			admin INTEGER NOT NULL,
			created INTEGER NOT NULL,
			last_active INTEGER NOT NULL DEFAULT 0
		);
		
		CREATE TABLE IF NOT EXISTS modlog (
			time INTEGER NOT NULL,
			mod_name TEXT NOT NULL,
			message TEXT NOT NULL
		);
		
		CREATE TABLE IF NOT EXISTS login_attempts (
			ip TEXT NOT NULL,
			time INTEGER NOT NULL,
			PRIMARY KEY (ip, time)
		);
		
		CREATE TABLE IF NOT EXISTS bans (
			ip TEXT PRIMARY KEY NOT NULL,
			created INTEGER NOT NULL,
			expires INTEGER NOT NULL,
			reason TEXT NOT NULL,
			board_name TEXT NOT NULL DEFAULT ''
		);
		
		CREATE TABLE IF NOT EXISTS file_bans (
			md5 BLOB PRIMARY KEY NOT NULL
		);
	"""

	def __init__(self):
		self._local = threading.local()
		gdb = self._update()
			
		self.unique_string = gdb.one("SELECT * FROM global").unique_string

		self.board_config_defaults = ObjDict()
		for row in gdb.all("SELECT name, value from board_config_defaults"):
			self.board_config_defaults[row.name] = row.value
		for field in Board.CONFIG_FIELDS:
			if field[0] not in self.board_config_defaults:
				gdb.exe("INSERT INTO board_config_defaults (name, value) VALUES (?, ?)", field[0], field[1].default)
				self.board_config_defaults[field[0]] = field[1].default
		
		self.mods = {}
		for row in gdb.all("SELECT name, password_hash, admin FROM mods"):
			self.mods[row.name] = (str(row.password_hash), bool(row.admin))
		if not self.mods:
			name = common.Mod.DEFAULT_LOGIN[0]
			password_hash = util.sha1(self.unique_string + ''.join(common.Mod.DEFAULT_LOGIN), False)
			gdb.exe("INSERT INTO mods (name, password_hash, admin, created) VALUES (?, ?, 1, ?)", name, buffer(password_hash), int(time.time()))
			self.mods[name] = (password_hash, True)
		
		self.boards = []
		for f in os.listdir(os.path.join(common.DATABASE_DIR, 'boards')):
			if f.endswith('.sqlite'):
				d = f[:-len('.sqlite')]
				if re.match(common.BOARD_DIR_REGEX, d):
					self.boards.append(Board(self, d))
		self.boards.sort(key = lambda b: b.dir)
	
	
	def _update(self):
		""" make sure the database version is current """
		gdb = _SQLiteDB(os.path.join(common.DATABASE_DIR, 'global.sqlite'))
		
		try:
			v = gdb.one("SELECT version FROM global").version
		except (sqlite3.OperationalError, AttributeError):		# no versioning on this db version...
			
			# first upgrade to the version DIRECTLY BEFORE THE ONE when versioning started...
			gdb.update_tables("""
				CREATE TABLE IF NOT EXISTS unique_string (
					unique_string TEXT NOT NULL
				);
				CREATE TABLE IF NOT EXISTS board_config_defaults (
					name TEXT PRIMARY KEY NOT NULL,
					value NOT NULL
				);
				CREATE TABLE IF NOT EXISTS mods (
					name TEXT PRIMARY KEY NOT NULL,
					password_hash BLOB NOT NULL,
					admin INTEGER NOT NULL,
					created INTEGER NOT NULL,
					last_active INTEGER NOT NULL DEFAULT 0
				);
				CREATE TABLE IF NOT EXISTS modlog (
					time INTEGER NOT NULL,
					mod_name TEXT NOT NULL,
					message TEXT NOT NULL
				);
				CREATE TABLE IF NOT EXISTS bans (
					ip TEXT PRIMARY KEY NOT NULL,
					expires INTEGER NOT NULL,
					reason TEXT NOT NULL,
                    board_name TEXT NOT NULL DEFAULT ''
				);
				CREATE TABLE IF NOT EXISTS file_bans (
					md5 BLOB PRIMARY KEY NOT NULL
				);
			""")
			if not gdb.one("SELECT * FROM unique_string"):
				us = util.rand_str(128)
				gdb.exe("INSERT INTO unique_string (unique_string) VALUES (?)", us)
			
			# then upgrade from that version to the one with a version field :D
			us = gdb.one("SELECT * FROM unique_string").unique_string
			gdb.exe("DROP TABLE unique_string")
			gdb.exe("CREATE TABLE global (version INT NOT NULL, unique_string TEXT NOT NULL)")
			gdb.exe("INSERT INTO global (version, unique_string) VALUES (1, ?)", us)
			v = 1
		
		def _1_2():
			gdb.exe("CREATE TABLE IF NOT EXISTS login_attempts (ip TEXT NOT NULL, time INTEGER NOT NULL, PRIMARY KEY (ip, time))")
		
		def _2_3():
			gdb.exe("ALTER TABLE bans ADD COLUMN created INT NOT NULL DEFAULT 0")
		
		while v < self.CURRENT_VERSION:
			locals()['_%d_%d' % (v, v+1)]()
			v += 1
			gdb.exe("UPDATE global SET version = ?", v)
		
		return gdb
		
		
	def __getattr__(self, name):
		if name == 'db':
			if not hasattr(self._local, 'db'):
				self._local.db = _SQLiteDB(os.path.join(common.DATABASE_DIR, 'global.sqlite'))
				self._local.db.create_functions(self.board_config_defaults.gmt_offset)
			return self._local.db
		raise AttributeError(name)
	

	# using a def makes -g give a warning...
	_ = lambda self, key: i18n.catalog(common.LOCALE_DIR, self.board_config_defaults.locale)[key]


	def make_board(self, dir):
		""" create the board and return it, or raise ValueError """
		_ = self._

		if self.get_board(dir):
			raise ValueError(_('Board %s already exists.') % dir)
		
		if not re.match(common.BOARD_DIR_REGEX, dir):
			raise ValueError(_('"%s" is not a valid Board dir.') % dir)
		
		board = Board(self, dir)
		board.set_config('title', dir.capitalize(), False)
		board.set_config('desc', _('Welcome to /%s/') % dir.capitalize(), False)
		self.boards.append(board)
		return board


	def delete_board(self, board):
		for i in xrange(len(self.boards)):
			if self.boards[i] == board:
				self.boards.pop(i)
				break
		
		board._local = None		#remove references to the _SQLiteDB instances
		os.unlink(os.path.join(common.DATABASE_DIR, 'boards', '%s.sqlite' % board.dir))
		shutil.rmtree(os.path.join(common.UPLOADED_FILE_DIR, board.dir), True)


	def rename_board(self, board, new_dir):
		_ = self._

		if self.get_board(new_dir):
			raise ValueError(_('A Board with dir "%s" already exists.') % new_dir)
			
		if not re.match(common.BOARD_DIR_REGEX, new_dir):
			raise ValueError(_('"%s" is not a valid Board dir.') % new_dir)
		
		d = lambda n: os.path.join(common.DATABASE_DIR, 'boards', '%s.sqlite' % n)
		u = lambda n: os.path.join(common.UPLOADED_FILE_DIR, n)
		
		os.rename(d(board.dir), d(new_dir))
		os.rename(u(board.dir), u(new_dir))
		board.dir = new_dir


	def set_board_config_default(self, key, value):
		""" assumes value is valid for the field named by key. """
		self.db.exe("INSERT OR REPLACE INTO board_config_defaults (name, value) VALUES (?, ?)", key, value)
		self.board_config_defaults[key] = value
		for board in self.boards:
			if board.use_config_defaults[key]:
				board.config[key] = value
				board._update_parsed_config()
	
	
	def modlog(self, mod_name, message):
		""" note: message will be displayed as raw html """
		self.db.exe("INSERT INTO modlog (time, mod_name, message) VALUES (inow(), ?, ?)", mod_name, message)
	
	
	def add_mod(self, name, admin):
		password = util.rand_str(12)
		password_hash = util.sha1(self.unique_string + name + password, False)
		self.db.exe("INSERT OR REPLACE INTO mods (name, password_hash, admin, created, last_active) VALUES (?, ?, ?, inow(), inow())", name, buffer(password_hash), int(admin))
		self.mods[name] = (password_hash, admin)
		return password
	
	
	def add_default_mod(self):
		name, password = common.Mod.DEFAULT_LOGIN
		password_hash = util.sha1(self.unique_string + name + password, False)
		self.db.exe("INSERT OR REPLACE INTO mods (name, password_hash, admin, created, last_active) VALUES (?, ?, 1, inow(), inow())", name, buffer(password_hash))
		self.mods[name] = (password_hash, True)
	

	def is_admin(self, name):
		return name in self.mods and self.mods[name][1]


	def is_mod(self, name):
		return name in self.mods


	def get_mod(self, name, password):
		""" if the name+password is valid as a mod then the name is returned, otherwise None. """
		if name in self.mods and util.sha1(self.unique_string + name + password, False) == self.mods[name][0]:
			return name
		else:
			return None
	
	
	def is_default_mod(self, name):
		return name == common.Mod.DEFAULT_LOGIN[0] and \
			name in self.mods and \
			self.mods[name][0] == util.sha1(self.unique_string + name + common.Mod.DEFAULT_LOGIN[1], False)
	
	
	def reset_mod_password(self, name):
		password = util.rand_str(12)
		self.set_mod_password(name, password)
		return password
	
	
	def set_mod_password(self, name, password):
		password_hash = util.sha1(self.unique_string + name + password, False)
		self.db.exe("UPDATE mods SET password_hash = ? WHERE name = ?", buffer(password_hash), name)
		self.mods[name] = (password_hash, self.mods[name][1])
	
	
	def set_mod_admin(self, name, admin):
		self.db.exe("UPDATE mods SET admin = ? WHERE name = ?", int(admin), name)
		self.mods[name] = (self.mods[name][0], admin)
	
	
	def delete_mod(self, name):
		self.db.exe("DELETE FROM mods WHERE name = ?", name)
		del self.mods[name]
	
	
	def mod_active(self, name):
		self.db.exe("UPDATE mods SET last_active = inow() WHERE name = ?", name)
	
	
	def get_board(self, dir):
		for board in self.boards:
			if board.dir == dir:
				return board
		return None
	
	
	def public_boards(self):
		return filter(lambda b: not b.config.mod_only, self.boards)
	
	
	def login_attempt(self, ip):
		""" register a login attempt for ip. return if this attempt should be allowed. """
		with self.db.write:
			self.db.exe("DELETE FROM login_attempts WHERE time < ?", time.time() - common.LOGIN_RESTRICT_SECONDS)
			self.db.exe("INSERT OR REPLACE INTO login_attempts (ip, time) VALUES (?, inow())", ip)
			return self.db.one("SELECT COUNT(*) as c FROM login_attempts WHERE ip = ?", ip).c <= common.LOGIN_RESTRICT_ATTEMPTS
	
	
	def get_index_data(self, mod = False):
		data = ObjDict(threads = [], new_post = NEW_POST, on_thread_page = False)
		for board in self.boards:
			if not board.config.mod_only or mod:
				for thread in board.get_index_data():
					data.threads.append(ObjDict(board_db = board, thread = thread))
		data.threads.sort(key = lambda e: e.thread.updated, reverse = True)
		return data
	
	
	def parsed_comment_cache_size(self):
		return sum(len(b._parsed_comment_cache) for b in self.boards)
	
	
	def get_config_area_existing_boards_data(self):
		return ObjDict(boards = [(b, b.get_config_area_existing_boards_data()) for b in self.boards])
	
	
	def get_config_area_mods_data(self):
		rows = self.db.all("SELECT format_time(created) as created, format_time(last_active) as last_active, name, admin FROM mods ORDER BY name")
		return ObjDict(mods = rows)
	
	
	def get_config_area_modlog_data(self, offset = None):
		with self.db.read:
			num_entries = self.db.one("SELECT COUNT(*) AS count FROM modlog").count
			
			if offset is None:
				offset = num_entries-common.MODLOG_ENTRIES_PER_PAGE
			else:
				offset = util.sint(offset)
			
			if offset > num_entries-common.MODLOG_ENTRIES_PER_PAGE:
				offset = num_entries-common.MODLOG_ENTRIES_PER_PAGE
			if offset < 0:
				offset = 0
			
			q = """	SELECT * FROM (
						SELECT format_time(time) as time, time as raw_time, mod_name, message FROM modlog ORDER BY raw_time LIMIT ?, ?
					) ORDER BY raw_time DESC """
			rows = self.db.all(q, offset, common.MODLOG_ENTRIES_PER_PAGE)
			return ObjDict(num_entries = num_entries, offset = offset, entries = rows)
	
	
	def get_config_area_bans_data(self, filter_str):
		with self.db.read:
			
			total_bans = self.db.one("SELECT COUNT(*) AS c FROM bans WHERE expires = 0 OR expires > inow()").c
			
			if filter_str:
				q = """ SELECT ip, format_time(created) AS created, CASE WHEN expires = 0 THEN 0 ELSE expires - inow() END as expires, reason, board_name
						FROM bans
						WHERE (expires = 0 OR expires > inow()) AND ip = ?
						ORDER BY expires > 0, expires, ip"""
				bans = self.db.all(q, filter_str)
			else:
				q = """	SELECT ip, format_time(created) AS created, CASE WHEN expires = 0 THEN 0 ELSE expires - inow() END as expires, reason, board_name
						FROM bans
						WHERE expires = 0 OR expires > inow()
						ORDER BY expires > 0, expires, ip"""
				bans = self.db.all(q)
			
			for ban in bans:
				ban.expires = util.format_interval(ban['expires'], 2)
			
			return ObjDict(bans = bans, filter = filter_str, total_bans = total_bans)
	
	
	def get_config_area_files_data(self):
		return self.db.one("SELECT COUNT(*) AS num_file_bans FROM file_bans")
		
	
	def get_config_area_posts_by_ip_data(self, ip):
		boards = []
		for board in self.boards:
			data = board.get_config_area_posts_by_ip_data(ip)
			if data.threads:
				boards.append((board, data))
		return ObjDict(boards = boards)
	
	
	def get_config_area_reviewed_posts_data(self):
		boards = []
		for board in self.boards:
			data = board.get_config_area_reviewed_posts_data()
			if data.reviewed_posts:
				boards.append((board, data.reviewed_posts))
		return ObjDict(boards = boards)
	
	
	def get_index_rss_data(self, server_name):
		items = []
		for board_db in self.public_boards():
			items.extend(board_db.get_board_rss_data(server_name).items)
		items.sort(key = lambda e: e.pub_date, reverse = True)
		return ObjDict( title = self.board_config_defaults.title,
						description = util.strip_html(self.board_config_defaults.desc),
						link = 'http://%s/' % server_name,
						pub_date = time.time(),
						items = items	)
	
	
	def get_flags(self):
		return [(b.dir, b.get_flags()) for b in self.boards]
	
	
	def move_thread(self, src_board, src_thread_id, dest_board, keep_locked_copy):
		_ = self._

		# grab data from src database
		with src_board.db.read:
			thread = src_board.db.one("SELECT * FROM threads WHERE id = ?", src_thread_id)
			posts = src_board.db.all("SELECT * FROM posts WHERE thread_id = ?", src_thread_id)
		
		if not thread:
			raise ValueError(_('Thread %d does not exist.') % src_thread_id)
	
		# put data into dest database
		with dest_board.db.write:
		
			q = "INSERT INTO threads (subject, sticky, locked, bumpable, updated) VALUES (?, ?, ?, ?, inow())"
			dest_board.db.exe(q, thread.subject, thread.sticky, thread.locked, thread.bumpable)
			dest_thread_id = dest_board.db.last_row_id()
			
			for post in posts:
				q = """	INSERT INTO posts (thread_id, id, posted, status, name, tripcode, mod, comment, file_md5, file_meta, ip)
						VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
				dest_board.db.exe(q, dest_thread_id, post.id, post.posted, post.status, post.name, post.tripcode, post.mod, post.comment, post.file_md5, post.file_meta, post.ip)
				if post.file_meta:
					uploaded_files.copy(	_json_decode(post.file_meta),
											(src_board, src_thread_id, post.id),
											(dest_board, dest_thread_id, post.id)	)
		
		if keep_locked_copy:
			# link src to dest thread and lock src
			comment = _("Please continue this thread in >>/%s/%d") % (dest_board.dir, dest_thread_id)
			try:
				src_board.add_new_reply(src_thread_id, int(time.time()), '', None, True, comment, None, None, None, False, None, True, None)
			except:
				pass
		else:
			src_board.delete_thread(src_thread_id)
		
		return dest_thread_id
	
	
	def get_ban(self, ip):
		q = """	SELECT ip, format_time(created) AS created, board_name, CASE WHEN expires = 0 THEN 'the end of time' ELSE format_time(expires) END AS expires, reason
				FROM bans
				WHERE (ip = ? OR fqdn(?) GLOB ip) AND (expires = 0 OR expires > inow())
				LIMIT 1	"""
		return self.db.one(q, ip, ip)
		
	
	def add_ban(self, ip, expires, reason, board_name=''):
		""" adds a ban, overwriting any existing one. """
		q = "INSERT OR REPLACE INTO bans (ip, created, expires, reason, board_name) VALUES (?, ?, ?, ?, ?)"
		self.db.exe(q, ip, time.time(), expires, reason, board_name)
	
	
	def extend_ban(self, ip, expires, reason, board_name=''):
		""" adds a ban, overwriting an existing one only if the new expires time is longer. """
		with self.db.write:
			row = self.db.one("SELECT expires FROM bans WHERE ip = ?", ip)
			if not row:
				self.db.exe("INSERT INTO bans (ip, created, expires, reason, board_name) VALUES (?, ?, ?, ?, ?)", ip, time.time(), expires, reason, board_name)
			elif row.expires and (not expires or expires > row.expires):
				self.db.exe("UPDATE bans SET expires = ? WHERE ip = ?", expires, ip)


	def delete_ban(self, ip):
		self.db.exe("DELETE FROM bans WHERE ip = ?", ip)
	
	
	def delete_expired_bans(self):
		self.db.exe("DELETE FROM bans WHERE expires != 0 AND expires <= inow()")
		
		
	def file_banned(self, md5):
		return self.db.one("SELECT 1 FROM file_bans WHERE md5 = ?", md5) is not None
	
	
	def add_file_ban(self, md5):
		self.db.exe("INSERT OR IGNORE INTO file_bans (md5) VALUES (?)", buffer(md5))
	
	
	def ban_files(self, md5s):
		with self.db.write:
			for md5 in md5s:
				self.db.exe("INSERT OR IGNORE INTO file_bans (md5) VALUES (unhex(?))", md5)
	
	
	def unban_files(self, md5s):
		with self.db.write:
			for md5 in md5s:
				self.db.exe("DELETE FROM file_bans WHERE md5 = unhex(?)", md5)
	
		
	def num_ip_posts(self, ip):
		return sum(b.num_ip_posts(ip) for b in self.boards)

	"""Number of visitors"""



	dict_of_board_visitors = defaultdict(dict)

	def new_visitor(self, board_name, ip, thrd=0):
		self.dict_of_board_visitors[board_name][ip] = (int(time.time()), thrd)
		
	def visitors_update(self):
		for board in list(self.dict_of_board_visitors):
			for ip in list(self.dict_of_board_visitors[board]):
				if int(time.time()) - self.dict_of_board_visitors[board][ip][0] > 10:
					del self.dict_of_board_visitors[board][ip] # delete all 10 sec old posts
		print(self.dict_of_board_visitors)

	def get_number_of_visitors(self):
		res = {}
		for board in self.boards:
			res[board.config.title] = len(self.dict_of_board_visitors[board.config.title])
		return res

	def get_number_of_visitors_for_thread(self, board_name, thrd):
		res = 0
		for ip in self.dict_of_board_visitors[board_name]:
			if self.dict_of_board_visitors[board_name][ip][1] == thrd:
				res += 1
		return res

	def get_last_visitors(self):
		res = self.dict_of_board_visitors
		return res

	def get_last_visitors(self):
		res = self.dict_of_board_visitors
		return res

	reader = geoip2.database.Reader("databases/GeoLite2-City.mmdb")
		
	def ip_country(self, ip):
		try:
			res = reader.city(ip).country.name
		except Exception:
			res = 'Address Not Found'
		return res


class Board(object):
	"""
	each board has its own database file and a single Board object for that file.
	this object contains cached config data and has methods for getting other data from the database
		
	Attributes:
		global_db
		dir
		config
		use_config_defaults
	"""
	
	CURRENT_VERSION = 4
	
	CURRENT_TABLEES = """
		CREATE TABLE IF NOT EXISTS board (
			version INT NOT NULL
		);
		
		CREATE TABLE IF NOT EXISTS config (
			name TEXT PRIMARY KEY NOT NULL,
			value NOT NULL,
			use_default INTEGER NOT NULL DEFAULT 1
		);
		
		CREATE TABLE IF NOT EXISTS threads (
			id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
			subject TEXT NOT NULL,
			sticky INTEGER NOT NULL,
			locked INTEGER NOT NULL,
			bumpable INTEGER NOT NULL,
			updated INTEGER NOT NULL
		);
		CREATE INDEX IF NOT EXISTS threads_updated ON threads (updated);
		
		CREATE TABLE IF NOT EXISTS posts (
			thread_id INTEGER NOT NULL,
			id INTEGER NOT NULL,
			posted INTEGER NOT NULL,
			name TEXT NOT NULL,
			tripcode TEXT,
			mod INTEGER NOT NULL,
			ip TEXT,
			comment TEXT NOT NULL,
			file_md5 BLOB,
			file_meta BLOB,
			status INTEGER NOT NULL,
			PRIMARY KEY (thread_id, id)
		);
		CREATE INDEX IF NOT EXISTS posts_posted ON posts (posted);
		CREATE INDEX IF NOT EXISTS posts_file_md5 ON posts (file_md5);
		
		CREATE TABLE IF NOT EXISTS flags (
			thread_id INTEGER NOT NULL,
			post_id INTEGER NOT NULL,
			ip TEXT NOT NULL,
			PRIMARY KEY (thread_id, post_id, ip)
		);
		
		CREATE TABLE IF NOT EXISTS reviewed_posts (
			thread_id INTEGER NOT NULL,
			post_id INTEGER NOT NULL,
			time INTEGER NOT NULL,
			PRIMARY KEY (thread_id, post_id)
		);
	"""
			
	_ = lambda x: x	#make pygettext think name/desc fields need translating - they do, but they dont get replaced just yet
	CONFIG_FIELDS = (
		('title', fields.String(_('Title'), 0, 256, 'img2')),
		('desc', fields.String(_('Description'), 0, 1024*1024, 'This is an img2 imageboard.', _('HTML is valid here.'), 13)),
	
		('css', fields.SelectField(_('CSS'), common.available_css, 'futaba.css')),
		('show_age_check_page', fields.Bool(_('Show Age Check Page'), False)),
		
		('mod_only', fields.Bool(_('Mod Only'), False, _('If set, only mods can view or post to this board.'))),
		 
		('default_first_names', fields.String(_('Default First Names'), 0, 1024, '', _('One name per line. When no name is entered a default name will be created by adding <random-default-first-name> + \" \" + <random-default-second-name>'), 6)),
		('default_second_names', fields.String(_('Default Second Names'), 0, 1024, '', _('One name per line.'), 6)),

		('post_filters', fields.String(_('Post Filters'), 0, 1024*1024, '', _('One filter per line. <string-to-find> + \" \" + <string-to-replace-with>. Can contain HTML.'), 6)),

		('allow_images', fields.Bool(_('Allow Images'), True, _('Allow Image Uploads.'))),
		('allow_flashes', fields.Bool(_('Allow Flashes'), True, _('Allow Flash Uploads.'))),
		('allow_torrents', fields.Bool(_('Allow Torrents'), True, 'Allow Torrent Uploads.')),
		('max_file_length', fields.Int(_('Max File Length'), 0, 32*1024*1024, 2*1024*1024, _('In Bytes.'))),
		('allow_duplicate_files', fields.Bool(_('Allow Duplicate Files'), True)),
		
		('allow_names', fields.Bool(_('Allow Names'), True, _('If not set a name will be selected from the default names if there are any.'))),
		('allow_tripcodes', fields.Bool(_('Allow Tripcodes'), True)),
		('allow_thread_without_file', fields.Bool(_('Allow Thread Without File'), True)),
		('allow_reply_without_file', fields.Bool(_('Allow Reply Without File'), True)),
		('allow_no_comment_with_file', fields.Bool(_('Allow No Comment With File'), True, _('If set textless posts will be allowed as long as a file is given.'))),

		('gmt_offset', fields.Int(_('GMT Offset'), 0, 23, 0, _('Affects displayed times/dates. In hours.'))),
		('locale', fields.SelectField(_('Locale'), common.available_locales, '', _('Controls which set of translated strings will be used for this board, if any.'))),
	
		('max_image_size', fields.Int(_('Max Image Width/Height'), 16, 1024*32, 4096, _('In Pixels.'))),
		('min_image_size_for_thumbnailing', fields.Int(_('Min Image Size For Thumbnailing'), 8, 1024*32, 390, _('Images this big or bigger (pixels) will be thumbnailed.'))),
		('max_image_thumb_size', fields.Int(_('Max Image Thumbnail Size'), 8, 1024*32, 300, _('Thumbnails will be created at this size (pixels).'))),
		('clean_images', fields.Bool(_('Clean Images'), True, _('Remove exif data and embedded files (eg. rars) from image uploads.'))),
		
		('max_comment_length', fields.Int(_('Max Comment Length'), 8, 1024**2, 1024, _('Max allowed comment length.'))),
		('max_subject_length', fields.Int(_('Max Subject Length'), 8, 1024**2, 64, _('Max allowed subject length.'))),
		('max_name_length', fields.Int(_('Max Name Length'), 8, 1024**2, 32, _('Max allowed name length'))),
		('max_emote_length', fields.Int(_('Max Emote Length'), 0, 1024**2, 8, _('Posts with length <= this will be wrapped in <div class=\"emote\">'))),
		('max_youtube_embeds_per_post', fields.Int(_('Max YouTubes Per Post'), 0, 4096, 1)),
		('posted_url_prefix', fields.String(_('Posted URL Prefix'), 0, 1024, '', _('Prefixed on to auto-linked URLs. For referer removal services.'))),
		
		('min_replies_for_omitting', fields.Int(_('Min Replies For Omitting'), 2, 1024*1024, 12, _('If there are this many or more replies then some will be omitted on the thread preview.'))),
		('num_replies_to_show_when_omitting', fields.Int(_('Num Replies To Show When Omitting'), 1, 1024*1024, 6)),
		
		('min_seconds_between_posts', fields.Int(_('Min Seconds Between Posts'), 0, 1024*1024, 10, _('Flood Control, per IP.'))),
		('min_seconds_between_threads', fields.Int(_('Min Seconds Between Threads'), 0, 1024*1024, 30, _('Flood Control, per IP.'))),

		('thread_captcha', fields.Bool(_('Thread Captcha'), False, _('Require a captcha for starting a thread?'))),
		('reply_captcha', fields.Bool(_('Post Captcha'), False, _('Require a captcha for replying to a thread?'))),
		('javascript_anti_spam', fields.Bool(_('Javascript Anti-Spam'), True, _('Uses Javascript to prevent stupid spam bots (and people without Javascript enabled) from posting.'))),

		('proxy_dnsbls', fields.String(_('Proxy DNSBLs'), 0, 1024**2, 'torexit.dan.me.uk', _('List of DNS blacklists used to identify proxies. One per line.'), 6)),
		('block_proxies_files', fields.Bool(_('Block Proxies Files'), True, _('Don\'t allow proxies to post files.'))),
		('block_proxies_fully', fields.Bool(_('Block Proxies Fully'), False, _('Don\'t allow proxies to post at all.'))),

		('threads_per_page', fields.Int(_('Threads Per Page'), 1, 1024, 12, _('Num. thread-previews shown on each board page.'))),
		('max_thread_posts', fields.Int(_('Max Thread Posts'), 1, 1024*1024, 1001, _('Includes thread head and replies.'))),
		
		('max_threads', fields.Int(_('Max Threads'), 10, 1024*1024, 250, _('Num threads at any one time are limited to this - old ones are deleted.'))),
		
		('max_threads_on_front_page', fields.Int(_('Max Threads On Front Page'), 0, 1024, 4, _('Max number of threads from this board to show on the front page.'))),
		('max_days_old_for_front_page_threads', fields.Int(_('Max Days Old For Front Page Threads'), 0, 1024*1024, 12, _('Max age of threads shown on the front page.'))),
		
		('hours_to_keep_post_ips', fields.Int(_('Hours To Keep Post IPs'), 0, 1024*1024, 24*7, _('After this time poster ips will be wiped from the database.'))),
		
		('default_ban_reason', fields.String(_('Default Ban Reason'), 0, 2048, 'Not respecting the rules.', _('This will be used on bans set by the quick-access links on posts.'))),
		('default_ban_length', fields.Interval(_('Default Ban Length'), 0, 60*60*24*365*4, 0, _('Same as above. This field should be formatted as described on the ban page.'))),
	)
	
	def __init__(self, global_db, dir):
		self.global_db = global_db
		self.dir = dir.decode('utf-8') if not isinstance(dir, unicode) else dir
		self._local = threading.local()
		
		#ensure uploaded_files/dir exists - required even if not used
		try:
			os.makedirs(os.path.join(common.UPLOADED_FILE_DIR, dir))
		except:
			pass
		
		#init caches
		
		db = self._update()
		with db.write:
		
			self.config = ObjDict()
			self.use_config_defaults = ObjDict()
			for name, field in self.CONFIG_FIELDS:
				row = db.one("SELECT value, use_default FROM config WHERE name = ?", name)
				if row:
					self.use_config_defaults[name] = bool(row.use_default)
					self.config[name] = global_db.board_config_defaults[name] if row.use_default else row.value
				else:
					db.exe("INSERT INTO config (name, value, use_default) VALUES (?, ?, 1)", name, field.default)
					self.use_config_defaults[name] = True
					self.config[name] = global_db.board_config_defaults[name]
		
		self._update_parsed_config()
		self._parsed_comment_cache = cache.Cache(5000)	# (thread_id, post_id) => parsed comment html, exists because post parsing was a bottleneck

	
	def _update(self):
		""" make sure the database version is current """
		db = _SQLiteDB(os.path.join(common.DATABASE_DIR, 'boards', '%s.sqlite' % self.dir))
		
		try:
			v = db.one("SELECT version FROM board").version
		except (sqlite3.OperationalError, AttributeError):		# no versioning on this db version...
			
			# first upgrade to the version DIRECTLY BEFORE THE ONE when versioning started...
			db.update_tables("""
				CREATE TABLE IF NOT EXISTS config (
					name TEXT PRIMARY KEY NOT NULL,
					value NOT NULL,
					use_default INTEGER NOT NULL DEFAULT 1
				);
				CREATE TABLE IF NOT EXISTS threads (
					id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
					forked_thread_id INTEGER,
					forked_post_id INTEGER,
					subject TEXT NOT NULL,
					sticky INTEGER NOT NULL,
					locked INTEGER NOT NULL,
					updated INTEGER NOT NULL
				);
				CREATE INDEX IF NOT EXISTS threads_updated ON threads (updated);
				CREATE INDEX IF NOT EXISTS threads_forked ON threads (forked_thread_id, forked_post_id);
				CREATE TABLE IF NOT EXISTS posts (
					thread_id INTEGER NOT NULL,
					id INTEGER NOT NULL,
					posted INTEGER NOT NULL,
					name TEXT NOT NULL,
					tripcode TEXT,
					mod INTEGER NOT NULL,
					ip TEXT,
					comment TEXT NOT NULL,
					file_md5 BLOB,
					file_meta BLOB,
					status INTEGER NOT NULL,
					PRIMARY KEY (thread_id, id)
				);
				CREATE INDEX IF NOT EXISTS posts_posted ON posts (posted);
				CREATE INDEX IF NOT EXISTS posts_file_md5 ON posts (file_md5);
				CREATE TABLE IF NOT EXISTS flags (
					thread_id INTEGER NOT NULL,
					post_id INTEGER NOT NULL,
					ip TEXT NOT NULL,
					PRIMARY KEY (thread_id, post_id, ip)
				);
				CREATE TABLE IF NOT EXISTS reviewed_posts (
					thread_id INTEGER NOT NULL,
					post_id INTEGER NOT NULL,
					time INTEGER NOT NULL,
					PRIMARY KEY (thread_id, post_id)
				);
			""")
			# then upgrade from that version to the one with a version field :D
			db.exe("CREATE TABLE board (version INT NOT NULL)")
			db.exe("INSERT INTO board (version) VALUES (1)")
			v = 1
				
		def _1_2():
			""" file meta data changed from pickled python objects to json dicts """
			import util.http
			
			with db.write:
				#in sqlite you have to drop/recreate the entire table to change the type of a column
				posts = db.all("SELECT * FROM posts")
				
				db.exe("DROP TABLE posts")	#also drops indices
				db.exe("""
					CREATE TABLE posts (
						thread_id INTEGER NOT NULL,
						id INTEGER NOT NULL,
						posted INTEGER NOT NULL,
						name TEXT NOT NULL,
						tripcode TEXT,
						mod INTEGER NOT NULL,
						ip TEXT,
						comment TEXT NOT NULL,
						file_md5 BLOB,
						file_meta TEXT,
						status INTEGER NOT NULL,
						PRIMARY KEY (thread_id, id)
					)""")
				db.exe("CREATE INDEX IF NOT EXISTS posts_posted ON posts (posted)")
				db.exe("CREATE INDEX IF NOT EXISTS posts_file_md5 ON posts (file_md5)")
				
				for post in posts:
					
					# convert saved meta data from pickled object to json
					if post.file_meta:
						# hack required to unpickle old meta data
						class SWFFile:
							pass
						class ImageFile:
							pass
						class TorrentFile:
							pass
						class TorrentFileEntry:
							pass
						uploaded_files.SWFFile = SWFFile
						uploaded_files.ImageFile = ImageFile
						uploaded_files.TorrentFile = TorrentFile
						uploaded_files.TorrentFileEntry = TorrentFileEntry
						try:
							dir = os.path.join(common.UPLOADED_FILE_DIR, self.dir, str(post.thread_id), str(post.id))
							name = filter(lambda f: f != 'thumb', os.listdir(dir))[0]
							flen = os.path.getsize(os.path.join(dir, name))
							
							obj = pickle.loads(str(post.file_meta))
							if isinstance(obj, SWFFile):
								obj = {'type': uploaded_files.FLASH, 'name': obj.name, 'length': flen}
							elif isinstance(obj, ImageFile):
								if obj.thumb_size:
									obj.thumb_size = map(int, obj.thumb_size)
								obj = {'type': uploaded_files.IMAGE, 'name': obj.name, 'length': flen, 'size': obj.size, 'thumb_size': obj.thumb_size}
							elif isinstance(obj, TorrentFile):
								obj = {	'type': uploaded_files.TORRENT,
										'name': obj.name,
										'length': flen,
										'info_hash': util.hex(obj.info_hash),
										'trackers': obj.trackers,
										'files': [{'name': f.name, 'length': f.length} for f in obj.files],
										'files_not_shown': 0,
										'comment': obj.comment,
										'created_by': obj.created_by,
										'creation_time': obj.creation_time	}
						except:
							logging.exception('error converting file meta data')
							post.file_meta = None
						else:
							post.file_meta = json.encode(obj)
				
					keys = post.keys()
					values = [post[k] for k in keys]
					db.exe("INSERT INTO POSTS (" + ','.join(keys) + ") VALUES (" + ','.join('?' * len(post)) + ")", *values)
		
		def _2_3():
			""" threads got a `bumpable` field """
			db.exe("ALTER TABLE threads ADD COLUMN bumpable INTEGER NOT NULL DEFAULT 1")
		
		def _3_4():
			""" forking was removed """
			threads = db.all("SELECT * FROM threads")
			
			db.exe("DROP TABLE threads")
			db.exe("""
					CREATE TABLE threads (
						id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
						subject TEXT NOT NULL,
						sticky INTEGER NOT NULL,
						locked INTEGER NOT NULL,
						bumpable INTEGER NOT NULL,
						updated INTEGER NOT NULL
					)
					""")
			db.exe("CREATE INDEX threads_updated ON threads (updated)")
			
			for t in threads:
				db.exe("INSERT INTO threads (id, subject, sticky, locked, bumpable, updated) VALUES (?, ?, ?, ?, ?, ?)", t.id, t.subject, t.sticky, t.locked, t.bumpable, t.updated)

		while v < self.CURRENT_VERSION:
			locals()['_%d_%d' % (v, v+1)]()
			v += 1
			db.exe("UPDATE board SET version = ?", v)
			
		
		return db

	
	# using a def makes -g give a warning...
	_ = lambda self, key: i18n.catalog(common.LOCALE_DIR, self.config.locale)[key]


	def _update_parsed_config(self):
		self.config.default_first_names_parsed = self.config.default_first_names.splitlines()
		self.config.default_second_names_parsed = self.config.default_second_names.splitlines()
		froms = {}
		for line in self.config.post_filters.splitlines():
			f, _, t = line.partition(' ')
			if f and t:
				froms[f] = t
		self.config.post_filters_parsed = (froms, re.compile('|'.join(map(re.escape, froms.keys()))))
	
	
	def _get_parsed_comment(self, comment, thread_id, post_id):
		return self._parsed_comment_cache.get((thread_id, post_id), lambda: parser.parse(comment, (self, thread_id, post_id)))


	def _delete_parsed_comment(self, thread_id, post_id):
		return self._parsed_comment_cache.delete((thread_id, post_id))


	def __getattr__(self, name):
		if name == 'db':
			if not hasattr(self._local, 'db'):
				self._local.db = _SQLiteDB(os.path.join(common.DATABASE_DIR, 'boards', '%s.sqlite' % self.dir))
				self._local.db.create_functions(self.config.gmt_offset)
			return self._local.db
		raise AttributeError(name)
			

	def set_config(self, key, value, use_default):
		""" assumes value is valid for the field named by key. """
		self.db.exe("INSERT OR REPLACE INTO config (name, value, use_default) VALUES (?, ?, ?)", key, value, int(use_default))
		self.config[key] = self.global_db.board_config_defaults[key] if use_default else value
		self.use_config_defaults[key] = use_default
		self._update_parsed_config()


	def get_default_name(self):
		f = self.config.default_first_names_parsed
		s = self.config.default_second_names_parsed
		if f and s:
			return random.choice(f) + ' ' + random.choice(s)
		elif f:
			return random.choice(f)
		elif s:
			return random.choice(s)
		else:
			return ''

	
	def files_allowed(self):
		return self.config.allow_images or self.config.allow_torrents or self.config.allow_flashes
	

	def file_exts(self):
		e = []
		if self.config.allow_images:
			e += uploaded_files.IMAGE_FILE_EXTS
		if self.config.allow_torrents:
			e += uploaded_files.TORRENT_FILE_EXTS
		if self.config.allow_flashes:
			e += uploaded_files.FLASH_FILE_EXTS
		return e
	
	
	def file_dir(self):
		return os.path.join(common.UPLOADED_FILE_DIR, self.dir)

		
	def get_index_data(self):
		""" similar to get_view_board_data, some subtle sql diffs tho. """
		with self.db.read:
			#get list of threads
			q = """	SELECT *,
					(SELECT COUNT(*) FROM posts WHERE posts.thread_id = threads.id) AS num_posts,
					(SELECT COUNT(*) FROM posts WHERE posts.thread_id = threads.id AND file_md5 IS NOT NULL) AS num_files
					FROM threads
					WHERE updated > ?
					ORDER BY updated DESC LIMIT ?"""
			threads = self.db.all(q, time.time() - 60*60*24*self.config.max_days_old_for_front_page_threads, self.config.max_threads_on_front_page)
			#get list of posts for each thread
			q = """	SELECT * FROM (
						SELECT thread_id, id, ip, format_time(posted) AS posted, name, tripcode, mod, status, comment, file_meta
						FROM posts
						WHERE thread_id = ?
						ORDER BY (id = 0) DESC, posts.id DESC LIMIT ?
					) ORDER BY id ASC"""
			for thread in threads:
				#get posts data
				if thread.num_posts-1 >= self.config.min_replies_for_omitting:
					limit = self.config.num_replies_to_show_when_omitting + 1
				else:
					limit = -1
				posts = self.db.all(q, thread.id, limit)
				#parse posts for display
				files_shown = 0
				for post in posts:
					if post.file_meta:
						post.file_meta = _json_decode(post.file_meta)
						files_shown += 1
					post.comment = self._get_parsed_comment(post.comment, thread.id, post.id)
				thread.posts = posts
				thread.max_post_id = posts[-1].id
				thread.num_posts_omitted = (posts[1].id-1) if (len(posts) > 1) else 0
				thread.num_files_omitted = thread.num_files - files_shown
			return threads


	def get_view_board_data(self, page):
		with self.db.read:
			q = "SELECT COUNT(*) AS count FROM threads"
			num_threads = self.db.one(q).count
			num_pages = int(math.ceil(num_threads / float(self.config.threads_per_page)))
			pages = range(max(1, num_pages))
			#get list of threads
			q = """	SELECT *,
					(SELECT COUNT(*) FROM posts WHERE posts.thread_id = threads.id) AS num_posts,
					(SELECT COUNT(*) FROM posts WHERE posts.thread_id = threads.id AND file_md5 IS NOT NULL) AS num_files
					FROM threads
					ORDER BY sticky DESC, updated DESC LIMIT ?, ?"""
			threads = self.db.all(q, self.config.threads_per_page * page, self.config.threads_per_page)
			#no threads on this page?
			if page > 0 and len(threads) == 0:
				return None
			#get list of posts for each thread
			q = """	SELECT * FROM (
						SELECT thread_id, id, ip, format_time(posted) AS posted, name, tripcode, mod, status, comment, file_meta
						FROM posts
						WHERE thread_id = ?
						ORDER BY (id = 0) DESC, posts.id DESC LIMIT ?
					) ORDER BY id ASC"""
			for thread in threads:
				#get posts data
				if (thread.num_posts-1) >= self.config.min_replies_for_omitting:
					limit = self.config.num_replies_to_show_when_omitting + 1
				else:
					limit = -1
				posts = self.db.all(q, thread.id, limit)
				#parse posts for display
				files_shown = 0
				for post in posts:
					if post.file_meta:
						post.file_meta = _json_decode(post.file_meta)
						files_shown += 1
					post.comment = self._get_parsed_comment(post.comment, thread.id, post.id)
				thread.posts = posts
				thread.max_post_id = posts[-1].id
				thread.num_posts_omitted = (posts[1].id-1) if (len(posts) > 1) else 0
				thread.num_files_omitted = (thread.num_files - files_shown)
			return ObjDict(board_db = self, threads = threads, cur_page = page, pages = pages, new_post = NEW_POST, new_thread = NEW_THREAD, on_thread_page = False)
	
	
	def get_view_thread_data(self, id):
		with self.db.read:
			#get thread data
			thread = self.db.one("SELECT * FROM threads WHERE id = ?", id)
			if not thread:
				return None
			#get post data
			q = """	SELECT thread_id, id, ip, format_time(posted) AS posted, name, tripcode, mod, status, comment, file_meta
					FROM posts
					WHERE thread_id = ?
					ORDER BY id"""
			posts = self.db.all(q, id)
			thread.blurb = parser.blurb(id, thread.subject, posts[0].status, posts[0].comment)
			#parse for display
			for post in posts:
				post.file_meta = _json_decode(post.file_meta)
				post.comment = self._get_parsed_comment(post.comment, id, post.id)
			thread.posts = posts
			thread.num_posts = len(posts)
			thread.max_post_id = posts[-1].id
			return ObjDict(board_db = self, thread = thread, new_post = NEW_POST, on_thread_page = True)
	
	
	def get_list_data(self):
		q = """	SELECT threads.*, format_time(threads.updated) AS updated,
				posts.status AS head_status, posts.comment AS head_comment, posts.file_meta as head_file_meta,
				(SELECT COUNT(*) FROM posts WHERE thread_id = threads.id) AS num_posts,
				(SELECT COUNT(*) FROM posts WHERE thread_id = threads.id AND file_md5 IS NOT NULL) AS num_files
				FROM threads, posts
				WHERE posts.thread_id = threads.id AND posts.id = 0
				ORDER BY sticky DESC, updated DESC"""
		threads = self.db.all(q)
		num_pages = int(math.ceil(len(threads) / float(self.config.threads_per_page)))
		pages = range(max(1, num_pages))
		for thread in threads:
			thread.head_file_meta = _json_decode(thread.head_file_meta)
			thread.head_comment = self._get_parsed_comment(thread.head_comment, thread.id, 0)
		return ObjDict(board_db = self, threads = threads, cur_page = -1, pages = pages, new_thread = NEW_THREAD)

	
	def _post_to_item(self, server_name, post):
		link = 'http://%s/%s/%d#%d' % (server_name, self.dir, post.thread_id, post.id)
		if post.status == PostStatus.UNDELETED:
			
			post.file_meta = _json_decode(post.file_meta)
			description = parser.parse(post.comment, (self, post.thread_id, post.id))
			if post.file_meta and post.file_meta.type in (uploaded_files.IMAGE, uploaded_files.FLASH):
				description = common.render_to_string(self, 'includes/uploaded_file.html', {'board_db': self, 'post': post}) + '<br>' + description
			
			return ObjDict(	title = '%d. %s' % (post.id, (util.clamp_str(post.comment, common.MAX_BLURB_LENGTH, '...') + (' <%s>' % post.file_meta.name if post.file_meta else '')).strip()),
							author = '%s/%d%s%s' % (self.dir, post.thread_id, ' ' + post.name if post.name else '', ' [%s]' % post.tripcode if post.tripcode else ''),
							description = description,
							link = link,
							guid = link,
							pub_date = post.posted	)
		else:
			description = self._('Deleted by Mod') if post.status == PostStatus.MOD_DELETED else self._('Deleted by Poster')
			
			return ObjDict(	title = '%d. %s' % (post.id, description),
							author = '',
							description = description,
							link = link,
							guid = link,
							pub_date = post.posted	)


	def get_board_rss_data(self, server_name):
		with self.db.read:
			posts = self.db.all("SELECT * FROM posts ORDER BY posted DESC LIMIT ?", common.MAX_RSS_ITEMS_PER_BOARD)
			return ObjDict(	title = self.config.title,
							description = util.strip_html(self.config.desc),
							link = 'http://%s/%s' % (server_name, self.dir),
							pub_date = posts[0].posted if posts else 0,
							items = [self._post_to_item(server_name, post) for post in posts] )
	

	def get_thread_rss_data(self, server_name, thread_id):
		with self.db.read:
			thread = self.db.one("SELECT * FROM threads WHERE id = ?", thread_id)
			if not thread:
				return None
			posts = self.db.all("SELECT * FROM posts WHERE thread_id = ? ORDER BY id DESC", thread_id)
			return ObjDict(	title = '%s - %s' % (parser.blurb(thread_id, thread.subject, posts[0].status, posts[0].comment), self.config.title),
							description = '',
							link = 'http://%s/%s/%d' % (server_name, self.dir, thread_id),
							pub_date = posts[-1].posted,
							items = [self._post_to_item(server_name, post) for post in posts]	)


	def get_board_config_data(self):
		config = ObjDict()
		for row in self.db.all("SELECT name, value, use_default FROM config"):
			config[row.name] = ObjDict(value = row.value, use_default = row.use_default)
		return ObjDict(board_db = self, config = config)
		
	
	def get_post_data(self, thread_id, post_id):
		with self.db.read:
			q = """	SELECT thread_id, id, ip, format_time(posted) AS posted, name, tripcode, mod, status, comment, file_meta
					FROM posts
					WHERE thread_id = ? AND id = ?"""
			post = self.db.one(q, thread_id, post_id)
			if not post:
				return None
			
			post.file_meta = _json_decode(post.file_meta)
			post.comment = self._get_parsed_comment(post.comment, thread_id, post_id)
		
			# need some thread data too
			q = """	SELECT id, locked, (SELECT COUNT(*) FROM posts WHERE thread_id = threads.id) AS num_posts
					FROM threads
					WHERE id = ?"""
			thread = self.db.one(q, thread_id)
			
			return ObjDict(board_db = self, thread = thread, post = post)
	

	def get_posts_data(self, thread_id, id_start, id_end):
		with self.db.read:
			q = """	SELECT thread_id, id, ip, format_time(posted) AS posted, name, tripcode, mod, status, comment, file_meta
					FROM posts
					WHERE thread_id = ? AND id >= ? AND id < ?
					ORDER BY id"""
			posts = self.db.all(q, thread_id, id_start, id_end)
			if not posts:
				return None
			
			for post in posts:
				post.file_meta = _json_decode(post.file_meta)
				post.comment = self._get_parsed_comment(post.comment, thread_id, post.id)
		
			# need some thread data too
			q = """	SELECT id, locked, (SELECT COUNT(*) FROM posts WHERE thread_id = threads.id) AS num_posts
					FROM threads
					WHERE id = ?"""
			thread = self.db.one(q, thread_id)
			thread.posts = posts
			
			return ObjDict(board_db = self, thread = thread, on_thread_page = False)


	def get_new_reply_data(self, thread_id):
		q = """	SELECT id, locked, sticky, bumpable,
				(SELECT MAX(id)+1 FROM posts WHERE thread_id = threads.id) AS next_id
				FROM threads
				WHERE id = ?"""
		thread = self.db.one(q, thread_id)
		return None if thread is None else ObjDict(thread = thread)
	
	
	def add_new_thread(self, subject, sticky, locked, bumpable, posted, name, tripcode, mod, comment, file_md5, file_meta, ip, ignore_constraints = False):
		""" either return ([id-of-deleted-thread, ...], id-of-new-thread) or raise DoubleThread/Flooding/DuplicateFile/BannedFile """
		with self.db.write:
			
			file_md5 = buffer(file_md5) if file_md5 else None
			file_meta = json.encode(file_meta) if file_meta else None
			
			if not ignore_constraints:
				
				if self.global_db.file_banned(file_md5):
					raise BannedFile
				if (not self.config.allow_duplicate_files) and self.file_exists(file_md5):
					raise DuplicateFile
			
				q = "SELECT 1 FROM posts WHERE ip = ? AND id = 0 AND ((comment != '' AND comment = ?) OR (file_md5 IS NOT NULL AND file_md5 = ?))"
				if self.db.one(q, ip, comment, file_md5):
					raise DoubleThread
				
				q = "SELECT 1 FROM posts WHERE ip = ? AND id = 0 AND posted > ? LIMIT 1"
				if self.db.one(q, ip, time.time() - self.config.min_seconds_between_threads):
					raise Flooding
						
			q = "INSERT INTO threads (subject, sticky, locked, bumpable, updated) VALUES (?, ?, ?, ?, ?)"
			self.db.exe(q, subject, sticky, locked, bumpable, posted)
			thread_id = self.db.last_row_id()
				
			q = "INSERT INTO posts (thread_id, id, posted, status, name, tripcode, mod, comment, file_md5, file_meta, ip) VALUES (?, 0, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
			self.db.exe(q, thread_id, posted, PostStatus.UNDELETED, name, tripcode, mod, comment, file_md5, file_meta, ip)
			
			#delete old threads
			q = """	SELECT id FROM threads
					ORDER BY sticky DESC, updated DESC
					LIMIT ?, -1"""
			dt = self.db.all(q, self.config.max_threads)
			for t in dt:
				self._delete_thread(t.id)
			deleted_threads = [t.id for t in dt]
			
			return deleted_threads, thread_id
	

	def edit_thread(self, thread_id, subject, sticky, locked, bumpable):
		q = "UPDATE threads SET subject = ?, sticky = ?, locked = ?, bumpable = ? WHERE id = ?"
		self.db.exe(q, subject, sticky, locked, bumpable, thread_id)
	
	
	def delete_thread(self, thread_id):
		with self.db.write:
			self._delete_thread(thread_id)

	
	def _delete_thread(self, thread_id):
		self.db.exe("DELETE FROM threads WHERE id = ?", thread_id)
		#delete files
		q = "SELECT id, file_meta FROM posts WHERE thread_id = ? AND file_md5 IS NOT NULL"
		files = self.db.all(q, thread_id)
		for f in files:
			uploaded_files.delete(self, thread_id, f.id, json.decode(f.file_meta))
		try:
			shutil.rmtree(os.path.join(self.file_dir(), str(thread_id)), True)
		except:
			pass
		self.db.exe("DELETE FROM posts WHERE thread_id = ?", thread_id)
		self.db.exe("DELETE FROM flags WHERE thread_id = ?", thread_id)
		self.db.exe("DELETE FROM reviewed_posts WHERE thread_id = ?", thread_id)
	
		
	def edit_post(self, thread_id, post_id, status, name, tripcode, mod, comment, delete_file, new_file_md5, new_file_meta, reviewed):
		with self.db.write:
			q = "SELECT file_md5, file_meta FROM posts WHERE thread_id = ? AND id = ?"
			row = self.db.one(q, thread_id, post_id)
			if row:
				
				if delete_file and row.file_md5:
					file_meta = _json_decode(row.file_meta)
					uploaded_files.delete(self, thread_id, post_id, file_meta)
					q = "UPDATE posts SET file_md5 = NULL, file_meta = NULL WHERE thread_id = ? AND id = ?"
					self.db.exe(q, thread_id, post_id)
				
				if new_file_md5:
					q = "UPDATE posts SET file_md5 = ?, file_meta = ? WHERE thread_id = ? AND id = ?"
					self.db.exe(q, buffer(new_file_md5), json.encode(new_file_meta), thread_id, post_id)
				
				q = "UPDATE posts SET status = ?, name = ?, tripcode = ?, mod = ?, comment = ? WHERE thread_id = ? AND id = ?"
				self.db.exe(q, status, name, tripcode, int(mod), comment, thread_id, post_id)
				
				if reviewed:
					self.db.exe("INSERT OR IGNORE INTO reviewed_posts (thread_id, post_id, time) VALUES (?, ?, inow())", thread_id, post_id)
				
				self._delete_parsed_comment(thread_id, post_id)

	
	def delete_post(self, thread_id, post_id, by_mod = False, delete_thread_if_alone = False):
		with self.db.write:
			self._delete_post(thread_id, post_id, by_mod, delete_thread_if_alone)
	
	
	def _delete_post(self, thread_id, post_id, by_mod = False, delete_thread_if_alone = False):
		q = "SELECT file_meta FROM posts WHERE thread_id = ? AND id = ?"
		data = self.db.one(q, thread_id, post_id)
		if data:
			if data.file_meta:
				uploaded_files.delete(self, thread_id, post_id, _json_decode(data.file_meta))
			q = """	UPDATE posts
					SET status = ?, file_md5 = NULL, file_meta = NULL
					WHERE thread_id = ? AND id = ?"""
			self.db.exe(q, PostStatus.MOD_DELETED if by_mod else PostStatus.USER_DELETED, thread_id, post_id)
			self.db.exe("DELETE FROM flags WHERE thread_id = ? AND post_id = ?", thread_id, post_id)
			self._delete_parsed_comment(thread_id, post_id)
		if delete_thread_if_alone:
			q = "SELECT COUNT(*) AS count FROM posts WHERE thread_id = ? AND status = ?"
			if self.db.one(q, thread_id, PostStatus.UNDELETED).count == 0:
				self._delete_thread(thread_id)

	
	def delete_posts_by_ip(self, ip, threads = None):
		"""
			delete - via _delete_post - all posts with specified ip.
			if threads is not None then deletions will be restricted to the specified thread ids
		"""
		q = "SELECT id, thread_id FROM posts WHERE ip = ?"
		if threads is not None:
			q += " AND thread_id IN (" + ",".join(map(str, threads)) + ")"
		with self.db.write:
			for post in self.db.all(q, ip):
				self._delete_post(post.thread_id, post.id, True, True)


	def extend_post_ip_ban(self, thread_id, post_id, expires, reason):
		q = self.db.one("SELECT ip FROM posts WHERE thread_id = ? AND id = ? AND ip IS NOT NULL", thread_id, post_id)
		if q:
			self.global_db.extend_ban(q.ip, expires, reason)
			return q.ip


	def ban_and_delete_post_file(self, thread_id, post_id):
		with self.db.write:
			post = self.db.one("SELECT file_md5, file_meta FROM posts WHERE thread_id = ? AND id = ? AND file_md5 IS NOT NULL", thread_id, post_id)
			if post:
				uploaded_files.delete(self, thread_id, post_id, _json_decode(post.file_meta))
				self.db.exe("UPDATE posts SET file_md5 = NULL, file_meta = NULL WHERE thread_id = ? AND id = ?", thread_id, post_id)
				self.db.exe("DELETE FROM flags WHERE thread_id = ? AND post_id = ?", thread_id, post_id)
				self.global_db.add_file_ban(str(post.file_md5))


	def add_new_reply(self, thread_id, posted, name, tripcode, mod, comment, file_md5, file_meta, ip, bump, sticky, locked, bumpable, ignore_constraints = False):
		""" either return the id of the post or raise NoThread/ThreadFull/DoublePost/Flooding/DuplicateFile/BannedFile """
		with self.db.write:
			
			post_id = self.db.one("SELECT MAX(id)+1 AS p FROM posts WHERE thread_id = ?", thread_id).p
			if not post_id:
				raise NoThread
			
			file_md5 = buffer(file_md5) if file_md5 else None
			file_meta = json.encode(file_meta) if file_meta else None
			
			if not ignore_constraints:
				
				if self.global_db.file_banned(file_md5):
					raise BannedFile
				if (not self.config.allow_duplicate_files) and self.file_exists(file_md5):
					raise DuplicateFile
			
				q = "SELECT 1 FROM posts WHERE ip = ? AND thread_id = ? AND ((comment != '' AND comment = ?) OR (file_md5 IS NOT NULL AND file_md5 = ?))"
				if self.db.one(q, ip, thread_id, comment, file_md5):
					raise DoublePost
			
				q = "SELECT 1 FROM posts WHERE ip = ? AND posted > ? LIMIT 1"
				if self.db.one(q, ip, time.time() - self.config.min_seconds_between_posts):
					raise Flooding
				
				if post_id+1 > self.config.max_thread_posts:
					raise ThreadFull
			
			q = """	INSERT INTO posts (thread_id, id, status, posted, name, tripcode, mod, comment, file_md5, file_meta, ip)
					VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
			self.db.exe(q, thread_id, post_id, PostStatus.UNDELETED, posted, name, tripcode, mod, comment, file_md5, file_meta, ip)
			
			if sticky is not None:
				self.db.exe("UPDATE threads SET sticky = ? WHERE id = ?", int(sticky), thread_id)
			if locked is not None:
				self.db.exe("UPDATE threads SET locked = ? WHERE id = ?", int(locked), thread_id)
			if bumpable is not None:
				self.db.exe("UPDATE threads SET bumpable = ? WHERE id = ?", int(bumpable), thread_id)
			if bump:
				#can't just update to the current time here because of when posts are transfered
				self.db.exe("UPDATE threads SET updated = ? WHERE id = ? AND bumpable", posted, thread_id)
			
			return post_id


	def flag_post(self, thread_id, post_id, ip):
		with self.db.write:
			q = "SELECT 1 FROM posts WHERE thread_id = ? AND id = ? AND status = ?"
			if self.db.one(q, thread_id, post_id, PostStatus.UNDELETED):
				q = "SELECT 1 FROM reviewed_posts WHERE thread_id = ? AND post_id = ?"
				if not self.db.one(q, thread_id, post_id):
					q = "INSERT OR IGNORE INTO flags (thread_id, post_id, ip) VALUES (?, ?, ?)"
					self.db.exe(q, thread_id, post_id, ip)


	def get_post_options_data(self, thread_id, post_id, ip):
		q = """	SELECT status, thread_id, id AS post_id, tripcode,
				EXISTS (SELECT 1 FROM reviewed_posts WHERE reviewed_posts.thread_id = posts.thread_id AND post_id = posts.id) AS reviewed,
				EXISTS (SELECT 1 FROM flags WHERE flags.thread_id = posts.thread_id AND post_id = posts.id AND flags.ip = ?) AS flagged
				FROM posts
				WHERE thread_id = ? AND id = ?"""
		data = self.db.one(q, ip, thread_id, post_id)
		if data is None:
			return None
		data.board_db = self
		return data
	

	def get_post_mod_options_data(self, thread_id, post_id):
		with self.db.read:
			thread = self.db.one("SELECT * FROM threads WHERE id = ?", thread_id)
			if thread == None:
				return None
			q = """	SELECT thread_id, id, format_time(posted) as posted, comment, name, tripcode, mod, ip, fqdn(ip) as fqdn, status, file_md5, file_meta,
						(SELECT COUNT(*) FROM flags WHERE flags.thread_id = posts.thread_id AND post_id = posts.id) AS num_flags,
						EXISTS (SELECT * FROM reviewed_posts WHERE reviewed_posts.thread_id = posts.thread_id AND reviewed_posts.post_id = posts.id) AS reviewed
					FROM posts WHERE thread_id = ? AND id = ?"""
			post = self.db.one(q, thread_id, post_id)
			if post is None:
				return None
			if post.file_md5:
				post.file_md5 = util.hex(str(post.file_md5))
				post.file_meta = _json_decode(post.file_meta)
			post.proxy_ip = proxycheck.proxy(self, post.ip) if post.ip else False
			post.num_ip_posts = self.global_db.num_ip_posts(post.ip)
			ban = self.global_db.get_ban(post.ip)
			return ObjDict(board_db = self, post = post, thread = thread, ban = ban)
	

	def file_exists(self, file_md5):
		return self.db.one("SELECT 1 FROM posts WHERE file_md5 = ?", file_md5) is not None
	

	def post_exists(self, thread_id, post_id):
		return self.db.one("SELECT 1 FROM posts WHERE thread_id = ? AND id = ?", thread_id, post_id) is not None


	def post_not_deleted(self, thread_id, post_id):
		return self.db.one("SELECT 1 FROM posts WHERE thread_id = ? AND id = ? AND status = ?", thread_id, post_id, PostStatus.UNDELETED) is not None

	
	def get_post_file_md5(self, thread_id, post_id):
		data = self.db.one("SELECT file_md5 FROM posts WHERE thread_id = ? AND id = ?", thread_id, post_id)
		return str(data.file_md5) if data else None


	def get_post_tripcode(self, thread_id, post_id):
		q = "SELECT tripcode FROM posts WHERE thread_id = ? AND id = ?"
		row = self.db.one(q, thread_id, post_id)
		return row.tripcode if row else None


	def num_ip_posts(self, ip):
		return self.db.one("SELECT COUNT(*) AS count FROM posts WHERE ip = ?", ip).count
	
	
	def updated(self):
		q = "SELECT MAX(posted) as updated FROM posts"
		return self.db.one(q).updated or 0
	
	
	def anonymize_old_posts(self):
		q = "UPDATE posts SET ip = NULL WHERE posted < ?"
		self.db.exe(q, time.time() - self.config.hours_to_keep_post_ips*60*60)


	def get_flags(self):
		q = """	SELECT thread_id, post_id, COUNT(*) as count
				FROM flags
				WHERE NOT EXISTS (SELECT * FROM reviewed_posts WHERE reviewed_posts.thread_id = flags.thread_id AND reviewed_posts.post_id = flags.post_id)
				GROUP BY thread_id, post_id
				ORDER BY count DESC, thread_id, post_id"""
		return self.db.all(q)
	

	def get_config_area_existing_boards_data(self):
		return ObjDict(stats = self.get_stats())
	

	def get_config_area_posts_by_ip_data(self, ip):
		with self.db.read:
			#get list of threads
			q = """	SELECT *,
					(SELECT COUNT(*) FROM posts WHERE thread_id = threads.id AND ip = ?) AS num_ip_posts,
					(SELECT COUNT(*) FROM posts WHERE thread_id = threads.id AND ip = ? AND file_meta IS NOT NULL) AS num_ip_files,
					(SELECT MAX(posted) FROM posts WHERE thread_id = threads.id AND ip = ?) AS last_ip_post_time
					FROM threads
					WHERE num_ip_posts > 0
					ORDER BY last_ip_post_time DESC"""
			threads = self.db.all(q, ip, ip, ip)
			total_ip_posts = sum(t.num_ip_posts for t in threads)
			for thread in threads:
				q = "SELECT id, status FROM posts WHERE thread_id = ? AND ip = ? ORDER BY id"
				thread.ip_posts = self.db.all(q, thread.id, ip)
			return ObjDict(total_ip_posts = total_ip_posts, threads = threads)


	def get_config_area_reviewed_posts_data(self):
		q = """ SELECT posts.thread_id, post_id, format_time(time) AS time, status
				FROM reviewed_posts, posts
				WHERE posts.thread_id = reviewed_posts.thread_id AND posts.id = post_id
				ORDER BY reviewed_posts.time"""
		return ObjDict(reviewed_posts = self.db.all(q))
	
	
	def get_stats(self):
		q = """	SELECT
					(SELECT COUNT(*) FROM threads) AS num_threads,
					(SELECT COUNT(*) FROM posts) AS num_posts,
					(SELECT COUNT(*) FROM posts WHERE file_md5 IS NOT NULL) AS num_files"""
		r = self.db.one(q)
		r.uploaded_files_size = util.get_dir_size(self.file_dir())
		return r
