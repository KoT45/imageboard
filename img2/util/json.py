""" json encoding/decoding """


import StringIO, re


_STRING_ESCAPES = {
	'"': r'\"',
	'\\': r'\\',
	'/': r'\/',
	'\b': r'\b',	
	'\f': r'\f',
	'\n': r'\n',
	'\r': r'\r',
	'\t': r'\t'
}


# all groups must be ?: type
_TOKENS = (
	r'"(?:\\"|[^"])*"',
	r'[{}\[\],:]',
	r'null',
	r'true',
	r'false',
	r'-?(?:0|[1-9]\d*)(?:\.\d+)?(?:[eE][-+]?\d+)?',
	r'\s+'
)
_DECODE_REGEX = re.compile(r'(?:' + ')|(?:'.join(_TOKENS) + r')', re.DOTALL)

_STRING_ESCAPES_REGEX = re.compile(r'\\(?:[\\"/bfnrt]|u[0-9a-fA-F]{4})')


class _Decoder:

	def __init__(self, input, dict_factory = dict):
		""" return single character string or (object,) """
		self.dict_factory = dict_factory
		self.ptr = 0
		toks = []
		for c in reversed(_DECODE_REGEX.findall(input)):
			if c[0] in ' \t\r\n':
				continue
			elif c in '{}[],:':
				toks.append(c)
			elif c == 'null':
				toks.append((None,))
			elif c == 'true':
				toks.append((True,))
			elif c == 'false':
				toks.append((False,))
			elif c[0] == '"':
				def sub(m):
					g = m.group()
					i = r'\"/bfnrt'.find(g[1])
					return unichr(int(g[2:], 16)) if i == -1 else '\\"/\b\f\n\r\t'[i]
				toks.append((_STRING_ESCAPES_REGEX.sub(sub, c[1:-1]),))
			else:	# is number
				try:
					toks.append((int(c),))
				except ValueError:
					toks.append((float(c),))
		self.toks = toks
	

	def next_object(self):
		""" return a decoded object or raise Exception """
		t = self.toks.pop()
		
		if type(t) is tuple:
			return t[0]
		
		elif t == '[':
			arr = []
			if self.toks[-1] == ']':
				self.toks.pop()
				return arr
			arr.append(self.next_object())
			while True:
				if self.toks[-1] == ']':
					self.toks.pop()
					return arr
				elif self.toks[-1] == ',':
					self.toks.pop()
					arr.append(self.next_object())
				else:
					raise ValueError
		
		elif t == '{':
			dict = self.dict_factory()
			if self.toks[-1] == '}':
				self.toks.pop()
				return dict
			key = self.next_object()
			if not isinstance(key, basestring) or self.toks.pop() != ':':
				raise ValueError
			dict[key] = self.next_object()
			while True:
				if self.toks[-1] == '}':
					self.toks.pop()
					return dict
				if self.toks[-1] == ',':
					self.toks.pop()
					key = self.next_object()
					if not isinstance(key, basestring) or self.toks.pop() != ':':
						raise ValueError
					dict[key] = self.next_object()
				else:
					raise ValueError
		
		else:
			raise ValueError

def decode(s, dict = dict):
	return _Decoder(s, dict).next_object()


def encode(obj, callback = None):
	"""
		encode the supplied object to a json string.
		object must be a nest of dicts, lists, tuples, strings, ints, floats, longs and bools.
	"""
	s = StringIO.StringIO(u'')
	if callback is not None:
		s.write(callback)
		s.write('(')
		_encode_sub(obj, s)
		s.write(')')
	else:
		_encode_sub(obj, s)
	return s.getvalue()
	

def _encode_sub(obj, output):
	
	if isinstance(obj, basestring):
		if isinstance(obj, str):
			obj = obj.decode('utf-8', 'replace')
		output.write('"')
		for c in obj:
			output.write(_STRING_ESCAPES.get(c, c))
		output.write('"')

	elif isinstance(obj, bool):
		output.write('true' if obj else 'false')
	
	elif isinstance(obj, (int, long, float)):
		output.write(str(obj))

	elif isinstance(obj, dict):
		output.write('{')
		for i, (k, v) in enumerate(obj.items()):
			if i:
				output.write(',')
			if not isinstance(k, basestring):
				raise TypeError('dictionary keys must be strings')
			_encode_sub(k, output)
			output.write(':')
			_encode_sub(v, output)
		output.write('}')

	elif isinstance(obj, (list, tuple)):
		output.write('[')
		for i, e in enumerate(obj):
			if i:
				output.write(',')
			_encode_sub(e, output)
		output.write(']')

	elif obj is None:
		output.write('null')

	else:
		raise TypeError('object can not be converted to json: %s' % obj)


if __name__ == '__main__':
	
	def test(obj):
		import time
		
		print obj
		s = encode(obj)
		print repr(s)
		if encode(decode(s)) != s:
			print '- failed'
		else:
			print '- passed'
		
		t = time.time()
		for i in xrange(1000):
			encode(obj)
		print 'encoding:', (time.time()-t)
		
		for i in xrange(1000):
			decode(s)
		print 'decoding:', (time.time()-t)
	
	
	def prof():
		test({"a": None, "b": 34, "c": 1.23, "d": {"a": "hi"}, "34": [], "0": {}, "": "", "e": True, "f": False, "list":
				["some fairly long string to test the speed of decoding such fairly long strings because thats important"]*20,
				"tests": u"\"\\\/\b\f\n\r\t\u2605"})
	prof()
	#import hotshot, hotshot.stats
	#p = hotshot.Profile('profiler.log')
	#p.runcall(prof)
	#p.close()
	#stats = hotshot.stats.load('profiler.log')
	#stats.strip_dirs()
	#stats.sort_stats('time', 'calls')
	#stats.print_stats(40)
	