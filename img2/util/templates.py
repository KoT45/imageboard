"""
simple python-programmer-centric template system where templates are translated into python code.

these are the tag types.

output tags:
	{{ expr }} -> output += escape(expr) (expr must be a string, int, float or bool)
	{$ expr $} -> output += expr (expr must be a string)

expression tags:
	{E exp E} -> where E = "%" or ":". ":" at start causes an indent reversal, at end it causes an indent on next line. "%" doesnt cause any indent change
	{:} -> tags containing only x ":"s cause x indent reversals
	example: {% for x in y :}
	example: {% x += 2 %}
	example: {: elif t == z :}
	example: {: x = x*4 %}

include another template:
	{" path "}

localized text tag:
	{_text to translate_}

comment tags:
	{# comment #}

spaces between tag start and end bits and middle bits are optional. eg. {{expr}} and {{       expr }}
only comment tags can contain newlines

the global namespace is the data argument to the render method.
names starting with "__" are reserved, as is the name "_".
included templates get a shallow copy of the parents local variables as their own

example usage:

print templates.render('templates', 'test.html', {'msg': 'hello'})

"""

from __future__ import with_statement

import re, os.path, copy, threading, traceback, sys

import util, i18n


_TAG_REGEX = (
	r'#(?P<comment>.*?)#',
	r'\{\s*(?P<esc_output>.+?)\s*\}',
	r'\$\s*(?P<output>.+?)\s*\$',
	r'(?P<rev_ind>:+)',
	r'(?P<exp_start>%|:)\s*(?P<exp>.+?)\s*(?P<exp_end>%|:)',
	r'_(?P<local_text>.*?)_',
	r'"\s*(?P<inc_path>.+?)\s*"'
)
_TAG_REGEX = re.compile(r'\{\s*(' + '|'.join(_TAG_REGEX) + r')\s*\}', re.DOTALL)


class TemplateException(Exception):
	
	def __init__(self, location, exc_info):
		self.locations = [location]
		self.exc_info = exc_info

	def __str__(self):
		
		def mangle(seq):
			return [e.decode('ascii', 'replace').encode('ascii', 'replace') for e in seq]
		
		s = 'exception in template file at:\n  '
		s += '\n  '.join('Template "%s", line %d\n    %s' % (f, n + 1, l) for f, n, l in self.locations)
		s += '\n'
		
		type, value, tb = self.exc_info
		trace = mangle(traceback.format_list(traceback.extract_tb(tb)[2:]))
		excep = mangle(traceback.format_exception_only(type, value))
		s += ''.join(trace + excep).rstrip()
		
		return s


_lock = threading.Lock()
_cache = {}	#path => (file_mod_time, Template())


def render(dir, path, data = {}, localize = lambda k: k):
	"""
		return a string rendered from the template loaded from the file at os.path.join(dir, path).
		use dir as the base file path for any included templates to be loaded from.
		raise TemplateException upon error.
	"""
	_purge_modified()
	out = []
	data['__dir'] = dir
	data['__out'] = out
	data['__app'] = out.append
	data['_'] = localize
	data['__html_escape'] = util.html_escape
	data['__render_to_list'] = _render_to_list
	data['__globals'] = data
	_render_to_list(dir, path, {}, data)
	return ''.join(out)


def flush_cache():
	with _lock:
		_cache.clear()


def _line_num(match):
	return match.string[:match.start()].count('\n')


def extract_local_strings(dir):

	inner_exp = re.compile(r'_\(([\'"])(.+?)(\1)\)')

	strs = {}	# msgid => set('filename:linenum', ...)

	def extract(f):
		for m in util.re_msplit(_TAG_REGEX, open(f, 'rb').read().decode('utf-8')):
			if not isinstance(m, basestring):
				d = m.groupdict()

				def found(s):
					strs.setdefault(s, set())
					strs[s].add('%s:%d' % (f, 1 + _line_num(m)))

				if d['local_text']:
					found(d['local_text'])
				elif d['esc_output']:
					for t in inner_exp.findall(d['esc_output']):
						found(t[1])
				elif d['output']:
					for t in inner_exp.findall(d['output']):
						found(t[1])
	
	for dirpath, dirnames, filenames in os.walk(dir):
		for f in filenames:
			if f.endswith('.html'):
				extract(os.path.join(dirpath, f))

	return dict((msgid, ('', lines)) for msgid, lines in strs.items())


def _render_to_list(dir, path, locals, globals):
	tpl = _load(dir, path)
	locals = copy.copy(locals)
	try:
		exec tpl.code in globals, locals
	except TemplateException, te:
		te.locations.insert(0, (path, locals['__line'], tpl.lines[locals['__line']]))
		raise te
	except Exception, e:
		raise TemplateException((path, locals['__line'], tpl.lines[locals['__line']]), sys.exc_info())


def _purge_modified():
	""" remove all modified templates from the cache. they will be loaded upon next use """
	with _lock:
		for path, tpl in _cache.items():
			try:
				if os.path.getmtime(path) != tpl[0]:
					del _cache[path]
			except OSError:	#file deleted?
				del _cache[path]


def _load(dir, path):
	with _lock:
		full_path = os.path.join(dir, path)
		if full_path not in _cache:
			mod = os.path.getmtime(full_path)
			tpl = _Template(dir, full_path, open(full_path, 'rb').read().decode('utf-8'))
			_cache[full_path] = (mod, tpl)
		return _cache[full_path][1]


class _Template:
	
	def __init__(self, dir, path, string):
		self.lines = string.split('\n')
		
		code = ''
		line = 0
		line_count = [0]	# indent => count
		
		for m in util.re_msplit(_TAG_REGEX, string):
			
			if isinstance(m, basestring):
				code += '\t'*(len(line_count)-1) + '__app(' + repr(m) + ')\n'
				line_count[-1] += 1
			
			else:
				if line != _line_num(m):
					line = _line_num(m)
					code += '\t'*(len(line_count)-1) + '__line = %d\n' % line
				
				def syntax_error():
					return SyntaxError('reverse-indented too far, line %d\n    %s' % (_line_num(m), self.lines[_line_num(m)]))
				
				d = m.groupdict()
				
				if d['inc_path']:
					code += '\t'*(len(line_count)-1) + '__render_to_list(__dir, "' + d['inc_path'] + '", locals(), __globals)\n'
					line_count[-1] += 1
				
				if d['exp']:
					s = d['exp_start']
					e = d['exp_end']
					if s == ':':
						if len(line_count) == 1:
							raise syntax_error()
						if not line_count.pop():
							code += '\t'*len(line_count) + 'pass\n'
					code += '\t'*(len(line_count)-1) + d['exp']
					line_count[-1] += 1
					if e == ':':
						code += ':'
						line_count += [0]
					code += '\n'
			
				elif d['rev_ind']:
					for c in d['rev_ind']:
						if len(line_count) == 1:
							raise syntax_error()
						if not line_count.pop():
							code += '\t'*len(line_count) + 'pass\n'
				
				elif d['local_text']:
					exp = d['local_text']
					code += '\t'*(len(line_count)-1) + '__app(_(u"""' + exp + '"""))\n'
					line_count[-1] += 1
	
				elif d['esc_output']:
					exp = d['esc_output']
					code += '\t'*(len(line_count)-1) + '__app(__html_escape(' + exp + '))\n'
					line_count[-1] += 1
				
				elif d['output']:
					exp = d['output']
					code += '\t'*(len(line_count)-1) + '__app(' + exp + ')\n'
					line_count[-1] += 1
		
		self.code = compile(code, path, 'exec')
		
