""" helps provide http session storage. """


from __future__ import with_statement

import random, threading, pickle, re

from time import time


ID_LENGTH = 16
ID_CHARS = '-_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
ID_REGEX = re.compile(r'^[%s]{%d}$' % (ID_CHARS, ID_LENGTH))


def _generate_id():
	""" return a randomly generated session id """
	return ''.join([random.choice(ID_CHARS) for i in xrange(ID_LENGTH)])


def _get_valid_id(input):
	""" if input is a valid session id (string matching ID_REGEX), return it. otherwise return _generate_id() """
	return input if (isinstance(input, basestring) and ID_REGEX.match(input)) else _generate_id()


class Manager:
	""" handles temporary (memory) and persistent (disk) session storage. """


	def __init__(self, factory, file = None, lifetime = 60*30):
		self.factory = factory
		self.file = file
		self.lifetime = lifetime
		self._lock = threading.Lock()
		self._data = {}				#id => (last-usage-time, session-object)
		self._last_clean = 0		#time of last clean
		#load existing sessions
		if file:
			try:
				version, objs = pickle.load(open(file, 'rb'))
				if version == factory.VERSION:
					self._data = objs
			except:
				pass


	def __del__(self):
		if self.file:
			pickle.dump((self.factory.VERSION, self._data), open(self.file, 'wb'))


	def clean(self):
		""" delete sessions from memory that are older than self.lifetime, but only do this if self.lifetime has passed since the last time it was done. """
		with self._lock:
			if (time() - self._last_clean) > self.lifetime:
				self._last_clean = time()
				exp = []
				for id, (t, obj) in self._data.items():
					if (time() - t) > self.lifetime:
						exp.append(id)
				for id in exp:
					del self._data[id]


	def load(self, id):
		""" load a session object for the specified id, creating a new one if one doesn't currently exist. """
		id = _get_valid_id(id)
		with self._lock:
			obj = self._data[id][1] if id in self._data else self.factory(id)
			self._data[id] = (time(), obj)
			return obj


	def store(self, object):
		""" store the specified session object, setting the age of the data to 0 """
		with self._lock:
			self._data[object.id] = (time(), object)


	def __len__(self):
		with self._lock:
			return len(self._data)

