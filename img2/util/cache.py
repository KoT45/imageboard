""" see the class Cache """


from __future__ import with_statement

import time, threading


class Cache(object):
	"""
		a thread-safe in-memory cache holding a limited number of items (LRU removal policy)
		keys can be any hashable object, stored items can be any object
	"""


	def __init__(self, max_items):
		""" creates a Cache that holds a maximum of max_items. if more items are added the least recently accessed ones will be removed """
		self.max_items = max_items
		self._lock = threading.Lock()
		self._dict = {}		# key => (time-accessed, value)

	
	def _make_space(self):
		""" ensures there is at least one free space in the cache by removing max_items/5 if the cache is full. requires external locking. """
		if len(self._dict) == self.max_items:
			to_rm = [(e[1][0], e[0]) for e in self._dict.items()]
			to_rm.sort()
			to_rm = to_rm[:self.max_items/5]
			for k in to_rm:
				del self._dict[k[1]]


	def set(self, key, value):
		""" sets an item in the cache. calling this method may remove other items from the cache. """
		with self._lock:
			if key not in self._dict:
				self._make_space()
			self._dict[key] = (time.time(), value)


	def get(self, key, make = None):
		""" gets an item from the cache given a key, if no item exists and make
			is not None then make will be called and the result added to the cache
			and returned. if make is None and no item exists then None will be
			returned. since this method may add to the cache calling it may remove
			other items from the cache. """
		with self._lock:
			if key in self._dict:
				value = self._dict[key][1]
				self._dict[key] = (time.time(), value)
				return value
			elif make != None:
				value = make()
				self._make_space()
				self._dict[key] = (time.time(), value)
				return value


	def delete(self, key):
		""" deletes key from the cache, if it exists """
		with self._lock:
			if key in self._dict:
				del self._dict[key]

	def __len__(self):
		with self._lock:
			return len(self._dict)


if __name__ == '__main__':
	c = Cache(10)
	c.set('a', 3)
	print c.get('a')
	c.delete('c')
