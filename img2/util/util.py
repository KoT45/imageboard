""" utility module... """


from __future__ import with_statement


import re, hashlib, os.path, random, time, tempfile, socket, sqlite3


import cache


_HEX_REGEX = re.compile('^[a-zA-Z0-9]*$')

_AMPM_HOURS = ['12:%.2dam'] \
	+ ['%d:%%.2dam' % i for i in xrange(1, 12)] \
	+ ['12:%.2dpm'] \
	+ ['%d:%%.2dpm' % i for i in xrange(1, 12)]

_TIME_REGEX = re.compile(r'^(never|(?P<number>\d{1,8}(\.\d{1,8})?)( )?(?P<unit>(second|minute|hour|day|week|month|year)s?))$')
_TIME_SECONDS = {'second': 1, 'minute': 60, 'hour': 60*60, 'day': 60*60*24, 'week': 60*60*24*7, 'month': 60*60*24*30, 'year': 60*60*24*365}


_dns_cache = cache.Cache(4096)	# ip => (time-of-last-lookup, string | None)


class TempFile:
	""" holds a file name that will be unlinked when the instance is garbage collected """
	
	def __init__(self):
		self.path = tempfile.mkstemp()[1]

	def contents(self):
		return open(self.path, 'rb').read()

	def length(self):
		return os.path.getsize(self.path)

	def __del__(self):
		try:
			os.unlink(self.path)
		except:
			pass


class ObjDict(dict):
	
	def __init__(self, copy = {}, **d):
		self.update(copy)
		self.update(d)

	def __getattribute__(self, name):
		try:
			return self[name]
		except:
			return object.__getattribute__(self, name)
	
	def __setattr__(self, name, value):
		self[name] = value


class SQLiteDB:
	""" wrapper around a sqlite3 database """
	
	def __init__(self, path):
		
		def dict_factory(cursor, row):
			d = ObjDict()
			for i, col in enumerate(cursor.description):
				d[col[0]] = row[i]
			return d
	
		if isinstance(path, unicode):
			path = path.encode('utf-8')
		self.path = path
		
		self.db = sqlite3.connect(path)
		self.db.isolation_level = None
		self.db.row_factory = dict_factory
		self.cur = self.db.cursor()
		
		class Transaction:
			def __init__(self_, immediate):
				self_.immediate = immediate
			def __enter__(self_):
				self.cur.execute("BEGIN IMMEDIATE" if self_.immediate else "BEGIN")
			def __exit__(self_, type, value, traceback):
				self.cur.execute("COMMIT" if type == value == traceback == None else "ROLLBACK")
		
		self.read = Transaction(False)
		self.write = Transaction(True)
		
	
	def exe(self, query, *params):
		"""do the query, return nothing"""
		self.cur.execute(query, params)


	def all(self, query, *params):
		"""do the query and return the fetchall() result in one call"""
		self.cur.execute(query, params)
		return self.cur.fetchall()


	def one(self, query, *params):
		"""do the query and return either the first returned row, or None if there are no returned rows"""
		data = self.all(query, *params)
		return data[0] if data else None


	def last_row_id(self):
		return self.cur.lastrowid


	def exe_script(self, script):
		self.cur.executescript(script)


def is_hex(s):
	return _HEX_REGEX.match(s) is not None


def hex(input):
	a = '0123456789ABCDEF'
	return ''.join([a[ord(c)>>4] + a[ord(c)&0xf] for c in input])


def unhex(input):
	out = ''
	i = 0
	while i+1 < len(input):
		out += chr(int(input[i:i+2], 16))
		i += 2
	return out


def quiet_unlink(path):
	try:
		os.unlink(path)
	except OSError:
		pass


def valid_ip(input):
	""" return whether input is a valid ipv4 or ipv6 address string. """
	for family in (socket.AF_INET, socket.AF_INET6):
		try:
			socket.inet_pton(family, input)
			return True
		except:
			pass
	return False


def reverse_dns(ip):
	""" do a reverse dns lookup on ip. cache both successfull and failed lookups for 24 hours. """
	
	def lookup():
		# the nested socket module function calls here allow ipv6 address reverse dns to work
		return (time.time(), socket.getfqdn(socket.gethostbyname(ip)))
	
	last_lookup, result = _dns_cache.get(ip, lookup)
	if time.time()-last_lookup > 60*60*24:
		last_lookup, result = lookup()
		_dns_cache.set(ip, (last_lookup, result))
	
	return result


def valid_time(timestamp):
	try:
		time.gmtime(timestamp)
		return True
	except:
		return False


def sint(input, default = 0):
	""" "safe" int cast """
	try:
		return int(input)
	except:
		return default	


def psint(input, default = 0):
	""" positive "safe" int cast """
	return abs(sint(input, default))


def sfloat(input, default = 0):
	""" "safe" float cast """
	try:
		return float(input)
	except:
		return default


def psfloat(input, default = 0):
	""" positive "safe" float cast """
	return abs(sfloat(input, default))


def md5(input, hex = True):
	""" one-call md5 """
	if isinstance(input, unicode):
		input = input.encode('utf-8', 'replace')
	h = hashlib.md5()
	h.update(input)
	return h.hexdigest() if hex else h.digest()


def sha1(input, hex = True):
	""" one-call sha1 """
	if isinstance(input, unicode):
		input = input.encode('utf-8', 'replace')
	h = hashlib.sha1()
	h.update(input)
	return h.hexdigest() if hex else h.digest()


def rand_str(len, pos = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'):
	return ''.join([random.choice(pos) for i in xrange(len)])


def clamp_str(input, max_len, app = ''):
	if not isinstance(input, basestring):
		return ''
	if (len(input) <= max_len):
		return input
	elif len(app) < max_len:
		return input[:max_len - len(app)] + app
	else:
		return app[:max_len]


def clamp_to_line(input):
	""" if input contains newlines return everything up to the first newline, otherwise return input verbatim. """
	a = input.splitlines()
	return '' if not a else a[0]


def limit(input, *limits):
	if input in limits:
		return input
	return limits[0]


def clamp(val, min, max):
	""" sint's val, then clamps it between min and max """
	val = sint(val)
	if val > max:
		return max
	if val < min:
		return min
	return val


def ext(fname):
	i = fname.rfind('.')
	return '' if i == -1 else fname[i+1:]


def copy_file_md5(path, file):
	""" copies data from file object file into the physical file at path. returns the md5 of the data copied """
	h = hashlib.md5()
	rt = open(path, 'wb')
	c = file.read(4096)
	while c:
		h.update(c)
		rt.write(c)
		c = file.read(4096)
	file.close()
	rt.close()
	return h.digest()


def get_dir_size(path):
	""" recursive directory size """
	size = 0
	for p, d, files in os.walk(path):
		for f in files:
			size += os.path.getsize(os.path.join(p, f))
	return size


def sanitize_file_name(input):
	#disallow filenames starting with ".", and remove slashes and nulls
	input = re.sub(r'^\.+', '_', input)
	input = re.sub(r'[\\\/\x00]+', '_', input)
	#only allow a single "." - prevent more than one extension being present (apache mod_mime/php issue), and remove ".." sequences
	input = re.sub(r'\.(?=.*?\.)', '_', input)
	#lower the extension
	input = re.sub(r'(?<=\.).+', lambda s: s.group().lower(), input)
	return input[-128:]


def utf8(s):
	return s if not isinstance(s, unicode) else s.encode('utf-8')


def format_file_size(size):
	size = float(size)
	if size < 1024:
		return '%db' % size
	size = size/1024	#now in kb
	if size < 1024:
		return '%dkb' % size
	size = size/1024	#now in mb
	if size < 1024:
		return '%dmb' % size
	size = size/1024	#now in gb
	return '%dgb' % size


def ampmtime(struct_time):
	return _AMPM_HOURS[struct_time.tm_hour] % struct_time.tm_min


def format_time(timestamp = None, gmt_offset = 0):
	""" formats timestamp, giving inaccurate information (to allow for caching) about how far ago it was if it is in the past """
	
	now = int(time.time())
	if timestamp is None:
		timestamp = now
	
	now_st = time.gmtime(now + gmt_offset*60*60)
	then_st = time.gmtime(timestamp + gmt_offset*60*60)
	apt = ampmtime(then_st)
	
	if (timestamp <= now) and (now_st.tm_year == then_st.tm_year):
		if now_st.tm_yday == then_st.tm_yday:
			min_dif = (now-timestamp)/60
			if min_dif < 2:
				return apt + ' - about a minute ago'
			elif min_dif < 60:
				return apt + ' - about %d minutes ago' % min_dif
			hr_dif = min_dif/60
			if hr_dif == 1:
				return apt + ' - about an hour ago'
			elif hr_dif < 24:
				return apt + ' - about %d hours ago' % hr_dif
		if now_st.tm_yday-1 == then_st.tm_yday:
			return time.strftime('%b %d - Yesterday - ', then_st) + apt
		else:
			return time.strftime('%b %d ', then_st) + apt
	
	return time.strftime('%b %d %Y ', then_st) + apt


def format_interval(time, digits = 0):
	""" format time (interval in seconds, with the special meaning of 0 as infinity) into a readable yet inaccurate format, to digits decimal places. """
	if time == 0:
		return 'never'
	d = '%%.%df ' % digits
	if time < 60:
		return (d+'second') % time + ('s' if time != 1 else '')
	time = time/60.0	#now in minutes
	if time < 60:
		return (d+'minute') % time + ('s' if time != 1 else '')
	time = time/60.0	#now in hours
	if time < 24:
		return (d+'hour') % time + ('s' if time != 1 else '')
	time = time/24.0	#now in days
	if time < 30:
		return (d+'day') % time + ('s' if time != 1 else '')
	day_time = time
	time = time/30.0	#now in months
	if time < 12:
		return (d+'month') % time + ('s' if time != 1 else '')
	time = day_time/365.0	#now in years
	return (d+'year') % time + ('s' if time != 1 else '')


def parse_interval(t):
	""" parse time (interval in seconds) in the format returned by format_interval, return time in int seconds or None with invalid input """
	mo = _TIME_REGEX.match(t)
	if mo is None:
		return None
	elif t == 'never':
		return 0
	else:
		dict = mo.groupdict()
		number = float(dict['number'])
		unit = dict['unit']
		if unit[-1] == 's':
			unit = unit[:-1]
		val = int(number * _TIME_SECONDS[unit])
		#too-high values cause an exception when formatting them, so just assume they are infinity
		return 0 if (int(time.time()+val) >= 1<<31) else val


def re_msplit(regex, input):
	""" a regex split that returns a list of interspersed strings and match objects """
	output = []
	start = 0
	for mo in regex.finditer(input):
		if mo.start() > start:
			output.append(input[start:mo.start()])
		output.append(mo)
		start = mo.end()
	if start != len(input):
		output.append(input[start:])
	return output


def html_escape(input):
	if isinstance(input, basestring):
		return input.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace('"', '&quot;').replace('\'', '&#039;')
	elif isinstance(input, (int, long, float, bool)):
		return str(input)
	else:
		raise TypeError('input not int, long, float, boolean or basestring: %s' % input)


def html_escape_nl(input):
	return html_escape(input).replace('\r\n', '<br>')


def strip_html(input):
	""" return input with html tags stripped """
	p = re.compile(r'<!--.*?-->|<.*?>', re.DOTALL)
	return re.sub(p, '', input)


def nl_to_br(input):
	""" return input with all occurences of "\r\n" replaced by "<br>" """
	return input.replace('\r\n', '<br>')


def read_checked(file, rlen = 1):
	r = file.read(rlen)
	if len(r) < rlen:
		raise EOFError
	return r
