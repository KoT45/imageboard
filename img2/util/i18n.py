

from __future__ import with_statement

import re, os.path, threading, time


TEMPLATE_NAME = 'messages.pot'
CATALOG_NAME = 'messages.po'


_PO = r'".*"'
_PO = r'(%s)(\s*(%s))*' % (_PO, _PO)
_PO = re.compile(r'(?P<comments>(#:.*\n)*)msgid\s*(?P<msgid>%s)\nmsgstr\s*(?P<msgstr>%s)' % (_PO, _PO))


_lock = threading.Lock()
_cache = {}		# dir => {code => (load-time, IdentityDict())}


class IdentityDict(dict):
	""" a dict that returns the key itself when no value can be found for it or the value is ''. """

	def __getitem__(self, name):
		try:
			return dict.__getitem__(self, name) or name
		except KeyError:
			return name


def parse(s):
	""" return dict(msgid => (msgstr, {comment-line, ...})) """

	def parse(s):
		return eval('+'.join(e.strip() for e in s.splitlines()))

	def parse_comments(s):
		c = set()
		for line in s.splitlines():
			for seg in line[2:].split():
				c.add(seg)
		return c

	strs = {}
	for m in _PO.finditer(s):
		strs[parse(m.group('msgid'))] = (parse(m.group('msgstr')), parse_comments(m.group('comments')) if m.group('comments') else set())
	return strs


def unparse(catalog):
	s = ''
	for msgid, (msgstr, comments) in sorted(catalog.items()):
		for line in sorted(comments):
			s += '#: %s\n' % line
		s += 'msgid "%s"\n' % esc_msg(msgid)
		s += 'msgstr "%s"\n' % esc_msg(msgstr)
		s += '\n'
	return s


def merge(a, b):
	""" merges the contents of the two templates. """
	c = {}
	for msgid in set(a.keys() + b.keys()):
		c[msgid] = ('', a.get(msgid, ('', set()))[1] | b.get(msgid, ('', set()))[1])
	if '' in c:
		del c['']
	return c


def diff(catalog, template):
	""" return a template-dict with all entries from template that are not in catalog. """
	return dict((msgid, data) for msgid, data in template.items() if msgid not in catalog)
	
	
def esc_msg(s):
	for c in '\\\t\r\n':
		s = s.replace(c, repr(c)[1:-1])
	return s.replace('"', r'\"')


def catalog(dir, code):
	"""
		return an IdentityDict containing string translations for the specified locale dir/code.
		return an empty IdentityDict if no file can be found for the specifed dir/code.
	"""
	with _lock:
		try:
			return _cache[dir][code][1]
		except KeyError:
			pass

		try:

			with open(os.path.join(dir, code, CATALOG_NAME)) as f:
				sd = IdentityDict()
				for msgid, (msgstr, comments) in parse(f.read().decode('utf-8')).items():
					sd[msgid] = msgstr
				sd[''] = ''

			_cache.setdefault(dir, {})
			_cache[dir][code] = (time.time(), sd)

			return sd
		except:
			return IdentityDict()


def flush_cache():
	with _lock:
		_cache.clear()


def purge_modified():
	with _lock:
		for dir, codes in _cache.items():
			for code, (loaded, catalog) in codes.items():
				path = os.path.join(dir, code, CATALOG_NAME)
				try:
					if os.path.getmtime(path) > loaded:
						del codes[code]
				except OSError:			# eg. if file not found
					del codes[code]

