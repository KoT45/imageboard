""" half compliant http server (stick it behind a proxy) + Request/Response objects + helper stuff """


import re, cgi, urllib, threading, time, socket, Queue, signal, logging, rfc822

import sessions


_REQUEST_LINE_REGEX = re.compile(r'^(?P<method>GET|POST) (?P<path>[^ ]*?)(\?(?P<query>[^ ]*))? HTTP/(?P<version>1\.0|1\.1)\r\n$')

_ACCEPT_GZIP_REGEX = re.compile(r'\bgzip\b')

_STANDARD_PAGE = """<html><head><title>Error</title></head><body><h1>%s</h1></body></html>"""
_STANDARD_FORBIDDEN_PAGE = _STANDARD_PAGE % '404 Not Found'
_STANDARD_NOT_FOUND_PAGE = _STANDARD_PAGE % '403 Forbidden'
_STANDARD_ERROR_PAGE = _STANDARD_PAGE % '500 Server Error'

_LOGGER = logging.getLogger(__name__)


class Isolation:
	EXCLUSIVE, QUEUE_SIMILAR, UNRESTRICTED = 0, 1, 2


class NotFound(Exception):
	""" thrown when a 404 page should be displayed """


class Forbidden(Exception):
	""" thrown when a 503 page should be displayed """


class ListDict(dict):
	""" an extension of dict with a __call__ method for accessing lists easily. used for Request.get, Request.post, Request.cookies and Request.files """
	
	def __init__(self, d = {}):
		self.update(d)
	
	def __call__(self, key, default = None, index = 0):
		if key in self and index < len(self[key]):
			return self[key][index]
		else:
			return default


class DefaultSession:
	VERSION = 0
	def __init__(self, id):
		self.id = id


class Handler:
	""" extend this to handle requests. """
	
	def identify(self, request):
		""" return the isolation level that the specified request object should be handled at. should be one of the Isolation class properties. """
		return Isolation.UNRESTRICTED
	
	def handle(self, request):
		""" return a Response, or raise NotFound, Forbidden, or another exception. """
		raise NotFound
	
	def notfound(self, request):
		""" called when NotFound is raised from handle(...) """
		return Response(_STANDARD_NOT_FOUND_PAGE)
	
	def forbidden(self, request):
		""" called when Forbidden is raised from handle(...) """
		return Response(_STANDARD_FORBIDDEN_PAGE)
	
	def error(self, request):
		""" called when an exception that is not NotFound or Forbidden is raised from handle(...) """
		return Response(_STANDARD_ERROR_PAGE)


class RegexHandler(Handler):

	def __init__(self, urls, trans = {}, trailing_slash_magic = True):
		"""
			urls is a tuple sequence: [(regex, view, extra, isolation), ...]
				regex is a regex-string.
				view is a function with a request argument and some keyword args matching named groups in the regex-string.
				extra is a dict of extra keyword arguments to transform and pass to view
				isolation is an Isolation constant
		"""
		self.urls = [(re.compile(e[0]), e[1], e[2], e[3]) for e in urls]
		self.trans = trans
		self.trailing_slash_magic = trailing_slash_magic
	
	def identify(self, request):
		path = request.path[1:]
		
		for url in self.urls:
			if url[0].match(path):
				return url[3]
		
		return Isolation.UNRESTRICTED
	
	def handle(self, request, *extras):
		""" hack for profiling - rename the lower handle to handle2 to enable. """
		import hotshot.stats
		p = hotshot.Profile('profiler.log')
		response = p.runcall(self.handle2, request, *extras)
		p.close()
		stats = hotshot.stats.load('profiler.log')
		stats.strip_dirs()
		stats.sort_stats('time', 'calls')
		stats.print_stats(40)
		return response
	
	def handle(self, request, *extras):
		path = request.path[1:]
		
		for url in self.urls:
			mo = url[0].match(path)
			if mo:
				d = mo.groupdict()
				# transform matched groups
				for k in d:
					if k in self.trans:
						d[k] = self.trans[k](request, *(extras + (d[k],)))
				# add extra groups
				d.update(url[2])
				return url[1](request, *extras, **d)
		
		#if we get here no matching url-regex was found
		if self.trailing_slash_magic and request.method == 'GET':
			path = path.rstrip('/') if path.endswith('/') else (path + '/')
			for url in self.urls:
				if url[0].match(path):
					return RedirectResponse('/' + path)
		
		raise NotFound


def _parse_qs(s):
	d = ListDict()
	for k, v in cgi.parse_qs(s, keep_blank_values = True).items():
		d[k.decode('utf-8', 'replace')] = [e.decode('utf-8', 'replace') for e in v]
	return d


class Request(object):

	def __init__(self, server, sock, addr):
		self.server = server

		self._addr = addr
		self._sock = sock
		self._file = sock.makefile()
		
		try:
			rlmo = _REQUEST_LINE_REGEX.match(self._file.readline())
			#print rlmo.group('method')
			#print urllib.unquote_plus(rlmo.group('path')).decode('utf-8', 'replace')
			#print rlmo.group('query')
			#print rlmo.group('version')

			if not rlmo:
				raise ValueError('invalid request line')
	
			self.method = rlmo.group('method')
			self.path = urllib.unquote_plus(rlmo.group('path')).decode('utf-8', 'replace')
			self.query = rlmo.group('query') or ''
			self.version = rlmo.group('version')
		
			self.headers = {}
			for line in rfc822.Message(self._file).headers:
				line = line.partition(':')
				name = line[0].strip().lower().decode('utf-8', 'replace')
				value = line[2].strip().decode('utf-8', 'replace')
				self.headers[name] = value
		
			self.headers.setdefault('content-length', '0')
			self.headers.setdefault('content-type', 'application/octet-stream')

			if not server.proxies or not self['x-forwarded-host']:
				self.server_name = (self['host'] or '127.0.0.1')
			else:
				self.server_name = self['x-forwarded-host'].split(', ')[-server.proxies]
		
			if not server.proxies or not self['x-forwarded-for']:
				self.remote_host = addr[0]
			else:
				self.remote_host = self['x-forwarded-for'].split(', ')[-server.proxies]
		
			# parse GET data
			self.get = _parse_qs(self.query)
		
			# parse POST data
			self.post = ListDict()
			self.files = ListDict()
			ct = cgi.parse_header(self['content-type'])
		
			if ct[0] == 'application/x-www-form-urlencoded':
				pl = int(self['content-length'])
				if pl > server.max_urlencoded_post_length:
					raise ValueError('request size too large')
				self.post = _parse_qs(self._file.read(pl))
		
			elif ct[0] == 'multipart/form-data':
				cgi_headers = dict(
					REQUEST_METHOD = self.method,
					CONTENT_TYPE = self['content-type'],
					CONTENT_LENGTH =  self['content-length'],
				)
				fs = cgi.FieldStorage(fp = self._file, environ = cgi_headers, keep_blank_values = True)
				for k in fs:
					val = fs[k]
					if not isinstance(val, list) and val.name is not None:
						if val.filename:
							self.files[val.name.decode('utf-8', 'replace')] = [UploadedFile(val.filename.decode('utf-8', 'replace'), val.file)]
						else:
							self.post[val.name.decode('utf-8', 'replace')] = [val.value.decode('utf-8', 'replace')]
		
			# parse cookies
			self.cookies = ListDict()
			if self['cookie']:
				for c in self['cookie'].split(';'):
					nv = c.partition('=')
					name = urllib.unquote_plus(str(nv[0]).strip()).decode('utf-8', 'replace')
					value = urllib.unquote_plus(str(nv[2]).strip()).decode('utf-8', 'replace')
					if name in self.cookies:
						self.cookies[name].append(value)
					else:
						self.cookies[name] = [value]
		
			self.session = server.session_manager.load(self.cookies('sid'))
			self.read_time = time.time()
			self.handle_time = None			#will be set when this request is about to be handled
		except:
			self._file.close()
			raise
	
	def __getitem__(self, key):
		""" return the header called key.lower(), or None if it doesnt exist """
		return self.headers.get(key.lower())
	
	def is_ajax(self):
		""" return if a "X-Requested-With" http header with value "XMLHttpRequest" was sent with this request """
		return self['x-requested-with'] == 'XMLHttpRequest'
	
	def local_referer(self):
		rf = (self['referer'] or '').partition('://')[2]
		return rf.startswith(self.server_name + '/') or (rf == self.server_name)
	
	def accepts_gzip(self):
		return _ACCEPT_GZIP_REGEX.match(self['accept-encoding'] or '') is not None

	def handle_period(self):
		return time.time() - self.handle_time
	
	def _store_session(self):
		self.server.session_manager.store(self.session)
	
	def _set_sid_cookie(self, response):
		if self.cookies('sid') != self.session.id:
			response.set_cookie('sid', self.session.id, path = '/')


class Response(object):
	
	def __init__(self, body = '', type = 'text/html; charset=utf-8', status = 200, headers = {}):
		""" body must be a str or a unicode. if it is a unicode it will be utf-8 encoded and stored as body """
		self.status = status
		self.headers = headers
		self.cookies = {}	#name => Cookie
		if isinstance(body, unicode):
			body = body.encode('utf-8', 'replace')
		self.body = body
		self['Content-Type'] = type
		self['Content-Length'] = str(len(body))
		self['Date'] = rfc822.formatdate(time.time())
		self['Expires'] = self['Date']						# dont allow caching by default
	
	def __setitem__(self, key, value):
		""" set the response header with name key to value """
		self.headers[key] = value
	
	def __getitem__(self, key):
		""" return the response header with name key """
		return self.headers[key]
	
	def expire(self, minutes = 30):
		""" set headers indicating this response can be cached for the specified number of minutes """
		self['Expires'] = rfc822.formatdate(time.time() + minutes*60)
	
	def set_cookie(self, name, value, max_age = None, expires = None, path = None, domain = None, secure = False, http_only = False, raw = False):
		if expires is None and max_age is not None:
			expires = time.time() + max_age
		self.cookies[name] = Cookie(name, value, expires, path, domain, secure, http_only, raw)
	
	def delete_cookie(self, name, path = None, domain = None, secure = False, http_only = False):
		self.set_cookie(name, '', -24*60*60, None, path, domain, secure, http_only)
	
	def _write(self, req):
		file = req._file
		file.write('HTTP/1.0 %d\r\n' % self.status)
		for k, v in self.headers.items():
			if re.search(r'[\r\n:]', k) or re.search(r'[\r\n]', v):
				raise ValueError('invalid header')
			if isinstance(k, unicode):
				k = k.encode('utf-8', 'replace')
			if isinstance(v, unicode):
				v = v.encode('utf-8', 'replace')
			file.write(k)
			file.write(': ')
			file.write(v)
			file.write('\r\n')
		for name, cookie in self.cookies.items():
			file.write('set-cookie: ')
			file.write(urllib.quote(name.encode('utf-8', 'replace')))
			file.write('=')
			if cookie.raw:
				file.write(cookie.value.encode('utf-8', 'replace'))
			else:
				file.write(urllib.quote(cookie.value.encode('utf-8', 'replace')))
			if cookie.expires is not None:
				file.write('; expires=')
				file.write(time.strftime('%a, %d-%b-%Y %H:%M:%S GMT', time.gmtime(cookie.expires)))
			if cookie.path is not None:
				file.write('; path=')
				file.write(urllib.quote(cookie.path.encode('utf-8', 'replace') if isinstance(cookie.path, unicode) else cookie.path))
			if cookie.domain is not None:
				file.write('; domain=')
				file.write(cookie.domain)
			if cookie.secure:
				file.write('; secure')
			if cookie.http_only:
				file.write('; http_only')
			file.write('\r\n')	
		file.write('\r\n')
		file.write(self.body)
	

class RedirectResponse(Response):
	def __init__(self, path):
		Response.__init__(self, status = 302, headers = {'location': path})


class UploadedFile(object):
	def __init__(self, name, file):
		self.name = name
		self.file = file


class Cookie(object):
	def __init__(self, name, value, expires, path, domain, secure, http_only, raw):
		self.name = name
		self.value = value
		self.expires = expires
		self.path = path
		self.domain = domain
		self.secure = secure
		self.http_only = http_only
		self.raw = raw


class _ServerThread(threading.Thread):
	def __init__(self, server):
		threading.Thread.__init__(self)
		self.server = server
		self.start()


class _ReadThread(_ServerThread):
	""" repeatedly get a socket from server._read_queue, read a request from it, and put the request in server._reorder_queue
		there can be more than one of these """
	
	def run(self):
		while True:
			con = self.server._read_queue.get()
			if con is None:
				self.server._reorder_queue.put(None)
				break
			else:
				sock, addr = con
				sock.settimeout(None)
				try:
					req = Request(self.server, sock, addr)
				except Exception, e:
					if not isinstance(e, socket.error):
						_LOGGER.exception('exception reading request')
					try:
						sock.close()
					except:
						pass
				else:
					isolation = self.server.handler.identify(req)
					self.server._reorder_queue.put((req, isolation))


class _ReorderThread(_ServerThread):
	""" get requests from server._reorder_queue, possibly reorder them, then put them in server._handle_queue. also read from server._finish_queue.
		there can be only one of these. """
	
	def run(self):
		block_time = 0.1
		
		current = []		# requests currently being handled - [(req, isolation), ...]
		pending = []		# requests currently pending handling - ditto
		nones = 0			# count of Nones recieved from server._finish_queue thus far
		
		def exclusive_pending():
			for i, r in enumerate(pending):
				if r[1] == Isolation.EXCLUSIVE:
					return i
			return -1
		
		def exclusive_current():
			return current and current[0][1] == Isolation.EXCLUSIVE
		
		def same_session_current(req):
			for r in current:
				if r[0].session.id == req[0].session.id:
					return True
			return False
		
		def similar_current(req):
			for r in current:
				if r[0].path == req[0].path:
					return True
			return False
		
		while True:
		
			#read into pending
			if len(pending) < self.server._max_pending:
				try:
					req = self.server._reorder_queue.get(True, block_time)
				except Queue.Empty:
					pass
				else:
					if req is None:
						self.server._handle_queue.put(None)
					else:
						pending.append(req)
			
			#move from pending into current
			if pending and len(current) < self.server.max_parallel_requests:
				ri = -1
				ei = exclusive_pending()
				
				if not current:
					ri = ei if ei != -1 else 0
				elif ei == -1 and not exclusive_current():
					for i, req in enumerate(pending):
						if not same_session_current(req) and \
								(req[1] == Isolation.UNRESTRICTED or (req[1] == Isolation.QUEUE_SIMILAR and not similar_current(req))):
							ri = i
							break
				
				if ri != -1:
					req = pending.pop(ri)
					self.server._handle_queue.put(req)
					current.append(req)
			
			#read from server._finish_queue
			try:
				req = self.server._finish_queue.get(True, block_time)
			except Queue.Empty:
				pass
			else:
				if req is None:
					nones += 1
					if nones == self.server.max_parallel_requests:
						break
				else:
					current.remove(req)


class _HandleThread(_ServerThread):
	""" repeatedly get request from server._handle_queue, handle it, and then put it in server._finish_queue.
		there must be as many of these per HTTPServer as there are _ReadThreads. """
	
	def run(self):
		while True:
			rt = self.server._handle_queue.get()
			
			if rt is None:
				self.server._finish_queue.put(None)
				break
			else:
				req, isolation = rt
				req.handle_time = time.time()
				resp = self._send_to_handler(req)
				req._store_session()
				req._set_sid_cookie(resp)
				try:
					resp._write(req)
					req._file.close()
					req._sock.close()
				except socket.error:
					pass
				except:
					_LOGGER.exception('exception writing response')
				self.server.session_manager.clean()
				self.server._finish_queue.put(rt)

	def _send_to_handler(self, request):
		handler = self.server.handler
		try:
			return handler.handle(request)
		except Exception, e:
			
			if isinstance(e, NotFound):
				m = handler.notfound
			elif isinstance(e, Forbidden):
				m = handler.forbidden
			else:
				m = handler.error
			
			try:
				return m(request)
			except:
				_LOGGER.exception('exception in %s handler:' % m.__name__)
				return Response(_STANDARD_ERROR_PAGE, status = 500)


class Server:
	def __init__(self, addr, handler, session_manager = sessions.Manager(DefaultSession), proxies = 0, max_parallel_requests = 8, max_urlencoded_post_length = 256 * 1024):
		"""
			addr is a tuple (host, port) to listen on.
			proxies is an integer specifying how many trusted proxies are in front of this server.
			handler is a subclass of Handler.
		"""
		
		self.addr = addr
		self.proxies = proxies
		self.max_parallel_requests = max_parallel_requests
		self.max_urlencoded_post_length = max_urlencoded_post_length
		self.session_manager = session_manager
		self.handler = handler
		
		self._max_pending = max_parallel_requests * 2	# max num of (read) requests to keep pending (not-handling)
		
		self._read_queue = Queue.Queue(2)				# size is somewhat arbitrary, but shouldn't be infinite
		self._reorder_queue = Queue.Queue(2)			# ditto
		self._handle_queue = Queue.Queue(0)
		self._finish_queue = Queue.Queue(0)
		
		self._quit = False
		
		def set_quit(number, frame):
			self._quit = True
		signal.signal(signal.SIGINT, set_quit)
		signal.signal(signal.SIGTERM, set_quit)
		
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		sock.settimeout(None)
		sock.bind(addr)
		sock.listen(5)
		
		read_threads = [_ReadThread(self) for i in xrange(self.max_parallel_requests)]
		reorder_thread = _ReorderThread(self)
		handle_threads = [_HandleThread(self) for i in xrange(self.max_parallel_requests)]
		
		while not self._quit:
			try:
				self._read_queue.put(sock.accept())
			except:
				self._quit = True
		
		sock.close()
		for thread in read_threads:
			self._read_queue.put(None)
		for thread in read_threads + [reorder_thread] + handle_threads:
			thread.join()


if __name__ == '__main__':
	class TestHandler(Handler):
		def handle(self, request):
			return Response('<html><head><title></title></head><body>hello.jpg</body></html>')
	logging.basicConfig()
	Server(('127.0.0.1', 8000), TestHandler(), max_parallel_requests = 8)
