""" for importing posts/threads from kusaba/wakaba/etc boards """


import re, time, calendar, os.path, sys, glob


import util


DEFAULT_ENCODING = 'utf-8'
DEFAULT_DATE_FORMAT = '%y/%m/%d(%a)%H:%M'


def _parse_date(d, format):
	try:
		return calendar.timegm(time.strptime(re.sub(r'\s+', '', d), format))
	except:
		return int(time.time())


def _extract_comments(url, page, date_format):
	
	comments = []
	
	for i, m in enumerate(re.finditer(r'name">(?P<name>[^<]*).*?</span>\s*(<span class="postertrip">(?P<tripcode>.*?)</span>)?(?P<date>[^<]*).*?(No\.)?(?P<id>\d+)</a>.*?blockquote>(?P<comment>.*?)</blockquote', page, re.DOTALL)):
		g = m.groupdict()
		m = re.search(r'(?<=href=")[^"]*?src/(?P<fname>[^"\'>]+?)(?=")', m.group() if i else page)
		g['img'] = m.group('fname') if m else None
		g['id'] = int(g['id'])
		g['name'] = util.strip_html(g['name'] or '')
		g['tripcode'] = util.strip_html(g['tripcode'] or '')
		g['date'] = _parse_date(g['date'], date_format)
		comments += [g]
	
	def ref_sub(m):
		id = int(m.group('id'))
		for i, c in enumerate(comments):
			if c['id'] == id:
				return '>>%d' % i
		return '>>%d' % id
	
	def html_ents(s):
		s = s.replace('&lt;', '<').replace('&gt;', '>').replace('&quot;', '"').replace('&amp;', '&')
		return re.sub(r'&#(\d+);', lambda m: unichr(int(m.group(1))), s)
		
	#second pass needed for correct >>X numbers
	for c in comments:
		cc = c['comment']
		cc = re.sub(r'[\r\n]', '', cc)
		cc = re.sub(r'<br[^>]*>', '\r\n', cc)
		cc = re.compile(r'<p>(.*?)</p>', re.DOTALL).sub(r'\1' + '\r\n\r\n', cc)
		cc = cc.strip()
		cc = re.sub(r'<.*?>', '', cc)
		cc = html_ents(cc)
		cc = re.sub(r'>>(?P<id>\d+)', ref_sub, cc)
		c['comment'] = cc
	
	return comments



def _get_yn():
	while True:
		input = raw_input('Please enter y or n: ').lower()
		if input == 'y':
			return True
		if input == 'n':
			return False
		

def _transfer_board(path, encoding, date_format):
	""" transfer a single board, giving command line prompts as needed. """
	
	import database, uploaded_files, util.http
	
	board_dir = os.path.split(path)[1]
	res, src = os.path.join(path, 'res'), os.path.join(path, 'src')
	threads = [f for f in os.listdir(res) if re.match(r'^\d+\.', f)]
	
	global_db = database.Global()
	board_db = global_db.get_board(board_dir)
	
	print 'Do you wish to import the board: /%s/ (%d threads found) ?' % (board_dir, len(threads))
	
	if board_db is None:
		print '/%s/ does not currently exist in img2, so a new board will be created with default settings.' % board_dir
	else:
		print '/%s/ already exists in img2, so the threads will be imported into this board.' % board_dir
	
	if not _get_yn():
		print '/%s/ will not be imported.' % board_dir
	else:
		print 'importing /%s/ ...' % board_dir
		
		if board_db is None:
			board_db = global_db.make_board(board_dir)
		
		for thread in threads:
			
			print '\timporting thread: %s ...' % thread
			comments = _extract_comments(thread, open(os.path.join(res, thread), 'rb').read().decode(encoding, 'replace'), date_format)
			
			for i, c in enumerate(comments):
				
				file_md5, file_meta, temp_file = None, None, None
				if c['img']:
					try:
						hup = util.http.UploadedFile(c['img'], open(os.path.join(src, c['img']), 'rb'))
						file_md5, file_meta, temp_file = uploaded_files.check(hup, board_db)
					except uploaded_files.BadFile, e:
						print '\t\tfile not allowed: "%s": %s; it won\'t be imported' % (c['img'], str(e))
					except:
						print '\t\terror reading file: "%s"; it won\'t be imported' % c['img']
				
				if i == 0:
					dt, thread_id = board_db.add_new_thread('', False, False, True, c['date'], c['name'], c['tripcode'], False, c['comment'], file_md5, file_meta, None, ignore_constraints = True)
				else:
					board_db.add_new_reply(thread_id, c['date'], c['name'], c['tripcode'], False, c['comment'], file_md5, file_meta, None, True, None, None, None, ignore_constraints = True)
				
				if file_md5:
					uploaded_files.save(temp_file, board_db, thread_id, i, file_meta)
			
		print '... finished importing /%s/' % board_dir


def transfer(path, encoding = DEFAULT_ENCODING, date_format = DEFAULT_DATE_FORMAT):
	""" transfer all boards found under the specified path. """
	
	boards = glob.glob(os.path.join(path, '*', 'res'))
	
	if not boards:
		print 'no boards were found at the specified path: nothing to import'
	else:
		for b in boards:
			_transfer_board(os.path.split(b)[0], encoding, date_format)


if __name__ == '__main__':
	import util
	print util.format_time(_parse_date('dasdad> 09/05/10(Sun)12:19</span'))
	print util.format_time(_parse_date(' 07/05/2009 09:01 </la'))
