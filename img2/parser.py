""" handles converting user comment input to displayable html """

import re

import common, database

from util import re_msplit, clamp_str, html_escape


_URL_REGEX = re.compile(r'(?:(?:http|https|irc)://)(?:(?:%[0-9a-fA-F]{2})|[0-9a-zA-Z;/\?:@&\=\+\$#\-_\.~\*!\'])+')
_LONG_WORD_REGEX = re.compile(r'\S{%d}(?=\S)' % common.MAX_COMMENT_WORD_LENGTH)
_YOUTUBE_REGEX = re.compile(r'^http://(?:(?:www|[a-z]{2}).)?youtube\.com/watch\?.*v=(?P<id>[-a-z0-9_]{11})', re.IGNORECASE)
_EMAIL_REGEX = re.compile(r'[a-z0-9._%+-]+@[a-z0-9-]+(?:\.[a-z0-9-]+)+')

#types of chain links:
#	absolute:
#		>>/board/thread#post
#		>>/board/thread
#		>>/board/
#	board relative:
#		>>/thread
#		>>/thread#post
#	thread relative:
#		>>post
_CHAIN_REGEX = re.compile(	r'>>(?:' +
							r'(?P<ab>/(?P<ab_b>%s)/(?:(?P<ab_t>\d+)(?:#(?P<ab_p>\d+))?)?)' % common.BOARD_DIR_REGEX_INTERNAL +
							r'|(?P<br>/(?P<br_t>\d+)(?:#(?P<br_p>\d+))?)' +
							r'|(?P<tr>\d+)'
							r')'	)


def parse(input, context):
	""" input must be a string, context must be (board_db, thread_id, post_id) """
	
	output = []
	max_youtubes = [context[0].config.max_youtube_embeds_per_post]
	

	def parse_links(sub_input, in_quote):
		for m in re_msplit(_URL_REGEX, sub_input):
			if isinstance(m, basestring):
				parse_emails(m, in_quote)
			else:
				link = m.group()
				ym = _YOUTUBE_REGEX.search(link)
				if ym and not in_quote and max_youtubes[0]:
					ytid = ym.group('id')
					output.append('<div class="youtube"><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/%s&hl=en"></param><embed src="http://www.youtube.com/v/%s&hl=en" type="application/x-shockwave-flash" width="425" height="344"></embed></object></div>' % (ytid, ytid))
					max_youtubes[0] -= 1
				else:
					output.append('<a class="posted_url" href="%s%s" rel="nofollow">%s</a>' % (context[0].config.posted_url_prefix, html_escape(link), html_escape(clamp_str(link, common.MAX_COMMENT_WORD_LENGTH, '...'))))


	def parse_emails(sub_input, in_quote):
		for m in re_msplit(_EMAIL_REGEX, sub_input):
			if isinstance(m, basestring):
				parse_chains(m, in_quote)
			else:
				mail = m.group()
				output.append('<a class="posted_url" href="mailto:%s">%s</a>' % (html_escape(mail), html_escape(clamp_str(mail, common.MAX_COMMENT_WORD_LENGTH, '...'))))


	def parse_chains(sub_input, in_quote):
		for m in re_msplit(_CHAIN_REGEX, sub_input):
			if isinstance(m, basestring):
				parse_long_words(m, in_quote)
			else:
				if in_quote:
					output.append('</span>')
				
				d = m.groupdict()
				b = t = p = None
				href = None
				
				if d['ab']:
					b = d['ab_b']
					t = d['ab_t']
					p = d['ab_p']
					if context[0].global_db.get_board(b):
						if t and p:
							href = '/%s/%s#%s' % (b, t, p)
						elif t:
							href = '/%s/%s' % (b, t)
						else:
							href = '/%s/' % b
							
				elif d['br']:
					t = d['br_t']
					p = d['br_p']
					if p:
						href = '/%s/%s#%s' % (context[0].dir, t, p)
					else:
						href = '/%s/%s' % (context[0].dir, t)
				
				elif d['tr']:
					p = d['tr']
					if int(p) < context[2]:
						href = '/%s/%d#%s' % (context[0].dir, context[1], p)
				
				if href:
					output.append('<a class="chain" href="%s">&gt;&gt;%s</a>' % (href, m.group()[2:]))
				else:
					output.append('<span class="invalid_chain">&gt;&gt;%s</span>' % (m.group()[2:]))
				
				if in_quote:
					output.append('<span>')
				
	
	def parse_long_words(sub_input, in_quote):
		for m in re_msplit(_LONG_WORD_REGEX, sub_input):
			if isinstance(m, basestring):
				parse_post_filters(m, in_quote)
			else:
				output.append(html_escape(m.group()) + ' ')
	
	
	def parse_post_filters(sub_input, in_quote):
		froms, regex = context[0].config.post_filters_parsed
		if froms:
			for m in re_msplit(regex, sub_input):
				if isinstance(m, basestring):
					output.append(html_escape(m))
				else:
					output.append(froms[m.group()])
		else:
			output.append(html_escape(sub_input))


	# split into lines and parse each line separately
	for i, line in enumerate(input.splitlines()):
		if i:
			output.append('<br>')
		if line.startswith('>'):
			output.append('<span class="quote">')
			parse_links(line, True)
			output.append('</span>')
		else:
			parse_links(line, False)
	output = ''.join(output)

	if len(input) <= context[0].config.max_emote_length and input == output:	# dont allow html in emotes
		return '<div class="emote">%s</div>' % output
	else:
		return output


def blurb(thread_id, subject, head_status, head_comment):
	""" the blurb for a thread is what is displayed in thread titles """
	if head_status == database.PostStatus.UNDELETED:
		if subject:
			return subject
		if head_comment:
			head_comment = re.sub(r'(\r\n)+', ' ', head_comment)
			return clamp_str(head_comment, common.MAX_BLURB_LENGTH, '...')
	return 'Thread %d' % thread_id
