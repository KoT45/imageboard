""" every function in this module returns a specific page """


from __future__ import with_statement

import logging, re, time

import common, captchas, util, database, uploaded_files, pagecache, cookies, tripcodes, proxycheck, util.http as http

from util import i18n, limit, psint, html_escape, ObjDict
from util.http import Response, RedirectResponse, NotFound, Forbidden
from common import board_link, thread_link, post_link, render_to_response, check_age, cache, mod_login, mod_only_board_check


_ = lambda s: s	# need these to be localised
CONFIG_AREA_SECTIONS = (
	('overview', _('Overview')),
	('mods', _('Mods')),
	('modlog', _('ModLog')),
	('bans', _('Bans')),
	('file-bans', _('File Bans')),
	('posts-by-ip', _('Posts By IP')),
	('reviewed-posts', _('Reviewed Posts')),
	('existing-boards', _('Existing Boards')),
	('new-board', _('New Board')),
	('change-password', _('Change Password')),
	('last-visitors', _('Last visitors'))
)



def captcha(request, global_db, code):
	data, mime = captchas.make(global_db, code, request.session.id)
	return Response(data, mime)


def confirm_overage(request, global_db):
	response = RedirectResponse(request['referer'] or '/')
	cookies.set_overage(response)
	return response


@check_age
@cache
def index(request, global_db):
	mod = request.session.mod
	boards = global_db.boards if mod else global_db.public_boards()
		
	global_db.visitors_update()
	if len(boards) == 1:
		return RedirectResponse('/%s/' % boards[0].dir)
	else:
		return render_to_response(request, global_db, 'index.html', global_db.get_index_data(mod))


@mod_only_board_check
@check_age
@cache
def view_board(request, global_db, board_db, page):
	data = board_db.get_view_board_data(page)
	if data is None:
		raise NotFound

	global_db.new_visitor(board_db.config.title, request.remote_host)
	global_db.visitors_update()

	return render_to_response(request, board_db, 'view_board.html', data)


@mod_only_board_check
@check_age
@cache
def view_thread(request, global_db, board_db, thread_id):
	data = board_db.get_view_thread_data(thread_id)
	if not data:
		raise NotFound

	global_db.new_visitor(board_db.config.title, request.remote_host, thread_id)
	global_db.visitors_update()

	return render_to_response(request, board_db, 'view_thread.html', data)


@mod_only_board_check
@check_age
@cache
def list(request, global_db, board_db):
	data = board_db.get_list_data()
	return render_to_response(request, board_db, 'list.html', data)


@cache
def rss(request, global_db):
	data = global_db.get_index_rss_data(request.server_name)
	return render_to_response(request, global_db, 'rss.xml', data)


@cache
def board_rss(request, global_db, board_db):
	if board_db.config.mod_only:
		raise Forbidden
	data = board_db.get_board_rss_data(request.server_name)
	return render_to_response(request, board_db, 'rss.xml', data)


@cache
def thread_rss(request, global_db, board_db, thread_id):
	if board_db.config.mod_only:
		raise Forbidden
	data = board_db.get_thread_rss_data(request.server_name, thread_id)
	if not data:
		raise NotFound
	return render_to_response(request, board_db, 'rss.xml', data)


@mod_only_board_check
@cache
def fetch_post(request, global_db, board_db, thread_id, post_id):
	data = board_db.get_post_data(thread_id, post_id)
	if not data:
		raise NotFound
	response = render_to_response(request, board_db, 'fetch_post.html', data)
	if not request.session.mod:
		response.expire(5)
	return response


@mod_only_board_check
@cache
def fetch_posts(request, global_db, board_db, thread_id, id_start, id_end):
	if id_end is None:
		id_end = id_start+1
	data = board_db.get_posts_data(thread_id, id_start, id_end)
	if not data:
		raise NotFound
	response = render_to_response(request, board_db, 'fetch_posts.html', data)
	if not request.session.mod:
		response.expire(5)
	return response


@mod_only_board_check
@check_age
def new_thread(request, global_db, board_db):
	_ = board_db._

	if request.method == 'GET':
		return render_to_response(request, board_db, 'new_thread.html', ObjDict(board_db = board_db, new_thread = ObjDict(database.NEW_THREAD)))
	
	# gather input
	sent = ObjDict(
		name = request.post('name', '').strip()[:board_db.config.max_name_length],
		tripcode = request.post('tripcode', ''),
		subject = request.post('subject', '').strip()[:board_db.config.max_subject_length],
		comment = request.post('comment', '').strip(),
		file = request.files('file'),
		captcha_code = request.post('captcha_code', ''),
		captcha_solution = request.post('captcha_solution', ''),
		noko = 'noko' in request.post,
		ip = request.remote_host,
		sticky = 'sticky' in request.post,
		locked = 'locked' in request.post,
		bumpable = 'bumpable' in request.post,
		mod = 'mod' in request.post,
		javascript_anti_spam = request.post('javascript_anti_spam'),
	)
	
	def set_cookies(response):
		cookies.set_noko(response, sent.noko)
		cookies.set_name(response, sent.name)
		cookies.set_tripcode(response, sent.tripcode)
		cookies.set_show_mod_flag(response, sent.mod)
		return response
	
	def error(msg):
		return set_cookies(render_to_response(request, board_db, 'new_thread.html', ObjDict(board_db = board_db, error_msg = msg, new_thread = sent)))
	
	ban = global_db.get_ban(sent.ip)
	if ban:
		if ban.board_name == '' or ban.board_name == board_db.config.title:
			return set_cookies(render_to_response(request, board_db, 'new_thread.html', ObjDict(board_db = board_db, ban = ban, new_thread = sent)))
	
	if board_db.config.block_proxies_fully and proxycheck.proxy(board_db, sent.ip):
		return error(_('Proxies are not allowed to post.'))
	
	if sent.file and board_db.config.block_proxies_files and proxycheck.proxy(board_db, sent.ip):
		return error(_('Proxies are not allowed to post files.'))
	
	if not sent.comment and not (board_db.config.allow_no_comment_with_file and sent.file):
		return error(_('You must write a comment.'))
	
	if len(sent.comment) > board_db.config.max_comment_length:
		return error(_('Comment too long, max allowed length is %d characters.') % board_db.config.max_comment_length)
	
	if board_db.config.thread_captcha:
		if sent.captcha_code in request.session.used_captcha_codes or \
				not captchas.valid(global_db, sent.captcha_code, request.session.id, sent.captcha_solution):
			return error(_('Incorrect captcha solution.'))

	if board_db.config.javascript_anti_spam:
		if sent.javascript_anti_spam != '':
			return error(_('You don\'t seem to have Javascript enabled, which is required for posting on this board.'))
	
	if sent.file is None and not board_db.config.allow_thread_without_file:
		return error(_('You must supply a file with a new thread.'))
	
	if sent.file and board_db.files_allowed():
		try:
			file_md5, file_meta, temp_file = uploaded_files.check(sent.file, board_db)
		except uploaded_files.BadFile, e:
			return error(str(e))
	else:
		file_md5, file_meta = None, None
	
	try:
		deleted_threads, thread_id = board_db.add_new_thread(	sent.subject,
																sent.sticky if request.session.mod else False,
																sent.locked if request.session.mod else False,
																sent.bumpable if request.session.mod else True,
																int(time.time()),
																(sent.name if board_db.config.allow_names else '') or board_db.get_default_name(),
																tripcodes.make(global_db, sent.tripcode) if board_db.config.allow_tripcodes else '',
																sent.mod if request.session.mod else False,
																sent.comment,
																file_md5,
																file_meta,
																sent.ip	)
	except database.DoubleThread:
		return error(_('Your thread was not created - it looks like you are double posting.'))
	except database.Flooding:
		return error(_('Your thread was not created - you are making threads too fast. Slow down!'))
	except database.DuplicateFile:
		return error(_('Your thread was not created - you are posting a file that already exists.'))
	except database.BannedFile:
		return error(_('Your thread was not created - the file you are posting is banned.'))
	
	if file_md5:
		uploaded_files.save(temp_file, board_db, thread_id, 0, file_meta)
	
	if board_db.config.thread_captcha:
		request.session.used_captcha_codes.add(sent.captcha_code)

	global_db.delete_expired_bans()
	board_db.anonymize_old_posts()
	pagecache.thread_modified(board_db, thread_id)		#delete caches of pages that should now contain this thread
	for thread in deleted_threads:
		pagecache.thread_modified(board_db, thread)
	
	path = '/%s/' % board_db.dir
	if sent.noko:
		path += str(thread_id)
	response = set_cookies(RedirectResponse(path))
	cookies.set_post_verifier(response, board_db, thread_id, 0)
	return response


@mod_only_board_check
@check_age
def new_reply(request, global_db, board_db, thread_id):
	_ = board_db._

	data = board_db.get_new_reply_data(thread_id)
	if data is None:
		raise NotFound
	thread = data.thread
	
	if request.method == 'GET':
		new_post = ObjDict(database.NEW_POST)
		if 'q' in request.get:
			new_post.comment = '>>%d\r\n\r\n' % psint(request.get('q'))
		return render_to_response(request, board_db, 'new_reply.html', ObjDict(board_db = board_db, thread = thread, new_post = new_post))
	
	# gather input
	sent = ObjDict(
		name = request.post('name', '').strip()[:board_db.config.max_name_length],
		tripcode = request.post('tripcode', ''),
		comment = request.post('comment', '').strip(),
		file = request.files('file'),
		captcha_code = request.post('captcha_code', ''),
		captcha_solution = request.post('captcha_solution', ''),
		noko = 'noko' in request.post,
		bump = 'bump' in request.post,
		locked = 'locked' in request.post,
		sticky = 'sticky' in request.post,
		bumpable = 'bumpable' in request.post,
		mod = 'mod' in request.post,
		ip = request.remote_host,
		javascript_anti_spam = request.post('javascript_anti_spam'),
	)

	def set_cookies(response):
		cookies.set_noko(response, sent.noko)
		cookies.set_name(response, sent.name)
		cookies.set_tripcode(response, sent.tripcode)
		cookies.set_show_mod_flag(response, sent.mod)
		return response
	
	def error(msg):
		return set_cookies(render_to_response(request, board_db, 'new_reply.html', ObjDict(board_db = board_db, error_msg = msg, thread = thread, new_post = sent)))
	
	ban = global_db.get_ban(sent.ip)
	if ban:
		return set_cookies(render_to_response(request, board_db, 'new_reply.html', ObjDict(board_db = board_db, ban = ban, thread = thread, new_post = sent)))
	
	if board_db.config.block_proxies_fully and proxycheck.proxy(board_db, sent.ip):
		return error(_('Proxies are not allowed to post.'))
	
	if sent.file and board_db.config.block_proxies_files and proxycheck.proxy(board_db, sent.ip):
		return error(_('Proxies are not allowed to post files.'))
	
	if thread.next_id >= board_db.config.max_thread_posts:
		return error(_('This thread is full.'))
	
	if not sent.comment and not (board_db.config.allow_no_comment_with_file and sent.file):
		return error(_('You must write a comment.'))
	
	if len(sent.comment) > board_db.config.max_comment_length:
		return error(_('Comment too long, max allowed length is %s characters.') % board_db.config.max_comment_length)
	
	if board_db.config.reply_captcha:
		if sent.captcha_code in request.session.used_captcha_codes or \
				not captchas.valid(global_db, sent.captcha_code, request.session.id, sent.captcha_solution):
			return error(_('Incorrect captcha solution.'))


	if board_db.config.javascript_anti_spam:
		if sent.javascript_anti_spam != '':
			return error(_('You don\'t seem to have Javascript enabled, which is required for posting on this board.'))
	
	if sent.file is None and not board_db.config.allow_reply_without_file:
		return error(_('You must supply a file with a reply.'))
		
	if not request.session.mod and thread.locked:
		return error(_('This thread is locked.'))

	if sent.file and board_db.files_allowed():
		try:
			file_md5, file_meta, temp_file = uploaded_files.check(sent.file, board_db)
		except uploaded_files.BadFile, e:
			return error(str(e))
	else:
		file_md5, file_meta = None, None
	
	try:
		post_id = board_db.add_new_reply(	thread_id,
											int(time.time()),
										 	(sent.name if board_db.config.allow_names else '') or board_db.get_default_name(),
											tripcodes.make(global_db, sent.tripcode) if board_db.config.allow_tripcodes else '',
											sent.mod if request.session.mod else False,
											sent.comment,
											file_md5,
											file_meta,
											sent.ip,
											sent.bump,
											sent.sticky if request.session.mod else None,
											sent.locked if request.session.mod else None,
											sent.bumpable if request.session.mod else None	)
	except database.NoThread:
		return error(_('Your reply was not added - that thread does not exist.'))
	except database.ThreadFull:
		return error(_('Your reply was not added - the thread is full.'))
	except database.Flooding:
		return error(_('Your reply was not added - you are posting too fast. Relax!'))
	except database.DoublePost:
		return error(_('Your reply was not added - it looks like you are double posting.'))
	except database.DuplicateFile:
		return error(_('Your reply was not added - you are posting a file that already exists.'))
	except database.BannedFile:
		return error(_('Your reply was not added - the file you are posting is banned.'))
	
	if file_md5:
		uploaded_files.save(temp_file, board_db, thread_id, post_id, file_meta)
	
	if board_db.config.reply_captcha:
		request.session.used_captcha_codes.add(sent.captcha_code)

	global_db.delete_expired_bans()
	board_db.anonymize_old_posts()
	pagecache.thread_modified(board_db, thread_id)
	
	if sent.noko:
		path = '/%s/%d#%d' % (board_db.dir, thread_id, post_id)
	else:
		path = '/%s/' % board_db.dir
	
	response = set_cookies(RedirectResponse(path))
	cookies.set_post_verifier(response, board_db, thread_id, post_id)

	return response


@mod_login
def config(request, global_db, section):
	_ = global_db._

	def section_desc():
		for s, desc in CONFIG_AREA_SECTIONS:
			if s == section:
				return desc

	mod = request.session.mod
	admin = global_db.is_admin(mod)
	
	action = request.post('action')
	data = ObjDict(section = section, section_desc = section_desc(), msg = None, is_default_mod = global_db.is_default_mod(mod))
	
	
	if section == 'overview':
	
		data.update({	'sessions': len(request.server.session_manager),
						'page_cache': pagecache.size(),
						'parsed_comment_cache': global_db.parsed_comment_cache_size()	})
		return render_to_response(request, global_db, 'config_overview.html', data)
	

	elif section == 'bans':
		
		if action == 'add':
			now = int(time.time())
			ip = request.post('ip', '').strip()
			expires_parsed = util.parse_interval(request.post('expires', '').strip())
			reason = request.post('reason', '').strip()
			board_name = request.post('board_name').strip()
			
			if expires_parsed is None:
				data.msg = _('Ban not added: Invalid expiry time.')
			elif not database.valid_ban_ip(ip):
				data.msg = _('Ban not added: Invalid IP')
			else:
				global_db.add_ban(ip, (now + expires_parsed) if expires_parsed else 0, reason, board_name)
				global_db.modlog(mod, _('Added ban on %s for reason "%s" expiring in %s.') % (ip, html_escape(reason), util.format_interval(expires_parsed)))
				data.msg = _('Added Ban on %s. board name = %s') % (ip, board_name)
		
		elif action == 'delete':
			ip = request.post('ip', '')
			global_db.delete_ban(ip)
			global_db.modlog(mod, _('Deleted ban on %s.') % ip)
			data.msg = _('Deleted Ban on %s.') % ip
		
		filter = request.get('filter', '').strip()
		data.update(global_db.get_config_area_bans_data(filter))
		data.new_ban_ip = request.get('new_ban_ip', '')
		return render_to_response(request, global_db, 'config_bans.html', data)
	

	elif section == 'file-bans':
	
		if action:
			fh = [x for x in request.post('hashes', '').split() if len(x) == 32 and util.is_hex(x)]
			
			if action == 'ban':
				global_db.ban_files(fh)
				msg = _('Added %d hashes to banned file list.') % len(fh)
				global_db.modlog(mod, msg)
				data.msg = msg
			
			elif action == 'unban':
				global_db.unban_files(fh)
				msg = _('Removed %d hashes from banned file list.') % len(fh)
				global_db.modlog(mod, msg)
				data.msg = msg
	
		data.update(global_db.get_config_area_files_data())
		return render_to_response(request, global_db, 'config_file_bans.html', data)
	

	elif section == 'new-board':
	
		if admin and action == 'new_board':
			dir = request.post('dir', '')
			try:
				global_db.make_board(dir)
			except ValueError, e:
				data.msg = (_('Unable to make new board: "%s": ') % dir) + e.message
			else:
				global_db.modlog(mod, _('Created board %s.') % board_link(dir))
				return RedirectResponse('/%s/config' % dir)
	
		return render_to_response(request, global_db, 'config_new_board.html', data)
	

	elif section == 'existing-boards':
	
		if admin and action == 'update_board_config_defaults':
			for key, field in database.Board.CONFIG_FIELDS:
				try:
					global_db.set_board_config_default(key, field.cast(request.post(key)))
				except:
					pass
			msg = _('Updated board config defaults.')
			global_db.modlog(mod, msg)
			data.msg = msg
			pagecache.purge()
	
		data.update(global_db.get_config_area_existing_boards_data())
		return render_to_response(request, global_db, 'config_existing_boards.html', data)
	

	elif section == 'mods':
		
		if action == 'new_mod':
			name = request.post('name', '')
			as_admin = 'admin' in request.post
			
			if admin:
				if not global_db.is_mod(name):
					if re.match(common.MOD_NAME_REGEX, name):
						password = global_db.add_mod(name, as_admin)
						global_db.modlog(mod, _('Created new mod (%s) "%s".') % (_('admin') if as_admin else _('non admin'), name))
						data.msg = _('New mod "%s" created, their password is "%s". Tell them to log in and change it.') % (name, password)
					else:
						data.msg = _('Did not create new mod: "%s" is not a valid mod name.') % name
				else:
					data.msg = _('Did not create new mod: "%s" already exists.') % name
			else:
				data.msg = _('Did not create new mod: You are not an admin so you do not have the power.')
		
		if action == 'give_admin':
			name = request.post('name', '')
			
			if admin:
				if name in global_db.mods:
					if not global_db.mods[name][1]:
						global_db.set_mod_admin(name, True)
						msg = _('Gave admin to mod "%s".') % name
						global_db.modlog(mod, msg)
						data.msg = msg
					else:
						data.msg = _('Did not give admin: mod "%s" is already an admin.') % name
				else:
					data.msg = _('Did not give admin: mod "%s" does not exist.') % name
			else:
				data.msg = _('Did not give admin: You are not an admin yourself so you do not have the power.')
		
		if action == 'take_admin':
			name = request.post('name', '')
			
			if admin:
				if name != mod:
					if name in global_db.mods:
						if global_db.mods[name][1]:
							global_db.set_mod_admin(name, False)
							msg = _('Took admin from mod "%s".') % name
							global_db.modlog(mod, msg)
							data.msg = msg
						else:
							data.msg = _('Did not take admin: mod "%s" is not an admin.') % name
					else:
						data.msg = _('Did not take admin: mod "%s" does not exist.') % name
				else:
					data.msg = _('Did not take admin: You may not take admin from yourself.')
			else:
				data.msg = _('Did not take admin: You are not admin yourself so you do not have the power')
		
		if action == 'reset_password':
			name = request.post('name', '')
			
			if admin:
				if name in global_db.mods:
					password = global_db.reset_mod_password(name)
					global_db.modlog(mod, _('Reset password for mod "%s".') % name)
					data.msg = _('Reset password for mod "%s": it is now "%s". Tell them to log in and change it.') % (name, password)
				else:
					data.msg = _('Did not reset password: mod "%s" does not exist.') % name
			else:
				data.msg = _('Did not reset password: You are not an admin so you do not have the power.')
		
		if action == 'delete':
			name = request.post('name', '')
			
			if admin:
				if name != mod:
					if name in global_db.mods:
						global_db.delete_mod(name)
						msg = 'Deleted mod "%s".' % name
						global_db.modlog(mod, msg)
						data.msg = msg
					else:
						data.msg = _('Did not delete: mod "%s" does not exist.') % name
				else:
					data.msg = _('Did not delete mod "%s": you may not delete yourself.') % name
			else:
				data.msg = _('Did not delete mod "%s": You are not an admin so you do not have the power.')
		
		data.update(global_db.get_config_area_mods_data())
		return render_to_response(request, global_db, 'config_mods.html', data)
	

	elif section == 'modlog':
		data.update(global_db.get_config_area_modlog_data(request.get('offset')))
		return render_to_response(request, global_db, 'config_modlog.html', data)


	elif section == 'posts-by-ip':
		ip = request.get('ip', '')
		
		if action == 'delete':
			ip = request.post('ip', '')
			pb = request.post.get('board', [])
			for board_db in global_db.boards:
				if board_db.dir in pb:
					board_db.delete_posts_by_ip(ip)
				else:
					pt = map(psint, request.post.get(board_db.dir + '_thread', []))
					board_db.delete_posts_by_ip(ip, pt)
			data.msg = _('Deleted selected posts by IP "%s".') % ip
		
		data.update(global_db.get_config_area_posts_by_ip_data(ip))
		data.ip = ip
		return render_to_response(request, global_db, 'config_posts_by_ip.html', data)
	
	
	elif section == 'reviewed-posts':
		data.update(global_db.get_config_area_reviewed_posts_data())
		return render_to_response(request, global_db, 'config_reviewed_posts.html', data)
	

	elif section == 'change-password':
		
		if action == 'update':
			cp = request.post('current_password') or ''
			np = request.post('new_password') or ''
			cnp = request.post('confirm_new_password') or ''
			
			if global_db.get_mod(mod, cp):
				if np == cnp:
					if np:
						global_db.set_mod_password(mod, np)
						global_db.modlog(mod, _('Changed password.'))
						data.msg = _('Password changed.')
					else:
						data.msg = _('Password not changed: You entered an empty password, which is not allowed.')
				else:
					data.msg = _('Password not changed: You failed to confirm your new password.')
			else:
				data.msg = _('Password not changed: You failed to input your current password correctly.')
		
		return render_to_response(request, global_db, 'config_change_password.html', data)


	elif section == 'last-visitors':
		
		return render_to_response(request, global_db, 'last_visitors.html', data)
	

@mod_login
def board_config(request, global_db, board_db):
	_ = board_db._

	mod = request.session.mod
	action = request.post('action')
	msg = None
	
	if action and not global_db.is_admin(mod):	#non-admin mods can't edit config
		raise Forbidden
	
	if action == 'update':
		for key, field in database.Board.CONFIG_FIELDS:
			try:
				board_db.set_config(key, field.cast(request.post(key)), ('%s_use_default' % key) in request.post)
			except:
				pass
		msg = _('Updated Board Config.')
		global_db.modlog(mod, _('Updated config for board %s.') % board_link(board_db.dir))
		pagecache.purge()
	
	elif action == 'rename':
		old_dir = board_db.dir
		new_dir = request.post('new_dir', '')
		try:
			global_db.rename_board(board_db, new_dir)
		except ValueError, e:
			msg = _('Did not rename Board: ') + e.message
		else:
			global_db.modlog(mod, _('Renamed board %s to %s.') % (board_link(old_dir), board_link(new_dir)))
			pagecache.purge()
			return RedirectResponse('/')
		
	elif action == 'delete':
	
		if 'confirm' in request.post:
			global_db.delete_board(board_db)
			global_db.modlog(mod, _('Deleted board %s.') % board_link(board_db.dir))
			pagecache.purge()
			return RedirectResponse('/')
	
	data = board_db.get_board_config_data()
	data.msg = msg
	return render_to_response(request, board_db, 'board_config.html', data)


def mod_logout(request, global_db):
	if request.session.mod and request.session.mod_logout_confirm == request.get('c'):
		request.session.mod = None
		response = RedirectResponse('/')
		cookies.delete_mod(response)
		return response
	else:
		raise Forbidden


@mod_only_board_check
@check_age
def options(request, global_db, board_db, thread_id, post_id):
	_ = board_db._

	action = request.post('action')
	msg = None
	correct_cookie = cookies.check_post_verifier(request, board_db, thread_id, post_id)
	
	if request.method == 'GET' or not action:
		data = board_db.get_post_options_data(thread_id, post_id, request.remote_host)
		if data is None or data.status != database.PostStatus.UNDELETED:
			raise NotFound
		data.msg = msg
		data.correct_cookie = correct_cookie
		return render_to_response(request, board_db, 'options.html', data)
	
	if not board_db.post_not_deleted(thread_id, post_id):
		raise NotFound
		
	if action == 'flag':
		board_db.flag_post(thread_id, post_id, request.remote_host)
		msg = _('Post Flagged.')
	
	elif action == 'cookie_delete':
		if cookies.check_post_verifier(request, board_db, thread_id, post_id):
			board_db.delete_post(thread_id, post_id, False, True)
			pagecache.thread_modified(board_db, thread_id)
			msg = _('Post Deleted.')
		else:
			msg = _('Invalid Cookie.')
	
	elif action == 'tripcode_delete':
		trip = board_db.get_post_tripcode(thread_id, post_id)
		if trip and tripcodes.make(global_db, request.post('tripcode', '')) == trip:
			board_db.delete_post(thread_id, post_id, False, True)
			pagecache.thread_modified(board_db, thread_id)
			msg = _('Post Deleted.')
		else:
			msg = _('Invalid Tripcode.')
	
	data = board_db.get_post_options_data(thread_id, post_id, request.remote_host)
	if data is None:
		raise NotFound
	data.msg = msg
	data.correct_cookie = correct_cookie
	return render_to_response(request, board_db, 'options.html', data)


@mod_login
def mod_options(request, global_db, board_db, thread_id, post_id):
	_ = board_db._

	mod = request.session.mod
	
	if request.method == 'GET':
		data = board_db.get_post_mod_options_data(thread_id, post_id)
		if data is None:
			raise NotFound
		data.msg = None
		return render_to_response(request, board_db, 'mod_options.html', data)
	
	action = request.post('action')
	msg = None
	
	if action == 'delete_thread':
		global_db.modlog(mod, _('Deleted %s') % thread_link(board_db.dir, thread_id))
		board_db.delete_thread(thread_id)
		pagecache.thread_modified(board_db, thread_id)
		return RedirectResponse('/%s/' % board_db.dir)
	
	elif action == 'move_thread':
		dest_board_db = global_db.get_board(request.post('dest_board'))
		keep_locked_copy = 'keep_locked_copy' in request.post
		if dest_board_db:
			try:
				dest_tid = global_db.move_thread(board_db, thread_id, dest_board_db, keep_locked_copy)
			except ValueError, e:
				msg = _('Did not move Thread: ') + e.message
			else:
				global_db.modlog(mod, _('Moved %s to %s.') % (thread_link(board_db.dir, thread_id), thread_link(dest_board_db.dir, dest_tid)))
				pagecache.thread_modified(board_db, thread_id)
				if keep_locked_copy:
					msg = _('Moved Thread to %s. Locked this copy.') % thread_link(dest_board_db.dir, dest_tid)
				else:
					return RedirectResponse('/%s/%d' % (dest_board_db.dir, dest_tid))
	
	elif action == 'modify_thread':
		sticky = 'sticky' in request.post
		locked = 'locked' in request.post
		bumpable = 'bumpable' in request.post
		subject = request.post('subject', '').strip()[:board_db.config.max_subject_length]
		
		board_db.edit_thread(thread_id, subject, sticky, locked, bumpable)
		global_db.modlog(mod, _('Modified Thread %s') % thread_link(board_db.dir, thread_id))
		pagecache.thread_modified(board_db, thread_id)
		msg = _('Modified Thread.')
	
	elif action == 'modify_post':
		ps = database.PostStatus
		
		reviewed = 'reviewed' in request.post
		status = limit(psint(request.post('status')), ps.UNDELETED, ps.USER_DELETED, ps.MOD_DELETED)
		name = request.post('name', '')[:board_db.config.max_name_length]
		tripcode = request.post('tripcode', '')
		comment = request.post('comment', '')[:board_db.config.max_comment_length]
		show_mod = 'mod' in request.post
		delete_file = 'delete_file' in request.post
		ban_file = 'ban_file' in request.post
		
		upload_error = None
		if request.files('file'):
			try:
				new_file_md5, new_file_meta, temp_file = uploaded_files.check(request.files('file'), board_db)
			except uploaded_files.BadFile, e:
				upload_error = _('Error uploading file: %s') % str(e)
			else:
				uploaded_files.save(temp_file, board_db, thread_id, post_id, new_file_meta)
		else:
			new_file_md5, new_file_meta = None, None
		
		if upload_error:
			msg = upload_error
		else:
			if ban_file:
				file_md5 = board_db.get_post_file_md5(thread_id, post_id)
				if file_md5:
					global_db.add_file_ban(file_md5)
					global_db.modlog(mod, _('Banned file posted with %s - %s.') % (post_link(board_db.dir, thread_id, post_id), util.hex(file_md5)))
			
			board_db.edit_post(thread_id, post_id, status, name, tripcode, show_mod, comment, delete_file, new_file_md5, new_file_meta, reviewed)
			global_db.modlog(mod, _('Modified post %s.') % post_link(board_db.dir, thread_id, post_id))
			pagecache.thread_modified(board_db, thread_id)
			msg = _('Modified Post.')

	data = board_db.get_post_mod_options_data(thread_id, post_id)
	if data is None:
		raise NotFound
	data.msg = msg
	return render_to_response(request, board_db, 'mod_options.html', data)


@mod_login
def quick_mod_action(request, global_db, board_db, thread_id, post_id):
	_ = board_db._

	mod = request.session.mod
	
	if request.method != 'POST':
		raise Forbidden
	if not board_db.post_not_deleted(thread_id, post_id):
		raise NotFound
	
	ban_ip = 'ban_ip' in request.post
	ban_and_delete_file = 'ban_and_delete_file' in request.post
	delete = 'delete' in request.post
	
	if ban_ip:
		ban_len = global_db.board_config_defaults.default_ban_length
		expires = (int(time.time()) + ban_len) if ban_len else 0
		reason = global_db.board_config_defaults.default_ban_reason
		board_db.extend_post_ip_ban(thread_id, post_id, expires, reason)
	
	if ban_and_delete_file:
		board_db.ban_and_delete_post_file(thread_id, post_id)
	
	if delete:
		board_db.delete_post(thread_id, post_id, True, True)
	
	msg = _('Quick Post Action for %s') % post_link(board_db.dir, thread_id, post_id)
	if ban_ip:
		msg += '; ' + _('Ban IP')
	if ban_and_delete_file:
		msg += '; ' + _('Ban and Delete file')
	if delete:
		msg += '; ' + _('Delete')
	global_db.modlog(mod, msg)
	
	pagecache.thread_modified(board_db, thread_id)
	
	return Response('Done', 'text/plain; charset=utf-8')


class Handler(http.RegexHandler):
	
	def __init__(self):
		self.global_db = database.Global()
		
		def board_db_trans(request, global_db, dir):
			board_db = global_db.get_board(dir)
			if board_db is None:
				raise http.NotFound
			else:
				return board_db
		
		def int_trans(request, global_db, i):
			return int(i)
		
		# funtions to transform arguments to view funtions (groups from the regex) before they are passed to the view functions
		trans = dict(
			board_db = board_db_trans,
			page = int_trans,
			thread_id = int_trans,
			post_id = int_trans,
			id_start = int_trans,
			id_end = int_trans,
			id = int_trans,
		)
		
		b = r'^(?P<board_db>%s)/' % common.BOARD_DIR_REGEX_INTERNAL
		
		ex = http.Isolation.EXCLUSIVE
		qs = http.Isolation.QUEUE_SIMILAR
		ur = http.Isolation.UNRESTRICTED
		
		urls = (
			(r'^$', index, {}, qs),
			(r'^~rss', rss, {}, qs),
			(r'^~confirm-overage$', confirm_overage, {}, qs),
			(r'^~captcha/(?P<code>%s)$' % captchas.CODE_REGEX, captcha, {}, ur),
			(r'^~config$', config, {'section': CONFIG_AREA_SECTIONS[0][0]}, ex),
			(r'^~config/(?P<section>%s)$' % '|'.join(map(re.escape, [c[0] for c in CONFIG_AREA_SECTIONS])), config, {}, ex),
			(r'^~mod-logout$', mod_logout, {}, ur),
			
			(b + r'$', view_board,  {'page': 0}, qs),
			(b + r'page(?P<page>[0-9]+)$', view_board, {}, qs),
			(b + r'list$', list, {}, qs),
			(b + r'rss$', board_rss, {}, qs),
			(b + r'config$', board_config, {}, ex),
			(b + r'new-thread$', new_thread, {}, ur),
			(b + r'(?P<thread_id>[0-9]+)$', view_thread, {}, qs),
			(b + r'(?P<thread_id>[0-9]+)/reply$', new_reply, {}, ur),
			(b + r'(?P<thread_id>[0-9]+)/rss$', thread_rss, {}, ur),
			(b + r'(?P<thread_id>[0-9]+)/(?P<post_id>[0-9]+)$', fetch_post, {}, qs),
			(b + r'(?P<thread_id>[0-9]+)/(?P<id_start>[0-9]+)-(?P<id_end>[0-9]+)$', fetch_posts, {}, qs),
			(b + r'(?P<thread_id>[0-9]+)/(?P<post_id>[0-9]+)/options$', options, {}, ur),
			(b + r'(?P<thread_id>[0-9]+)/(?P<post_id>[0-9]+)/mod-options$', mod_options, {}, ur),
			(b + r'(?P<thread_id>[0-9]+)/(?P<post_id>[0-9]+)/quick-mod-action', quick_mod_action, {}, ur),
		)
		
		http.RegexHandler.__init__(self, urls, trans)

	def handle(self, request):
		i18n.purge_modified()
		#guard against (most?) csrf attacks
		if request.method == 'POST' and not request.local_referer():
			s = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
				<html><head><title>Incorrect Referer</title></head><body>
				<p>The referer your browser sent was incorrect or non-existent.
				Site-local referers are required for all POST requests.</p>
				<p>This is likely a configuration error on the part of yourself, or of the site admin.</p>
				</body></html>"""
			return http.Response(s, status = 403)
		return http.RegexHandler.handle(self, request, self.global_db)
	
	def notfound(self, request):
		return render_to_response(request, self.global_db, '404.html', status = 404)
	
	def forbidden(self, request):
		return render_to_response(request, self.global_db, '403.html', status = 403)
	
	def error(self, request):
		logging.exception('exception in request handler')
		return render_to_response(request, self.global_db, '500.html', status = 500)
