""" important stuff """

#IDEA: autogenerate tags for threads and put them on the thread-page.
#IDEA: python 3000 - fewer unicode issues is main reason ... however I tried a few things with 3.0 ... sucks - a few big bugs and poor package support
#IDEA: option for sorting threads differently: interleave sorted by started with sorted by updated
#IDEA: have the index "/" page display threads/posts from all boards OR boards chosen by user - choices saved in cookie
#IDEA: "thanks" (or other text) indicator for posts (with count not shown unless you "thank" too? - would suck for poster)
#IDEA: backup & restore boards - zip containing database file + uploaded_files dir - on big boards would take a long time...
#IDEA: image dump - rar/zip upload? - would be easy way to spam...
#IDEA: full-text search - is an SQLite extension for it
#IDEA: make the quick-post-options be available on the mod-options page aswell
#IDEA: paginate bans
#IDEA: more spam prevention - bad strings etc
#IDEA: have "show previous 100 posts" when lots of posts are hidden...
#IDEA: allow uploading of arbitrary files (with custom icons?)
#IDEA: goto on mod login possibly shouldnt show mod-only boards
#IDEA: nntp interface
#IDEA: dont allow http requests to be sent on internal network via torrents (except possibly not worth bothering with considering dns).
#IDEA: unified interface for thread-list pages - hide all replies/reply box, but otherwise show like board pages


#TODO: investigate possible deadlocks


from __future__ import with_statement

import sys, os.path, random, urllib, subprocess, glob, optparse, fcntl, logging.handlers


NAME = 'img2'
VERSION = '0.9.0'
URL = 'http://img2.info'

DEFAULT_HTTP_PORT = 63331
PID_FILE = '%s.pid' % NAME

CACHING = False

UPLOADED_FILE_DIR = 'webroot/uploaded'				#where uploaded files are saved to
STATIC_DIR = 'webroot/static'						#where static files (css, js, etc) are located
TEMPLATE_DIR = 'templates'							#where templates are loaded from
DATABASE_DIR = 'databases'							#where databases data will be saved
LOCALE_DIR = 'locale'
UPLOADED_FILE_URL = '/~uploaded/'					#prefixed onto urls to uploaded files
STATIC_URL = '/~static/'							#prefixed onto urls to static resources

MAX_COMMENT_WORD_LENGTH = 90
MAX_BLURB_LENGTH = 128

BOARD_DIR_REGEX_INTERNAL = r'(?!~|favicon\.ico|robots\.txt)[^/?#;:@&=<>"\'\x00]{1,64}'
BOARD_DIR_REGEX = r'^%s$' % BOARD_DIR_REGEX_INTERNAL
MOD_NAME_REGEX = r'^[a-zA-Z0-9_]+$'

MODLOG_ENTRIES_PER_PAGE = 256

LOGIN_RESTRICT_SECONDS = 60
LOGIN_RESTRICT_ATTEMPTS = 6

MAX_RSS_ITEMS_PER_BOARD = 64


def available_css():
	return filter(lambda f: f.endswith('.css'), os.listdir(STATIC_DIR))


def available_locales():
	return [''] + [os.path.split(os.path.split(f)[0])[1] for f in glob.glob(os.path.join(LOCALE_DIR, '*', 'messages.po'))]


import pagecache, cookies, captchas, uploaded_files, util, database, transfer

from util import http, templates, json, i18n


class Mod:
	DEFAULT_LOGIN = ('admin', 'password')


class Session:
	""" session data for each user is stored in an instance of this. """
	
	VERSION = 0
	
	def __init__(self, id):
		self.id = id
		self.used_captcha_codes = set()					#captcha codes that this session/user has already used and may not use again
		self.mod = None									#name of the logged in mod, or None
		self.mod_logout_confirm = util.rand_str(24)


def board_link(dir):
	return '<a href="/%s/">/%s/</a>' % (dir, dir)


def thread_link(dir, thread_id):
	return '<a href="/%s/%d">/%s/%d</a>' % ((dir, thread_id)*2)


def post_link(dir, thread_id, post_id):
	return '<a href="/%s/%d#%d">/%s/%d#%d</a>' % ((dir, thread_id, post_id)*2)


def check_age(view):
	""" decorator for view functions to enforce the age check. if used on views with a board arg it checks that board, otherwise it checks if any boards are nsfw. """
	def new_view(request, global_db, **kwargs):
		boards = [kwargs['board_db']] if ('board_db' in kwargs) else global_db.boards
		
		for board_db in boards:
			if board_db.config.show_age_check_page and not cookies.get_overage(request):
				return render_to_response(request, global_db, 'age_check.html')
		
		return view(request, global_db, **kwargs)
	return new_view


def cache(view):
	""" decorator for view functions that returns a cached page under certain conditions. """
	if not CACHING:
		return view
	def new_view(request, global_db, **kwargs):
		pagecache.clean()
		if request.session.mod:
			return view(request, global_db, **kwargs)
		else:
			if request.accepts_gzip():
				page = pagecache.load_gzip(request.path)
				if page:
					return http.Response(page, headers = {'Content-Encoding': 'gzip'})
			else:
				page = pagecache.load(request.path)
				if page:
					return http.Response(page)
			
			response = view(request, global_db, **kwargs)
			if response.status == 200:
				pagecache.store(	kwargs['board_db'].dir if 'board_db' in kwargs else None,
									kwargs['thread_id'] if 'thread_id' in kwargs else None,
									request.path,
									response.body	)
			return response
	return new_view


def _handle_login(view, request, global_db, **kwargs):
	""" helper for other decorators. return http.Response """
	_ = global_db._

	def referer():
		r = request['referer']
		if r:
			r = urllib.unquote_plus(r.encode('ascii', 'replace')).decode('utf-8', 'replace').partition('://')[2]
			i = r.find('/')
			return '/' if i == -1 else r[i:]
	
	mod_name = request.session.mod
	if not mod_name or not global_db.is_mod(mod_name):
		
		if request.is_ajax():
			raise http.Forbidden		# no point showing login page for ajax requests
		
		if request.post('action') == 'login':
			if not global_db.login_attempt(request.remote_host):
				return render_to_response(request, global_db, 'mod_login.html', {'referer': referer(), 'msg': _('You are trying to login too fast. Try again in a minute.')})
			
			name = request.post('name', '')
			password = request.post('password', '')
			mod = global_db.get_mod(name, password)
			
			if mod:
				request.session.mod = name
				if request.post('goto'):
					response = http.RedirectResponse(util.clamp_to_line(request.post('goto', '/')))
					cookies.set_mod(response)
					return response
				else:
					response = http.RedirectResponse('/~config')
					cookies.set_mod(response)
					return response
			else:
				return render_to_response(request, global_db, 'mod_login.html', {'referer': referer(), 'msg': _('Invalid name or password.')})
		else:
			return render_to_response(request, global_db, 'mod_login.html', {'referer': referer(), 'msg': None})
	
	else:
		
		global_db.mod_active(mod_name)
		response = view(request, global_db, **kwargs)
		cookies.set_mod(response)
		return response


def mod_login(view):
	""" decorator for view functions that ensures only mods can access the page. """
	def new_view(request, global_db, **kwargs):
		return _handle_login(view, request, global_db, **kwargs)
	return new_view


def mod_only_board_check(view):
	""" decorator for view functions that ensures only mods can access the page *if* board.config.mod_only. use only with views that take a board_db argument """
	def new_view(request, global_db, **kwargs):
		if not kwargs['board_db'].config.mod_only:
			return view(request, global_db, **kwargs)
		else:
			return _handle_login(view, request, global_db, **kwargs)
	return new_view


def render_to_response(request, db, path, data = None, status = 200):
	global_db = db if isinstance(db, database.Global) else db.global_db
	mod_name = request.session.mod
	if mod_name not in global_db.mods:
		mod_name = None
	data = data or {}
	data.update({
		'mod_name': mod_name,
		'mod_admin': global_db.is_admin(mod_name),
		'mod_flags': global_db.get_flags() if mod_name else None,
		'mod_logout_confirm': request.session.mod_logout_confirm,
		'request': request,
	})
	return http.Response(render_to_string(db, path, data), status = status)


def render_to_string(db, path, data = None):
	global_db = db if isinstance(db, database.Global) else db.global_db
	data = data or {}
	import common, views
	data.update({
		'global_db': global_db,
		
		'sys': sys,
		'random': random,
		
		'database':	database,
		'views': views,
		'common': common,
		'captchas': captchas,
		'uploaded_files': uploaded_files,
		'json': json,
		'util': util,
	})
	return templates.render(TEMPLATE_DIR, path, data, db._)


def generate_pot():
	tpl_1 = i18n.parse(subprocess.Popen(['pygettext', '-o', '-', 'img2/*.py'], stdout = subprocess.PIPE).communicate()[0].decode('utf-8'))
	tpl_2 = templates.extract_local_strings(TEMPLATE_DIR)
	tpl = i18n.merge(tpl_1, tpl_2)

	path = os.path.join(LOCALE_DIR, i18n.TEMPLATE_NAME)
	open(path, 'wb').write(i18n.unparse(tpl).encode('utf-8'))
	print 'generated template "%s"' % path
	
	#update existing catalogs
	for catalog in glob.glob(os.path.join(LOCALE_DIR, '*', i18n.CATALOG_NAME)):
		data = open(catalog, 'rb').read().decode('utf-8')
		dif = i18n.diff(i18n.parse(data), tpl)
		if dif:
			while data and not data.endswith('\n\n'):
				data += '\n'
			data += i18n.unparse(dif)
			open(catalog, 'wb').write(data.encode('utf-8'))
			print 'updated catalog "%s" with new strings' % catalog


def bootstrap():
	""" start img2 """

	#only allow one instance to run at once
	try:
		fd = os.open(PID_FILE, os.O_CREAT | os.O_WRONLY)
		fcntl.flock(fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
		f = os.fdopen(fd, 'w')
		f.truncate()
		f.write(str(os.getpid()))
		f.flush()
	except:
		print 'img2 appears to be already running. try `sudo /etc/init.d/img2 stop`'
		sys.exit(1)

	op = optparse.OptionParser()
	op.add_option('-m', '--make-default-mod', action = 'store_true', help = 'generate a mod/admin account with the default name and password, then exit')
	op.add_option('-g', '--generate-translations', action = 'store_true', help = 'generate a template translation file and update existing translation files with any new strings, then exit')
	op.add_option('-i', '--import', dest = 'import_', metavar = "PATH", help = 'import posts from the kusaba(x)/wakaba installation at the specified path, then exit')
	op.add_option('-e', '--encoding', help = 'encoding to read thread files as when importing them [default: %default]', default = transfer.DEFAULT_ENCODING)
	op.add_option('-d', '--date-format', help = 'date format to read dates as when importing them [default: %default]', default = transfer.DEFAULT_DATE_FORMAT)
	op.add_option('-p', '--port', type = 'int', help = 'http listen port [default: %default]', default = DEFAULT_HTTP_PORT)

	options, args = op.parse_args()

	try:
		#log to a file and stdout
		logger = logging.getLogger()
		logger.setLevel(logging.ERROR)
		logger.addHandler(logging.StreamHandler())
		fh = logging.handlers.RotatingFileHandler('log', maxBytes = 5*1024*1024, backupCount = 2)
		fh.setFormatter(logging.Formatter('-'*80 + '\n%(asctime)s %(name)s %(levelname)s %(message)s'))
		logger.addHandler(fh)
	
		if options.make_default_mod:
			database.Global().add_default_mod()
			print 'default mod added - name: "%s", password: "%s"' % Mod.DEFAULT_LOGIN
			sys.exit(0)

		if options.generate_translations:
			generate_pot()
			sys.exit(0)
	
		if options.import_:
			transfer.transfer(options.import_, options.encoding, options.date_format)
			sys.exit(0)
		
		spath = os.path.join(DATABASE_DIR, 'sessions.pickle')
		import views
		try:
			util.http.Server(('127.0.0.1', options.port), views.Handler(), util.sessions.Manager(Session, spath), 1)
		except:
			logging.exception('exception running server')
		
	finally:
		try:
			os.unlink(PID_FILE)
			pagecache.purge()
		except:
			pass

