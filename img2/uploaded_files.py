"""
stuff to do with validating, saving, deleting etc uploaded files and their meta data.

file meta-data is stored in the database as json, in this format:

	{"type": type, ...other properties...}

where type is one of IMAGE, FLASH or TORRENT, and the other properties depend upon the value of type.

	if type is IMAGE, then data is in this format. if thumb_size is null then this image has no thumbnail:
	
		{"type": IMAGE, "name": filename, "length": bytes-length, "size": [width, height], "thumb_size": ([width, height] OR null)}
	
	if type is FLASH, then data is in this format:
	
		{"type": FLASH, "name": filename, "length": bytes-length}
	
	if type is TORRENT, then data is in this format:
	
		{	"type": TORRENT,
			"name": filename,
			"length": bytes-length,
			"info_hash": base-16-encoded-info-hash,
			"trackers": [tracker, ...],
			"files": [{"name": filename, "length": byte-length}, ...],
			"files_not_shown": number-of-files-not-shown-in-files,
			"comment": comment OR null,
			"created_by": created-by OR null,
			"creation_time": unix-timestamp	OR null}

"""

from __future__ import with_statement

import os, subprocess, shutil, re, urllib, socket, threading, errno, time, logging, Queue

import common, util, util.bencode as bencode

from util import ObjDict, utf8


IMAGE, FLASH, TORRENT = 'IMAGE', 'FLASH', 'TORRENT'

IMAGE_FILE_EXTS = ('jpg', 'gif', 'png')
TORRENT_FILE_EXTS = ('torrent',)
FLASH_FILE_EXTS = ('swf',)

SECONDS_BETWEEN_TORRENT_SCRAPES = 60*15

MAX_TORRENT_COMMENT_LENGTH = 1024
MAX_TORRENT_CREATED_BY_LENGTH = 64
MAX_TORRENT_TRACKER_LENGTH = 128
MAX_TORRENT_TRACKERS = 10
MAX_TORRENT_FILE_NAME_LENGTH = 128
MIN_TORRENT_FILES_FOR_OMITTING = 20
NUM_TORRENT_FILES_TO_SHOW_WHEN_OMITTING = 14

_SCRAPE_REGEX = re.compile('(?<=/)announce(?=[^/]*$)')


class _ScrapeThread(threading.Thread):
	
	def __init__(self):
		self._queue = Queue.Queue(0)		# of (torrent_meta, url)
		self._cache = {}					# util.unhex(info_hash) => (time, {'seeders': x, 'leechers': y} instance or None for fetch failure)
		self._lock = threading.Lock()
		
		threading.Thread.__init__(self)
		self.setDaemon(True)
		self.start()
	
	def _get_scrape_url(self, torrent_meta):
		for url in torrent_meta['trackers']:
			url, n = _SCRAPE_REGEX.subn('scrape', url)
			if n and (url.startswith('http://') or url.startswith('https://')):
				return url
		return None
	
	def _scrape(self, torrent_meta):
		self._queue.put(torrent_meta)
		with self._lock:
			x = self._cache.get(util.unhex(torrent_meta['info_hash']))
			return None if (x is None or x[1] is None) else ObjDict(x[1])
	
	def run(self):
		""" poll self._queue and update self._cache """
		try:
			while True:
				torrent_meta = self._queue.get()
				info_hash = util.unhex(torrent_meta['info_hash'])
				
				with self._lock:
					for key in self._cache.keys():
						if self._cache[key][0] < (time.time() - SECONDS_BETWEEN_TORRENT_SCRAPES):
							del self._cache[key]
					scrape_now = info_hash not in self._cache
				
				#do requested scrape?
				if scrape_now:
				
					socket.setdefaulttimeout(3)	#this is not thread safe, 2.6 fixes the issue
					url = '%s?info_hash=%s' % (self._get_scrape_url(torrent_meta), urllib.quote(info_hash))
					try:
						obj = bencode.decode(urllib.urlopen(url).read(64*1024))	#dont read over 64kb to help stop malicious bastards
						f = obj['files'][info_hash]
						scraped = {	'seeders': util.psint(f.get('complete')),
									'leechers': util.psint(f.get('incomplete'))	}
					except:
						scraped = None
					
					with self._lock:
						self._cache[info_hash] = (time.time(), scraped)
		except:
			#ignore spurious errors from daemon thread termination fuckery (hope there are no actual bugs...)
			pass

_scrape_thread = _ScrapeThread()


class BadFile(Exception):
	""" raised when an uploaded file is no good for some reason. """


def _calc_thumb_size(board_db, size):
	w, h = size
	mw = mh = board_db.config.max_image_thumb_size
	if w > mw:	#fit to width
		h = h * (mw/float(w))
		w = mw
	if h > mh:	#fit to height
		w = w * (mh/float(h))
		h = mh
	return [int(w), int(h)]


def _make_flash(board_db, name, temp_file):
	return {'type': FLASH, 'name': name, 'length': temp_file.length()}


def _make_torrent(board_db, name, temp_file):
	from util import clamp_str, strip_html, valid_time, sha1
	
	d = bencode.decode(temp_file.contents())

	info_hash = None
	trackers = []
	files = []
	files_not_shown = 0
	comment = None
	created_by = None
	creation_time = None
	
	def format(s, ml):
		return clamp_str(strip_html(s.decode('utf-8').strip()), ml, '...')
	
	if 'files' not in d['info']:	# single file only
		
		files = [{	'name': clamp_str(d['info']['name'].encode('utf-8').strip(), MAX_TORRENT_FILE_NAME_LENGTH, '...'),
					'length': int(d['info']['length'])	}]
	
	else:							# multiple files
		
		files = []
		for file in d['info']['files']:
			files += [{	'name': clamp_str('/'.join([t.decode('utf-8') for t in file['path']]), MAX_TORRENT_FILE_NAME_LENGTH, '...'),
						'length': int(file['length'])	}]
		files.sort(key = lambda e: e['name'])
		
		if len(files) >= MIN_TORRENT_FILES_FOR_OMITTING:
			files_not_shown = len(files) - NUM_TORRENT_FILES_TO_SHOW_WHEN_OMITTING
			files = files[:NUM_TORRENT_FILES_TO_SHOW_WHEN_OMITTING]
	
	if 'announce-list' in d:
		trackers = map(lambda t: clamp_str(t.decode('utf-8'), MAX_TORRENT_TRACKER_LENGTH, '...'), d['announce-list'][0])
	elif 'announce' in d:
		trackers = [clamp_str(d['announce'].decode('utf-8'), MAX_TORRENT_TRACKER_LENGTH)]
	trackers = trackers[:MAX_TORRENT_TRACKERS]
	
	if 'comment' in d:
		comment = format(d['comment'], MAX_TORRENT_COMMENT_LENGTH)
	
	if 'created by' in d:
		created_by = format(d['created by'], MAX_TORRENT_CREATED_BY_LENGTH)
	
	if 'creation date' in d:
		creation_time = d['creation date']
		assert valid_time(creation_time)
	
	info_hash = sha1(bencode.encode(d['info']))
	
	return {	'type': TORRENT,
				'name': name,
				'length': temp_file.length(),
				'info_hash': info_hash,
				'comment': comment,
				'created_by': created_by,
				'creation_time': creation_time,
				'trackers': trackers,
				'files': files,
				'files_not_shown': files_not_shown	}


def _make_image(board_db, name, temp_file):
	_ = board_db._

	p = subprocess.Popen(['identify', '-format', '%m %w %h\n', temp_file.path], stdout = subprocess.PIPE)
	r = p.communicate()[0]
	if p.returncode:
		raise BadFile(_('ImageMagick error.'))
	r = util.clamp_to_line(r)	#animated gifs produce one line for *each frame*
	r = r.split()
	img_type = r[0].lower()
	size = [int(r[1]), int(r[2])]
	
	# work around ie exploit
	if not (name.lower().endswith('.' + img_type) or (name.lower().endswith('.jpg') and img_type == 'jpeg')):
		raise BadFile(_('Extension doesn\'t match file format.'))
			
	if size[0] > board_db.config.max_image_size:
		raise BadFile(_('Image too wide, maximum allowed width is %dpx.') % board_db.config.max_image_size)
	
	if size[1] > board_db.config.max_image_size:
		raise BadFile(_('Image too tall, maximum allowed height is %dpx.') % board_db.config.max_image_size)
	
	if (size[0] >= board_db.config.min_image_size_for_thumbnailing) or (size[1] > board_db.config.min_image_size_for_thumbnailing):
		thumb_size = _calc_thumb_size(board_db, size)
	else:
		thumb_size = None
	
	return {'type': IMAGE, 'name': name, 'length': temp_file.length(), 'size': size, 'thumb_size': thumb_size}


def check(file, board_db):
	"""
		file is a util.http.UploadedFile
		return (file_md5, file_meta, temp_file) or raise BadFile
	"""
	_ = board_db._

	temp = util.TempFile()
	md5 = util.copy_file_md5(temp.path, file.file)
	name = util.sanitize_file_name(file.name)
	ext = util.ext(name)
	
	# non type specific checks
	
	if not name or not ext:
		raise BadFile(_('Invalid file name.'))
	
	if not temp.length():
		raise BadFile(_('File too small: zero length files are not allowed.'))
	elif temp.length() > board_db.config.max_file_length:
		raise BadFile(_('File too large: maximum allowed size is %dkb - yours is %dkb') % (board_db.config.max_file_length/1024, temp.length()/1024))
	
	# type specific parsing/checking
	
	if ext in FLASH_FILE_EXTS and board_db.config.allow_flashes:
		f = _make_flash
	elif ext in TORRENT_FILE_EXTS and board_db.config.allow_torrents:
		f = _make_torrent
	elif (ext in IMAGE_FILE_EXTS or file.name.lower().endswith('.jpeg')) and board_db.config.allow_images:
		f = _make_image
	else:
		raise BadFile(_('Disallowed or unrecognised file format.'))
	
	try:
		return md5, f(board_db, name, temp), temp
	except BadFile:
		raise
	except:
		logging.exception('error reading file')
		raise BadFile(_('Error reading file.'))


def _file_paths(file_meta, board_db, thread_id, post_id):
	""" return (file-path, file-thumb-path) for the specified file meta data and post location. """
	def p(*e):
		return os.path.join(common.UPLOADED_FILE_DIR, board_db.dir, str(thread_id), str(post_id), *e)
	return (p(file_meta['name']), p('thumb', file_meta['name']))


def _check_dir(path):
	try:
		os.makedirs(os.path.dirname(utf8(path)))
	except OSError, e:
		if e.errno != errno.EEXIST:
			raise


def save(temp_file, board_db, thread_id, post_id, file_meta):
	"""
		temp_file contains the file data. file_meta contains the meta-data already extracted from that data.
		this function saves the file data, after doing any neccesary conversions/thumbnailing,
		in the correct place given the specified board/thread/post.
	"""
	try:
		fp, tp = _file_paths(file_meta, board_db, thread_id, post_id)
		_check_dir(fp)
		
		if file_meta['type'] == FLASH or file_meta['type'] == TORRENT:
			shutil.copy(temp_file.path, utf8(fp))
			os.chmod(utf8(fp), 0644)
		
		else:	# type == IMAGE
			_check_dir(tp)
			
			if board_db.config.clean_images:
				subprocess.check_call(['mogrify', '-strip', temp_file.path])
			
			shutil.copy(temp_file.path, utf8(fp))
			os.chmod(fp, 0644)
			
			if file_meta['thumb_size']:
				x, y = file_meta['thumb_size']
				subprocess.check_call(['convert', utf8(fp), '-thumbnail', '%dx%d>' % (x, y), utf8(tp)])
				os.chmod(tp, 0644)
	except:
		logging.exception('error saving uploaded file')


def delete(board_db, thread_id, post_id, file_meta):
	""" deletes any files associated with the specified file meta data, given the post it is associated with is as specified. """
	try:
		fp, tp = _file_paths(file_meta, board_db, thread_id, post_id)
		os.unlink(utf8(fp))
		
		if file_meta['type'] == IMAGE and file_meta['thumb_size']:
			os.unlink(utf8(tp))
	except:
		logging.exception('error deleting uploaded file')


def copy(file_meta, src_post, dst_post):
	"""
		src_post and dst_post are both (board_db, thread_id, post_id).
		this function copies any files associated with the specified file meta data, from their locations for
		src_post to their locations for dst_post.
	"""
	try:
		src_fp, src_tp = _file_paths(file_meta, *src_post)
		dst_fp, dst_tp = _file_paths(file_meta, *dst_post)
		
		_check_dir(dst_fp)
		shutil.copy(utf8(src_fp), utf8(dst_fp))
		
		if file_meta['type'] == IMAGE and file_meta['thumb_size']:
			_check_dir(dst_tp)
			shutil.copy(utf8(src_tp), utf8(dst_tp))
	except:
		logging.exception('error copying uploaded file')


def file_urls(board_db, thread_id, post_id, file_meta):
	""" return (file-url, file-thumb-url) for the specified file meta data and the specified post location. """
	x = (	common.UPLOADED_FILE_URL,
			board_db.dir,
			thread_id,
			post_id,
			urllib.quote(utf8(file_meta['name']))	)
	return ('%s%s/%d/%d/%s' % x, '%s%s/%d/%d/thumb/%s' % x)


def scrape_torrent(torrent_meta):
	""" return ObjDict({'seeders': x, 'leechers': y}), or None. """
	return _scrape_thread._scrape(torrent_meta)

