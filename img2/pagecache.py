"""
handles the saving/loading of cached pages.
cached pages are stored as files in .../identity/ and .../gzip/, with "/" in the file names replaced by "#"
pages for mods are not cached.
"""

from __future__ import with_statement

import os, time, threading, glob, tempfile, gzip, shutil

from os.path import join

import common

from util import utf8, quiet_unlink


MAX_MINUTES_OF_LIFE = 5			# max time to cache pages for - needs to be lowish for torrent scrapes and time difference display


_dir = None
_index = (set(), {})	# indexes cached pages by <board,thread>: (global-urls, {board-dir => (board-urls, {thread-id => thread-urls})})
_last_clean = 0
_lock = threading.Lock()


def _url_to_filename(url):
	return utf8(url.replace('/', '#'))


def _check_dir():
	global _dir
	if _dir is None:
		_dir = tempfile.mkdtemp(prefix = '%s-' % common.NAME)
		os.mkdir(join(_dir, 'identity'))
		os.mkdir(join(_dir, 'gzip'))


def purge():
	""" removes all files from the cache. """
	global _dir
	with _lock:
		if _dir:
			shutil.rmtree(_dir)
			_dir = None
			_index[0].clear()
			_index[1].clear()


def thread_modified(board_db, thread_id):
	""" clears the cache of any items related to the specified thread """
	with _lock:
		_check_dir()
		
		def unlink(urls):
			for url in urls:
				for type in ('identity', 'gzip'):
					quiet_unlink(join(_dir, type, _url_to_filename(url)))
			urls.clear()
		
		#thread pages
		try:
			urls = _index[1][board_db.dir][1][thread_id]
		except KeyError:
			pass
		else:
			unlink(urls)
			del _index[1][board_db.dir][1][thread_id]
		
		#board pages
		try:
			urls = _index[1][board_db.dir][0]
		except KeyError:
			pass
		else:
			unlink(urls)
			if not _index[1][board_db.dir][1]:
				del _index[1][board_db.dir]
		
		#global pages
		unlink(_index[0])


def clean():
	""" if MAX_MINUTES_OF_LIFE have elapsed since the last clean, clear the cache of items older than MAX_MINUTES_OF_LIFE """
	global _last_clean
	with _lock:
		_check_dir()
		
		if (time.time() - _last_clean) > MAX_MINUTES_OF_LIFE*60:
			
			_last_clean = time.time()
			min_mtime = time.time() - MAX_MINUTES_OF_LIFE*60
			
			def clean_urls(urls):
				for url in list(urls):
					try:
						fi = join(_dir, 'identity', _url_to_filename(url))
						fg = join(_dir, 'gzip', _url_to_filename(url))
						if os.path.getmtime(fi) < min_mtime:
							urls.remove(url)
							os.unlink(fi)
							os.unlink(fg)
					except OSError:
						urls.discard(url)
			
			gurls, boards = _index
			clean_urls(gurls)
			for dir, (burls, threads) in boards.items():
				clean_urls(burls)
				for tid, turls in threads.items():
					clean_urls(turls)
					if not turls:
						del threads[tid]
				if not burls and not threads:
					del boards[dir]


def load(url):
	""" return a utf-8 encoded byte-string from the cache that has the specified url, or None if it doesnt exist """
	with _lock:
		_check_dir()
		try:
			with open(join(_dir, 'identity', _url_to_filename(url)), 'rb') as f:
				return f.read()
		except IOError:
			return None


def load_gzip(url):
	""" return a utf-8 encoded, gzipped, byte-string from the cache that has the specified url, or None if it doesnt exist """
	with _lock:
		_check_dir()
		try:
			with open(join(_dir, 'gzip', _url_to_filename(url)), 'rb') as f:
				return f.read()
		except IOError:
			return None


def store(board_dir, thread_id, url, page):
	""" store page under url in the cache, linked to <board_dir, thread_id>. board_dir and/or thread_id may be None. page must be a utf-8 encoded byte-string. """
	with _lock:
		_check_dir()
		
		# save to disk - should be atomic so put it in a temp file then rename it
		
		fd, path = tempfile.mkstemp(prefix = '*', dir = _dir)
		with os.fdopen(fd, 'wb') as f:
			f.write(page)
		os.rename(path, join(_dir, 'identity', _url_to_filename(url)))
		
		fd, path = tempfile.mkstemp(prefix = '*', dir = _dir)
		with os.fdopen(fd, 'wb') as f:
			gzf = gzip.GzipFile(mode = 'wb', compresslevel = 3, fileobj = f)
			gzf.write(page)
			gzf.close()
		os.rename(path, join(_dir, 'gzip', _url_to_filename(url)))
		
		# modify index
		if board_dir is None:
			_index[0].add(url)
		else:
			_index[1].setdefault(board_dir, (set(), {}))
			if thread_id is None:
				_index[1][board_dir][0].add(url)
			else:
				_index[1][board_dir][1].setdefault(thread_id, set())
				_index[1][board_dir][1][thread_id].add(url)


def size():
	""" return the number of cached files, not counting extra for compressed versions of the same file """
	n = len(_index[0])
	for urls, threads in _index[1].values():
		n += len(urls)
		for urls in threads.values():
			n += len(urls)
	return n
