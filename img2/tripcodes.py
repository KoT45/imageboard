""" stuff to do with tripcodes """


import random

import util


def make(global_db, input):
	""" returns the tripcode output string for the given input string """
	
	if input == '':
		return ''

	rnd = random.Random()	#need to be threadsafe
	rnd.seed(util.md5(global_db.unique_string + input))
		
	def make_word(length):
		v = 'aeiou'
		c = 'bcdfghjklmnpqrstvwxyz'
	
		word = ''
		curV = True
		
		while len(word) < length:
			if (curV and rnd.randint(0, 6)) or (not curV and rnd.randint(0, 16)):
				curV = not curV
			word += rnd.choice(v if curV else c)
		return word

	first_len = rnd.randint(3, 6)
	return make_word(first_len) + ' ' + make_word(11-first_len)
