

import util


class Field(object):
	
	def __init__(self, name, default, desc = ""):
		self.name = name
		self.default = default
		self.desc = desc
	
	def cast(self, x):
		""" x is a string that is to be converted to the values stored in this field. if x cannot be converted ValueError is thrown. """
		return x
	
	def html(self, name, value, disabled = False):
		return '<input type="text" name="%s" value="%s" %s>' % (name, value, 'disabled' if disabled else '')


class Bool(Field):
	
	def cast(self, x):
		try:
			return bool(x)
		except:
			raise ValueError
	
	def html(self, name, value, disabled = False):
		return '<input type="checkbox" name="%s" %s %s>' % (name, "checked" if value else "", 'disabled' if disabled else '')


class Int(Field):
	
	def __init__(self, name, min, max, default, desc = ""):
		Field.__init__(self, name, default, desc)
		self.min = min
		self.max = max
		
	def cast(self, x):
		try:
			x = int(x)
			assert self.min <= x <= self.max
			return x
		except:
			raise ValueError
	
	def html(self, name, value, disabled = False):
		return '<input type="text" name="%s" value="%s" %s>' % (name, value, 'disabled' if disabled else '')


class String(Field):
	
	def __init__(self, name, min_len, max_len, default = '', desc = '', input_rows = 1):
		Field.__init__(self, name, default, desc)
		self.min_len = min_len
		self.max_len = max_len
		self.input_rows = input_rows
	
	def cast(self, x):
		try:
			assert self.min_len <= len(x) <= self.max_len
			return x
		except:
			raise ValueError
	
	def html(self, name, value, disabled = False):
		if self.input_rows == 1:
			return '<input type="text" name="%s" value="%s" %s>' % (name, util.html_escape(value), 'disabled' if disabled else '')
		else:
			return '<textarea rows="%d" cols="70" name="%s" %s>%s</textarea>' % (self.input_rows, name, 'disabled' if disabled else '', util.html_escape(value))


class SelectField(Field):

	def __init__(self, name, list_all, default = '', desc = ''):
		Field.__init__(self, name, default, desc)
		self.list_all = list_all

	def cast(self, x):
		try:
			assert (x in self.list_all())
			return x
		except:
			raise ValueError

	def html(self, name, value, disabled = False):
		return ('<select name="%s" %s>' % (name, 'disabled' if disabled else '')) +\
			''.join('<option value="%s" %s>%s</option>' % (util.html_escape(f), 'selected' if f == value else '', util.html_escape(f)) for f in self.list_all()) +\
				'</select>'
	


class Interval(Field):
	
	def __init__(self, name, min, max, default = 0, desc = ""):
		Field.__init__(self, name, default, desc)
		self.min = min
		self.max = max
	
	def cast(self, x):
		try:
			x = util.parse_interval(x)
			assert x is not None and self.min <= x <= self.max
		except:
			raise ValueError
		return x
	
	def html(self, name, value, disabled = False):
			return '<input type="text" name="%s" value="%s" %s>' % (name, util.format_interval(value, 2), 'disabled' if disabled else '')
