""" anything that touches the cookies should go through here instead of accessing request/response cookie methods directly """

import util


_YEAR = 60*60*24*365
_MONTH = 60*60*24*30


def set_overage(response):
	response.set_cookie('overage', '1', _YEAR, path = '/')

def get_overage(request):
	return request.cookies('overage') == '1'


def set_name(response, name):
	response.set_cookie('name', name, _YEAR, path = '/')

def get_name(request):
	return request.cookies('name', '')


def set_tripcode(response, tripcode):
	response.set_cookie('tripcode', tripcode, _YEAR, path = '/')

def get_tripcode(request):
	return request.cookies('tripcode', '')


def set_noko(response, value):
	response.set_cookie('noko', '1' if value else '0', _YEAR, path = '/')

def get_noko(request):
	return request.cookies('noko') == '1'


def set_mod(response):
	response.set_cookie('mod', '1', _YEAR, path = '/')

def get_mod(request):
	return request.cookies('mod') == '1'

def delete_mod(response):
	response.delete_cookie('mod', path = '/')


def set_show_mod_flag(response, value):
	response.set_cookie('show_mod_flag', '1' if value else '0', _YEAR, path = '/')

def get_show_mod_flag(request):
	return request.cookies('show_mod_flag') == '1'


def set_post_verifier(response, board_db, thread_id, post_id):
	"""sets the cookie used to verify a user made a post (used when they attempt to delete it) """
	value = util.sha1('%s%s%s%s' % (board_db.global_db.unique_string, board_db.dir, thread_id, post_id))
	response.set_cookie('verifier', value, _MONTH, path = '/%s/%d/%d/' % (board_db.dir, thread_id, post_id))

def check_post_verifier(request, board_db, thread_id, post_id):
	value = util.sha1('%s%s%s%s' % (board_db.global_db.unique_string, board_db.dir, thread_id, post_id))
	return request.cookies('verifier') == value

def delete_post_verifier(response, board_db, thread_id, post_id):
	response.delete_cookie('verifier', path = '/%s/%d/%d/' % (board_db.dir, thread_id, post_id))