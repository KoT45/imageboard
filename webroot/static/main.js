

function escapeHtml(input){
	return input ? input.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;') : '';
}


function getFragment(){
	var fullURL = document.location.href;
	var ind = fullURL.indexOf('#');
	return ind==-1 ? '' : fullURL.substring(ind+1);
}


function readCookie(name, defaultValue){
	if(typeof(defaultValue) == 'undefined')
		defaultValue = '';
	var sc = document.cookie.split(";");
	for(var i=0; i<sc.length; i++){
		var cur = sc[i];
		//padding at the start...
		while(cur.length > 0 && cur.charAt(0) == ' ')
			cur = cur.substring(1);
		if(cur.indexOf(name + '=') == 0)
			return decodeURIComponent(cur.substring(name.length+1));
	}
	return defaultValue;
}


//called by the "Quote" link on each post
function quote(event){
	var aElement = $(this);
	var btp = /([^\/]+)\/(\d+)\/reply\?q=(\d+)$/.exec(aElement.attr('href'));
	
	var e = $('#[id=\'tr_comment_' + btp[1] + '_' + btp[2] + '\']');
	if(e.size() == 0)
		return true;
	
	var text = '';
	try{	//probably doesnt work in all browsers...
		var ws = window.getSelection();
		var ca = ws.getRangeAt(0).commonAncestorContainer;
		if (ws.toString() && $(ca).closest('.reply_text').size() == 1 && $(ca).closest('.reply').get(0) == $(aElement).closest('.reply').get(0)) {
			text = '>' + ws + '\r\n';
		}
	}catch(e){}
	
	e.val(e.val() + '>>' + btp[3] + '\r\n' + text + '\r\n');
	e.focus();
	e.scrollTop(999999);
	return false;
}


//called by thumbnailed images when clicked on
function expImg(a, width, height){
	if(width > $(window).width()*0.8 || height > $(window).height()*0.9){
		$(a).attr('target', '_blank');
		return true;
	}
	else{
		var img = $(a).children().filter('img');
		var cw = img.width(), ch = img.height(), cs = img.attr('src');
		$(a).attr('onclick', '');
		$(a).unbind('click');
		$(a).click(function(){
			return cntImg(a, cw, ch, cs);
		});
		img.attr('src', a.href).attr('class', 'image').width(width).height(height);
		return false;
	}
}


function cntImg(a, width, height, src){
	var img = $(a).children().filter('img');
	var cw = img.width(), ch = img.height(), cs = img.attr('src');
	$(a).unbind('click');
	$(a).click(function(){
		return expImg(a, cw, ch);
	});
	img.attr('src', src).attr('class', 'image_thumb').width(width).height(height);
	return false;
}


function omitted(aElement, board, threadId, endPostId){
	//clicking more than once shouldnt fuck it up
	if(aElement.loading)
		return false;
	aElement.loading = true;
	
	$.get('/' + board + '/' + threadId + '/1-' + endPostId, {}, function(data, status){
		if(status == 'success'){
			var posts = $(data);
			$('.chain', posts).click(chain);
			$('.do_quote', posts).click(quote);
			$(aElement).replaceWith(posts);
		}
	}, 'html');
	
	return false;
}


function chain(event){
	
	var aElement = $(this);
	var btp = /([^\/]+)\/(\d+)#(\d+)$/.exec(aElement.attr('href'));
	
	if(btp == null)
		return true;
	
	//clicking more than once shouldnt fuck it up
	if(aElement.attr('loading'))
		return false;
	aElement.attr('loading', true);
	
	$.get('/' + btp[1] + '/' + btp[2] + '/' + btp[3], {}, function(data, status){
		if(status == 'success'){
			var table = $(data);
			$('.chain', table).click(chain);
			$('.do_quote', table).click(quote);
			aElement.replaceWith(table);
		}
	}, 'html');
	
	return false;
}


$(document).ready(function(){
	if(typeof page == 'undefined')
		page = 'other';

	//highlight linked post
	if(page == 'thread' && getFragment())
		$('#' + getFragment()).addClass('highlighted_post');

	//auto set form fields
	$('table.new_reply input[name=name]').add('table.new_thread input[name=name]').val(readCookie('name'));
	$('table.new_reply input[name=tripcode]').add('table.new_thread input[name=tripcode]').val(readCookie('tripcode'));
	$('input[name=noko]').each(function(){
		$(this).get(0).checked = readCookie('noko') == '1';
	});
	$('table.new_reply input[name=mod]').add('table.new_thread input[name=mod]').each(function(){
		$(this).get(0).checked = readCookie('show_mod_flag') == '1';
	});
		
	//enforce maxlength on textareas
	function limit(){
		if($(this).attr('maxlength')){
			var v = $(this).val();
			var m = $(this).attr('maxlength');				
			if(v.length > m)
				$(this).val(v.substring(0, m));
		}
	}
	$('textarea').keyup(limit);
	
	//setup javascript anti spam
	$('table.new_reply textarea[name=comment]').add('table.new_thread textarea[name=comment]').each(function(){
		$(this).after('<input type="hidden" name="javascript_anti_spam" value="">');
	});
	
	//setup >>X quote expansion
	$('.chain').click(chain);
	
	//setup quote links
	$('.do_quote').click(quote);
	
	//setup ajax quick-mod-action
	
	var names = ['ban_ip', 'ban_and_delete_file', 'delete'];
	
	$('.quick_mod_action').each(function(){
		var cur = this;
		
		function updateButton(){
			var any = false;
			for(var i=0; i<names.length; i++){
				if($(cur).find('[name=' + names[i] + ']').is(':checked'))
					any = true;
			}
			any ? $(cur).find('[type=submit]').removeAttr('disabled') : $(cur).find('[type=submit]').attr('disabled', 'disabled');
		}
		
		for(var i=0; i<names.length; i++)
			$(cur).find('[name=' + names[i] + ']').change(updateButton);
		
		updateButton();
	});
	
	$('.quick_mod_action').submit(function(){
		var postData = {};
		
		for(var i=0; i<names.length; i++)
			if($(this).find('[name=' + names[i] + ']').is(':checked'))
				postData[names[i]] = 'on';
		
		$(this).find('input').attr('disabled', 'disabled');
		
		var submit = $(this).find('[type=submit]');
		$.post($(this).attr('action'), postData, function(data, textStatus){
			submit.attr('value', textStatus);
		}, 'text');
			
		return false;
	});

});
